/*
 * error.h 
 * (c) 2009 Alexey Paznikov
 */

#ifndef _NM_ERROR_H
#define _NM_ERROR_H

#define LOG_DIR		"/opt/monitoring/netmon/var"

enum
{
	LOG_STR_LEN =      4096,	/* Size of string for record in log. */
	LOG_FILENAME_LEN = 80,
	LOG_DATE_LEN =     50
};

/* Name of the program calling error. */
extern char *Program_name; 	

int initlog(const char *home_path);
void error(int, int, char*, ...);
void applog(char *fmt, ...);

#endif /* _NM_ERROR_H */
