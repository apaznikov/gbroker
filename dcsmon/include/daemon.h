/*
 * daemon.h:
 * (c) 2008 Alexey Paznikov
 */

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#ifndef _DAEMON_H
#define _DAEMON_H

#define	DAEMON_ERR	1
#define	BUFLEN		128

#define LOCKDIRMODE	(S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH)
#define LOCKMODE	(S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)

enum
{
	NHOSTS	 = 10,		/* Number of host systems. */
	STR_SIZE = 255
};

/* daemonize: Initialize damon process. */
int daemonize(void);

/* already_running: Is the daemon already run? */
int isrunning(const char *var_path);

/* gobg: Go to the background mode. */
void gobg(void);

extern char Lockfile[BUFLEN];

#endif /* _DAEMON_H */
