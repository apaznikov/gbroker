/*
 * dm_gw.h
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _DM_GW_H
#define _DM_GW_H

typedef struct
{
	char queue_name[DM_GWHOST_STRLEN];
	char queue_nodecount[DM_GWHOST_STRLEN];
	char queue_freenodecount[DM_GWHOST_STRLEN];
	char queue_maxcputime[DM_GWHOST_STRLEN];
	char queue_status[DM_GWHOST_STRLEN];
	char queue_dispatchtype[DM_GWHOST_STRLEN];
} dm_gwqueue_t;

typedef struct
{
	char hostattrfile[DM_PATH_LEN];
	char hostname[DM_GWHOST_STRLEN];
	char arch[DM_GWHOST_STRLEN];
	char os_name[DM_GWHOST_STRLEN];
	char os_version[DM_GWHOST_STRLEN];
	char cpu_model[DM_GWHOST_STRLEN];
	char cpu_mhz[DM_GWHOST_STRLEN];
	char cpu_free[DM_GWHOST_STRLEN];
	char cpu_smp[DM_GWHOST_STRLEN];
	char nodecount[DM_GWHOST_STRLEN];
	char fork_name[DM_GWHOST_STRLEN];
	char lrms_name[DM_GWHOST_STRLEN];
	char lrms_type[DM_GWHOST_STRLEN];

	dm_gwqueue_t *queues;
	int nqueues;
} dm_gwhost_t;

/* gwattrupdate: Get from dcsmon information, renew GridWay resource files. */
int gwattrupdate();

/* gwwriteattrs: Write attributes to file. */
int gwwriteattrs();

/* gwreadhosts: Read GridWay config, hosts. */
int gwreadhosts();

/* gwhostfreemem: Free memory for GWhosts. */
void gwhostfreemem();

#endif /* _DM_GW_H */
