/*
 * nm_stat.h: Client module
 *
 * (c) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _NM_STAT_H
#define _NM_STAT_H

enum { DM_CMD_LEN = 255 };

#endif /* _NM_STAT_H */
