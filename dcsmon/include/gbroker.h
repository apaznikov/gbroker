/*
 * grbroker.h: Main gbroker module.
 *
 * (c) 2008-2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _GBROKER_H
#define _GBROKER_H

#include <semaphore.h>
#include <pthread.h>
#include "tcp.h"

#define _DEBUG
#define _SCHEDULER_DEBUG
/*
#define _JOBMON_DEBUG
*/

/* 
 * Criterion for job migration. 
 * Min gain by object function between old and new object values. 
 */ 
#define GB_MIGRATE_OBJ_CRIT 0.2

/* Min value of stagetime. */
#define GB_STAGETIME_EPS    5.0

enum
{
	/* Number of hosts, on which job is simultaneously submitted. */
	GB_NSUBMITS        = 1,

	/* Frequency of job state query. */
	/*
	GB_JOBMON_TIMEOUT  = 20,
	*/
	GB_JOBMON_TIMEOUT  = 20,

	/* 
	 * Job migrating interval. Must be more than minimal time 
	 * after submitting to host when job is in pending state. 
	 */
	/*
	GB_MIGR_INTERVAL   = 60,
	*/
	GB_MIGR_INTERVAL   = 30,

	GB_PATH_LEN        = 255,
	GB_HNAME_LEN       = 255,
	GB_JOBDESCR_STRLEN = 255,
	GB_CMD_LEN         = 255,
	GB_CONTACT_LEN     = 255,
	GB_STATUS_LEN      = 255,
	GB_TOKEN_LEN       = 255,
	GB_GID_LEN         = 255,
	GB_SRVMSG_LEN      = 20,

	GB_STAT_TIMEOUT    = 4
};

typedef enum
{
	GB_JOBRUNNING_ST,
	GB_JOBCOMPLETE_ST,
	GB_UNKNOWN_ST
} gb_jobstate_t;

#define GBROKER_PORT          "15000"
#define GB_JOBSUBMIT_RQST     "gb_jobsubmit_request_"
#define GB_GETOUTPUT_RQST     "gb_getoutput_request_"
#define GB_JOBSUBMIT_ERROR    "gb_jobsubmit_error___"
#define GB_JOB_RUNNING_RPL    "gb_job_running_______"
#define GB_JOB_COMPLETE_RPL   "gb_job_complete______"
#define GB_SUCCESS            "gb_success___________"

typedef struct
{
	int    id;
	SOCKET sock;
	char   hname[GB_HNAME_LEN];
	char   shname[GB_HNAME_LEN];
	double obj;
	int    nprocs;
} gb_host_t;

typedef struct
{
	char name[GB_JOBDESCR_STRLEN];
	char src[GB_JOBDESCR_STRLEN];
	char dest[GB_JOBDESCR_STRLEN];
} gb_stage_t;

typedef struct 
{
	int hid;                  /* Host ID */

	/* Host current state: */
	double stagetime;		/* Time to stage files. */
	double frprocs;			/* Number of free processors. */
	double totprocs;		/* Total number of processors. */
	double nrjobs;			/* Number of running jobs. */
	double nqjobs;			/* Number of queued jobs. */
	double obj;			    /* Value of objective function. */
} gb_schedhosts_t;

typedef struct
{
	/* Job identifiers. */
	char gid[GB_GID_LEN];				/* Global job id. */
	int  id;							/* Local job id. */

	/* Job description. */
	char name[GB_JOBDESCR_STRLEN];		/* Job name. */
	char exec[GB_JOBDESCR_STRLEN];		/* Execution file. */
	char args[GB_JOBDESCR_STRLEN];		/* Program arguments. */
	char stout[GB_JOBDESCR_STRLEN];		/* Name of standart output file. */
	char sterr[GB_JOBDESCR_STRLEN];		/* Name of standart error file. */
	gb_stage_t *stages;					/* Stage-files. */
	int  nstages;						/* Number of stage-files. */
	int  rank;							/* Parallel program rank. */
	int  wallt;							/* Walltime. */

	/* Globus execution. */
	char **contact;						/* Globus Toolkit contact string. */
	char **status;   					/* Globus Toolkit status. */
	char **rsl;							/* Globus Toolkit RSL job file. */
	gb_host_t **submithosts;            /* Hosts where job was submitted. */
	gb_schedhosts_t *schedhosts;        /* Non-sorted hosts (state & obj), 
	                                       where job is submitted. */
	int migrhost;                       /* Host, from which job is migrating */
	gb_host_t *exechost;				/* Host where job was executed. */
	int  *nodes;						/* Number of nodes (subsystem). */
	int  *ppn;							/* Processors per node (subsystems) */


	/* Flags. */
	int  outputflag;					/* 1 if job s interactive. */
	sem_t complete_sem;					/* 1 if job is completed. */

	/* Time moments. */
	struct timeval hostsubmittime;		/* When job was submitted to host. */
	struct timeval submittime;			/* When job was submitted to gbroker. */
	struct timeval migrtime;			/* Migrate attempt time. */
} gb_job_t;

typedef struct
{
	SOCKET sock;
} server_args_t;

extern char Gbroker_home[GB_PATH_LEN];
extern char Thishost[GB_HNAME_LEN];
extern gb_host_t *Hosts;
extern int Nhosts;

extern pthread_mutex_t Jid_mutex;
extern pthread_mutex_t Jobsubmit_mutex;

#endif /* _GBROKER_H */
