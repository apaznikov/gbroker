/*
 * dm_sysinfo.h: Various system information functions.
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _DM_SYSINFO_H
#define _DM_SYSINFO_H

#include "dcsmon.h"

/* getarch: Get system architecture (i686, x86_64, etc) */
int getarch(char *arch);

#endif /* _DM_SYSINFO_H */
