/*
 * dm_torque.h: Functions for accumulate info from TORQUE.
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _DM_TORQUE_H
#define _DM_TORQUE_H

#include "dcsmon.h"

enum { DM_TOKEN_LEN = 1024 };

/* torque_getprocmap: Get info about free/busy processors. */
unsigned short int *torque_getprocmap(char *pbsnodes_path, int *nnodes);

/* torque_getnprocs: Get number of free processors. */
int torque_getfreeprocs(char *pbsnodes_path, int *nprocs);

/* torque_getnprocs: Get total number of processors. */
int torque_getallprocs(char *pbsnodes_path, int *nprocs);

/* torque_getallnodes: Get total number of nodes. */
int torque_getallnodes(char *pbsnodes_path, int *nnodes);

/* torque_getppn: Get number of processors per node. */
int torque_getppn(char *pbsnodes_path, int *nprocs);

/* torque_getfreenodes: Get upper bound to serve queue (walltime). */
int torque_getfreenodes(char *pbsnodes_path, int *nnodes);

/* torque_getnjobs: Get number of jobs running and waiting in queue. */
int torque_getnjobs(char *qstat_path, int *nrunning, int *nqueued);

/* torque_getnrunning: Get number of running jobs. */
int torque_getnrunning(char *qstat_path, int *njobs);

/* torque_getnqueued: Get number of jobs waiting in queue. */
int torque_getnqueued(char *qstat_path, int *njobs);

/* torque_getqueuewalltime: Get total number of nodes. */
int torque_getqueuewalltime(char *qstat_path, int *time);

/* getcpufreq: Get CPU frequency. */
int getcpufreq(int *cpufreq);

/* cpufreqinit: Initialize CPU frequency value. */
int cpufreqinit(char *pbsnodes_path);

#endif /* _DM_TORQUE_H */
