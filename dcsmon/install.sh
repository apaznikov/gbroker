#!/bin/sh

#HOSTS="xeon16.cpct.sibsutis.ru xeon32.cpct.sibsutis.ru l400a1.vpn.local l400a2.vpn.local l400l1.vpn.local"
source ~/bin/header.sh

DCSMON_PATH=/opt/monitoring/dcsmon

cd src
make 
cd ..

if [ -f obj/dcsmon ]; then
	cp  obj/dcsmon $DCSMON_PATH/bin/
	cp  obj/dcsstat $DCSMON_PATH/bin/
	cp  obj/dcs_subsystem $DCSMON_PATH/bin/
	cp  etc/dcsmon-xeon80.vpn.local.conf \
	    $DCSMON_PATH/etc/dcsmon.conf

	chmod +s $DCSMON_PATH/bin/dcsmon

	for host in $VPN_REMOTE_HOSTS; do
		scp obj/dcsmon gdev@$host:/opt/monitoring/dcsmon/bin/
		scp obj/dcsstat gdev@$host:/opt/monitoring/dcsmon/bin/
		scp obj/dcs_subsystem \
			gdev@$host:/opt/monitoring/dcsmon/bin/
		scp etc/dcsmon-$host.conf \
			gdev@$host:/opt/monitoring/dcsmon/etc/dcsmon.conf

		CMD="chmod +s $DCSMON_PATH/bin/dcsmon"
		ssh -q $host $CMD
	done
fi
