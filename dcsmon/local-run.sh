#!/bin/sh

cd src
make

if [ -f /opt/monitoring/dcsmon/var/dcsmon.pid ]; then
	rm /opt/monitoring/dcsmon/var/dcsmon.pid
fi

../obj/dcsmon -g
