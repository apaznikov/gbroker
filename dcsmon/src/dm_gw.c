/*
 * dm_gw.c: Dcsmon GridWay functions
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>

#include "dcsmon.h"
#include "error.h"
#include "dm_client.h"
#include "dm_gw.h"

char GWhome[DM_PATH_LEN];
dm_gwhost_t *GWhosts;
int NGWhosts;

/* getgwattrs: Get some of subsystem attributes (nnodes, ppn, etc). */
static int getgwattrs(char *hostname, int *nnodes, int *freenodes, int *nprocs,
                      int *ppn, int *freeprocs, int *cpufreq);

/* gethostlist: Get GridWay host list. */
static int gethostlist(char *hostlistfile);

/* gethostattrs: Get GridWay host attributes. */
static int gethostattrs();

/* getgwhostfile: Read GW cfg and find IM_MAD host file. */
static int getgwhostfile(char *file);

/* gwattrupdate: Get from dcsmon information, renew GridWay resource files. */
int gwattrupdate()
{
	int i, j, nnodes, freenodes, nprocs, ppn, freeprocs, cpufreq;

	for (i = 0; i < NGWhosts; i++)
	{
		if (getgwattrs(GWhosts[i].hostname, &nnodes, &freenodes, &nprocs, &ppn,
			           &freeprocs, &cpufreq) < 0)
		{
			error(0, 0, "getgwattr() failed for %s", GWhosts[i].hostname);

			sprintf(GWhosts[i].cpu_free, "=0\n");
			sprintf(GWhosts[i].nodecount, "=0\n");
			sprintf(GWhosts[i].cpu_smp, "=0\n");
			for (j = 0; j < GWhosts[i].nqueues; j++)
			{
				sprintf(GWhosts[i].queues[j].queue_nodecount,"=0\n");
				sprintf(GWhosts[i].queues[j].queue_freenodecount,"=0\n");
			}
		}
		else
		{
			#ifdef _CPUFREQ_ENABLE
			sprintf(GWhosts[i].cpu_mhz, "=%d\n", cpufreq);
			#endif

			if ((nprocs == 0) || (freeprocs == 0))
				sprintf(GWhosts[i].cpu_free, "=0\n");
			else
				sprintf(GWhosts[i].cpu_free, "=%d\n", 
						(int) ((double) freeprocs / (double) nprocs * 100));
			/*
			sprintf(GWhosts[i].nodecount, "=%d\n", nnodes);
			*/
			sprintf(GWhosts[i].nodecount, "=%d\n", nprocs);
			sprintf(GWhosts[i].cpu_smp, "=%d\n", ppn);

			for (j = 0; j < GWhosts[i].nqueues; j++)
			{
				/*
				sprintf(GWhosts[i].queues[j].queue_nodecount, "=%d\n", nnodes);
				sprintf(GWhosts[i].queues[j].queue_freenodecount, "=%d\n", 
				        freenodes);
				*/
				sprintf(GWhosts[i].queues[j].queue_nodecount, "=%d\n", nprocs);
				sprintf(GWhosts[i].queues[j].queue_freenodecount, "=%d\n", 
				        freeprocs);
			}
		}

		/*
		fprintf(stderr, "%s\n", GWhosts[i].cpu_mhz);
		fprintf(stderr, "%s\n", GWhosts[i].cpu_free);
		fprintf(stderr, "%s\n", GWhosts[i].nodecount);
		fprintf(stderr, "%s\n", GWhosts[i].cpu_smp);

		for (j = 0; j < GWhosts[i].nqueues; j++)
		{
			fprintf(stderr, "\t%s\n", GWhosts[i].queues[j].queue_nodecount);
			fprintf(stderr, "\t%s\n", GWhosts[i].queues[j].queue_freenodecount);
		}
		*/
	}

	return 0;
}

/* getgwattrs: Get some of subsystem attributes (nnodes, ppn, etc). */
static int getgwattrs(char *hostname, int *nnodes, int *freenodes, int *nprocs,
                      int *ppn, int *freeprocs, int *cpufreq)
{
	if ((*nnodes = dm_getnnodes(hostname)) < 0)
	{
		/* error(0, 0, "dm_getnnodes() failed for %s", hostname); */
		return -1;
	}

	if ((*freenodes = dm_getfreenodes(hostname)) < 0)
	{
		/* error(0, 0, "dm_getfreenodes() failed for %s", hostname); */
		return -1;
	}

	if ((*nprocs = dm_getnprocs(hostname)) < 0)
	{
		/* error(0, 0, "dm_getnprocs() failed for %s", hostname); */
		return -1;
	}
	
	if ((*ppn = dm_getppn(hostname)) < 0)
	{
		/* error(0, 0, "dm_getppn() failed for %s", hostname); */
		return -1;
	}

	if ((*freeprocs = dm_getfreeprocs(hostname)) < 0)
	{
		/* error(0, 0, "dm_getfreeprocs() failed for %s", hostname); */
		return -1;
	}

	#ifdef _CPUFREQ_ENABLE
	if ((*cpufreq = dm_getcpufreq(hostname)) < 0)
	{
		/* error(0, 0, "dm_getcpufreq() failed for %s", hostname); */
		return -1;
	}
	#endif

	return 0;
}

/* gwreadhosts: Read GridWay config, hosts. */
int gwreadhosts()
{
	char hostlistfile[DM_PATH_LEN];

	if (getgwhostfile(hostlistfile) < 0)
	{
		error(0, errno, "getgwhostfile() failed");
		return -1;
	}

	if (gethostlist(hostlistfile) < 0)
	{
		error(0, errno, "gethostlist() failed");
		return -1;
	}

	if (gethostattrs() < 0)
	{
		error(0, errno, "gethostattrs() failed");
		return -1;
	}

	if (gwwriteattrs() < 0)
	{
		error(0, errno, "writattrstofiles() failed");
		return -1;
	}

	return 0;
}

/* gwwriteattrs: Write attributes to file. */
int gwwriteattrs()
{
	char path[DM_PATH_LEN];
	FILE *file;
	int i, j;

	for (i = 0; i < NGWhosts; i++)
	{
		sprintf(path, "%s.tmp", GWhosts[i].hostattrfile);
		if ((file = fopen(path, "w")) == NULL)
		{
			error(0, errno, "fopen() failed for %s", path);
			return -1;
		}

		fprintf(file, "HOSTNAME=\"%s\"\n", GWhosts[i].hostname);
		fprintf(file, "ARCH%s", GWhosts[i].arch);
		fprintf(file, "OS_NAME%s", GWhosts[i].os_name);
		fprintf(file, "OS_VERSION%s", GWhosts[i].os_version);
		fprintf(file, "CPU_MODEL%s", GWhosts[i].cpu_model);
		fprintf(file, "CPU_MHZ%s", GWhosts[i].cpu_mhz);
		fprintf(file, "CPU_FREE%s", GWhosts[i].cpu_free);
		fprintf(file, "CPU_SMP%s", GWhosts[i].cpu_smp);
		fprintf(file, "NODECOUNT%s", GWhosts[i].nodecount);
		fprintf(file, "FORK_NAME%s", GWhosts[i].fork_name);
		fprintf(file, "LRMS_NAME%s", GWhosts[i].lrms_name);
		fprintf(file, "LRMS_TYPE%s\n", GWhosts[i].lrms_type);

		for (j = 0; j < GWhosts[i].nqueues; j++)
		{
			fprintf(file, "QUEUE_NAME[%d]%s", 
			        j, GWhosts[i].queues[j].queue_name);
			fprintf(file, "QUEUE_NODECOUNT[%d]%s", 
			        j, GWhosts[i].queues[j].queue_nodecount);
			fprintf(file, "QUEUE_FREENODECOUNT[%d]%s", 
			        j, GWhosts[i].queues[j].queue_freenodecount);
			fprintf(file, "QUEUE_MAXCPUTIME[%d]%s", 
			        j, GWhosts[i].queues[j].queue_maxcputime);
			fprintf(file, "QUEUE_STATUS[%d]%s", 
			        j, GWhosts[i].queues[j].queue_status);
			fprintf(file, "QUEUE_DISPATCHTYPE[%d]%s\n", 
			        j, GWhosts[i].queues[j].queue_dispatchtype);
		}

		fclose(file);
		
		rename(path, GWhosts[i].hostattrfile);
	}

	return 0;
}

/* gethostlist: Get GridWay host list. */
static int gethostlist(char *hostlistfile)
{
	FILE *hostlist;
	char path[DM_PATH_LEN], c;
	int i;

	sprintf(path, "%s/%s", GWhome, hostlistfile);
	if ((hostlist = fopen(path, "r")) == NULL)
	{
		error(0, errno, "fopen() failed for %s", path);
		return -1;
	}

	NGWhosts = 0;
	while ((c = fgetc(hostlist)) != EOF)
		if (c == '\n')
			NGWhosts++;

	if ((GWhosts = (dm_gwhost_t *) malloc(sizeof(dm_gwhost_t) * NGWhosts)) 
		== NULL)
	{
		error(0, errno, "malloc() failed for GWhosts");
		return -1;
	}

	fseek(hostlist, 0, 0);

	for (i = 0; i < NGWhosts; i++)
	{
		fscanf(hostlist, "%s", GWhosts[i].hostname);
		fscanf(hostlist, "%s", path);
		sprintf(GWhosts[i].hostattrfile, "%s/%s", GWhome, path);
	}
	
	fclose(hostlist);

	return 0;
}

/* gethostattrs: Get GridWay host attributes. */
static int gethostattrs()
{
	int i, n, q, slen;
	FILE *hostattr;
	char *line = NULL, *s, *ss, qstr[DM_TOK_LEN], attrname[DM_TOK_LEN];

	for (i = 0; i < NGWhosts; i++)
	{
		if ((hostattr = fopen(GWhosts[i].hostattrfile, "r")) == NULL)
		{
			error(0, errno, "fopen() failed for %s", GWhosts[i].hostattrfile);
			return -1;
		}

		GWhosts[i].nqueues = 0;
		while (getline(&line, (size_t *) &n, hostattr) != -1)
		{
			if (strstr(line, "QUEUE_NAME") != NULL)
				GWhosts[i].nqueues++;
		}

		if ((GWhosts[i].queues = (dm_gwqueue_t *) 
				  malloc(sizeof(dm_gwqueue_t) * GWhosts[i].nqueues)) == NULL)
		{
			error(0, errno, "malloc() failed for GWhosts");
			return -1;
		}

		fseek(hostattr, 0, 0);

		while (getline(&line, (size_t *) &n, hostattr) != -1)
		{
			s = NULL;
			ss = NULL;

			s = index(line, '[');

			if (strlen(line) > 1)
			{
				if (s != NULL)
				{
					s++;
					ss = index(line, ']');
					snprintf(qstr, strlen(s) - strlen(ss) + 1, "%s", s);
					q = atoi(qstr);

					snprintf(attrname, strlen(line) - strlen(s), line);

					s = index(line, '=');

					if (!strcmp(attrname, "QUEUE_NAME"))
						sprintf(GWhosts[i].queues[q].queue_name, "%s", s);
					else if (!strcmp(attrname, "QUEUE_NODECOUNT"))
						sprintf(GWhosts[i].queues[q].queue_nodecount, "%s", s);
					else if (!strcmp(attrname, "QUEUE_FREENODECOUNT"))
						sprintf(GWhosts[i].queues[q].queue_freenodecount, 
								"%s", s);
					else if (!strcmp(attrname, "QUEUE_MAXCPUTIME"))
						sprintf(GWhosts[i].queues[q].queue_maxcputime, "%s", s);
					else if (!strcmp(attrname, "QUEUE_STATUS"))
						sprintf(GWhosts[i].queues[q].queue_status, "%s", s);
					else if (!strcmp(attrname, "QUEUE_DISPATCHTYPE"))
						sprintf(GWhosts[i].queues[q].queue_dispatchtype, 
								"%s", s);
				}
				else
				{
					s = index(line, ' ');
					slen = 0;
					while ((line[slen] != ' ') && (line[slen] != '='))
						slen++;
					slen++;

					snprintf(attrname, slen, line);

					s = index(line, '=');

					if (!strcmp(attrname, "HOSTATTRFILE"))
						sprintf(GWhosts[i].hostattrfile, "%s", s);
					else if (!strcmp(attrname, "HOSTNAME"))
					{
						ss = index(s, '"');
						ss++;
						s = rindex(line, '"');
						slen = strlen(ss) - strlen(s) + 1;
						snprintf(GWhosts[i].hostname, slen, "%s", ss);
					}
					else if (!strcmp(attrname, "ARCH"))
						sprintf(GWhosts[i].arch, "%s", s);
					else if (!strcmp(attrname, "OS_NAME"))
						sprintf(GWhosts[i].os_name, "%s", s);
					else if (!strcmp(attrname, "OS_VERSION"))
						sprintf(GWhosts[i].os_version, "%s", s);
					else if (!strcmp(attrname, "CPU_MODEL"))
						sprintf(GWhosts[i].cpu_model, "%s", s);
					else if (!strcmp(attrname, "CPU_MHZ"))
						sprintf(GWhosts[i].cpu_mhz, "%s", s);
					else if (!strcmp(attrname, "CPU_FREE"))
						sprintf(GWhosts[i].cpu_free, "%s", s);
					else if (!strcmp(attrname, "CPU_SMP"))
						sprintf(GWhosts[i].cpu_smp, "%s", s);
					else if (!strcmp(attrname, "NODECOUNT"))
						sprintf(GWhosts[i].nodecount, "%s", s);
					else if (!strcmp(attrname, "FORK_NAME"))
						sprintf(GWhosts[i].fork_name, "%s", s);
					else if (!strcmp(attrname, "LRMS_NAME"))
						sprintf(GWhosts[i].lrms_name, "%s", s);
					else if (!strcmp(attrname, "LRMS_TYPE"))
						sprintf(GWhosts[i].lrms_type, "%s", s);
				}
			}
		}

		fclose(hostattr);
	}

	if (line)
		free(line);

	return 0;
}

/* gwhostfreemem: Free memory for GWhosts. */
void gwhostfreemem()
{
	int i;

	for (i = 0; i < NGWhosts; i++)
		free(GWhosts[i].queues);
	free(GWhosts);
}

/* getgwhostfile: Read GW cfg and find IM_MAD host file. */
static int getgwhostfile(char *file)
{
	FILE *gwcfg;
	char gwcfgpath[DM_PATH_LEN], *line = NULL, c, *s1 = NULL, *s2 = NULL;
	int n;

	sprintf(gwcfgpath, "%s/etc/gwd.conf", GWhome);
	if ((gwcfg = fopen(gwcfgpath, "r")) == NULL)
	{
		error(0, errno, "fopen() failed for %s", gwcfgpath);
		return -1;
	}

	while ((c = fgetc(gwcfg)) != EOF)
	{
		if (c == '\n')
		{
			if ((c = fgetc(gwcfg)) == 'I')
			{
				getline(&line, (size_t *) &n, gwcfg);

				s1 = rindex(line, ' ');
				s1++;
				s2 = index(s1, ':');
				snprintf(file, strlen(s1) - strlen(s2) + 1, "%s", s1);
				fclose(gwcfg);

				if (line)
					free(line);

				return 0;
			}
		}
	}

	fclose(gwcfg);
	return -1;
}
