/*
 * dm_client.c: Dcsmon client functions
 *
 * (c) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include "dcsmon.h"
#include "dm_client.h"
#include "tcp.h"
#include "error.h"

/* dm_getnnodes: Get number of nodes. */
int dm_getnnodes(char *host)
{
	SOCKET sock;
	int nnodes;

	if ((sock = tcp_client(host, DCSMON_PORT)) < 0)
		return -1;

	if (sendn(sock, NNODES_RQST, DM_SERV_MSG_LEN) != DM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	if (recvn(sock, (char *) &nnodes, sizeof(nnodes)) != sizeof(nnodes))
	{
		close(sock);
		error(0, errno, "recvn() failed");
		return -1;
	}

	close(sock);

	return nnodes;
}

/* dm_getppn: Get number of processors per node. */
int dm_getppn(char *host)
{
	SOCKET sock;
	int ppn;

	if ((sock = tcp_client(host, DCSMON_PORT)) < 0)
		return -1;

	if (sendn(sock, PPN_RQST, DM_SERV_MSG_LEN) != DM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	if (recvn(sock, (char *) &ppn, sizeof(ppn)) != sizeof(ppn))
	{
		close(sock);
		error(0, errno, "recvn() failed");
		return -1;
	}

	close(sock);

	return ppn;
}

/* dm_getfreenodes: Get number of free nodes. */
int dm_getfreenodes(char *host)
{
	SOCKET sock;
	int freenodes;

	if ((sock = tcp_client(host, DCSMON_PORT)) < 0)
		return -1;

	if (sendn(sock, FREENODES_RQST, DM_SERV_MSG_LEN) != DM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	if (recvn(sock, (char *) &freenodes, sizeof(freenodes)) != 
		sizeof(freenodes))
	{
		close(sock);
		error(0, errno, "recvn() failed");
		return -1;
	}

	close(sock);

	return freenodes;
}

/* dm_getnprocs: get number of processors. */
int dm_getnprocs(char *host)
{
	SOCKET sock;
	int nprocs;

	if ((sock = tcp_client(host, DCSMON_PORT)) < 0)
		return -1;

	if (sendn(sock, NPROCS_RQST, DM_SERV_MSG_LEN) != DM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	if (recvn(sock, (char *) &nprocs, sizeof(nprocs)) != sizeof(nprocs))
	{
		close(sock);
		error(0, errno, "recvn() failed");
		return -1;
	}

	close(sock);

	return nprocs;
}

/* dm_getfreeprocs: Get number of free processors. */
int dm_getfreeprocs(char *host)
{
	SOCKET sock;
	int freeprocs;

	if ((sock = tcp_client(host, DCSMON_PORT)) < 0)
		return -1;

	if (sendn(sock, FREEPROCS_RQST, DM_SERV_MSG_LEN) != DM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	if (recvn(sock, (char *) &freeprocs, sizeof(freeprocs)) != 
		sizeof(freeprocs))
	{
		close(sock);
		error(0, errno, "recvn() failed");
		return -1;
	}

	close(sock);

	return freeprocs;
}

/* dm_getnjobs: Get number of jobs*/
int dm_getnjobs(char *host, int *nrunning, int *nqueued)
{
	SOCKET sock;

	if ((sock = tcp_client(host, DCSMON_PORT)) < 0)
		return -1;

	if (sendn(sock, NJOBS_RQST, DM_SERV_MSG_LEN) != DM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	if (recvn(sock, (char *) nrunning, sizeof(*nrunning)) != sizeof(*nrunning))
	{
		close(sock);
		error(0, errno, "recvn() failed");
		return -1;
	}

	if (recvn(sock, (char *) nqueued, sizeof(*nqueued)) != sizeof(*nqueued))
	{
		close(sock);
		error(0, errno, "recvn() failed");
		return -1;
	}

	close(sock);

	return 0;
}

/* dm_getwalltime: Get total walltime over the jobs. */
int dm_getwalltime(char *host)
{
	SOCKET sock;
	int walltime;

	if ((sock = tcp_client(host, DCSMON_PORT)) < 0)
		return -1;

	if (sendn(sock, WALLTIME_RQST, DM_SERV_MSG_LEN) != DM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	if (recvn(sock, (char *) &walltime, sizeof(walltime)) != sizeof(walltime))
	{
		close(sock);
		error(0, errno, "recvn() failed");
		return -1;
	}

	close(sock);

	return walltime;
}

/* dm_getprocmap: Get map of free processors. */
procmap_t *dm_getprocmap(char *host, int *nnodes)
{
	SOCKET sock;
	procmap_t *procmap;

	if ((sock = tcp_client(host, DCSMON_PORT)) < 0)
		return NULL;

	if (sendn(sock, PROCMAP_RQST, DM_SERV_MSG_LEN) != DM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return NULL;
	}

	if (recvn(sock, (char *) nnodes, sizeof(*nnodes)) != sizeof(*nnodes))
	{
		close(sock);
		error(0, errno, "recvn() failed");
		return NULL;
	}

	if ((procmap = (procmap_t *) malloc(sizeof(procmap_t) * (*nnodes))) 
		== NULL)
	{
		close(sock);
		error(0, errno, "malloc() failed");
		return NULL;
	}

	if (recvn(sock, (char *) procmap, sizeof(procmap_t) * (*nnodes)) 
		!= sizeof(procmap_t) * (*nnodes))
	{
		close(sock);
		error(0, errno, "recvn() failed");
		return NULL;
	}

	close(sock);

	return procmap;
}

/* dm_getcpufreq: Get value of CPU frequency. */
int dm_getcpufreq(char *host)
{
	SOCKET sock;
	int cpufreq;

	if ((sock = tcp_client(host, DCSMON_PORT)) < 0)
		return -1;

	if (sendn(sock, CPUFREQ_RQST, DM_SERV_MSG_LEN) != DM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	if (recvn(sock, (char *) &cpufreq, sizeof(cpufreq)) != sizeof(cpufreq))
	{
		close(sock);
		error(0, errno, "recvn() failed");
		return -1;
	}

	close(sock);

	return cpufreq;
}

/* dm_getarch: Get system architecture (i686, x86_64, etc). */
int dm_getarch(char *host, char *arch)
{
	SOCKET sock;

	if ((sock = tcp_client(host, DCSMON_PORT)) < 0)
		return -1;

	if (sendn(sock, ARCH_RQST, DM_SERV_MSG_LEN) != DM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	if (recvn(sock, arch, DM_ARCH_LEN) != DM_ARCH_LEN)
	{
		close(sock);
		error(0, errno, "recvn() failed");
		return -1;
	}

	close(sock);

	return 0;
}
