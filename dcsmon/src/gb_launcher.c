#include <stdio.h>
#include <string.h>

#include "gbroker.h"
#include "nm_client.h"
#include "dm_client.h"
#include "gb_launcher.h"
#include "error.h"

/*
 * Globus Toolkit functions
 */

/* globusrun: Globus Toolkit globusrun launcher. */
int globusrun(char *resource, char *job, char *contact)
{
	char cmd[GB_CMD_LEN], tok[GB_TOKEN_LEN];
	FILE *pipe;
	int i;

	sprintf(cmd, "globusrun -b -r %s -f %s", resource, job);

	if ((pipe = popen(cmd, "r")) == NULL)
	{
		error(0, 0, "popen() failed");
		return -1;
	}

	for (i = 0; i < 6; i++)
		fscanf(pipe, "%s", tok);

	fscanf(pipe, "%s", contact);

	/* Find word 'failed' in the output. */
	while (fscanf(pipe, "%s", tok) != EOF)
		if (!strcmp(tok, "GLOBUS_GRAM_PROTOCOL_JOB_STATE_FAILED"))
			return -1;

	pclose(pipe);

	return 0;
}

/* globuskill: Kill job by Globus Toolkit globusrun. */
int globuskill(char *contact)
{
	char cmd[GB_CMD_LEN], tok[GB_TOKEN_LEN];
	FILE *pipe;

	sprintf(cmd, "globusrun -k %s", contact);

	if ((pipe = popen(cmd, "r")) == NULL)
	{
		error(0, 0, "popen() failed");
		return -1;
	}

	/* Find word 'failed' in the output. */
	while (fscanf(pipe, "%s", tok) != EOF)
		if (!strcmp(tok, "Error"))
			return -1;

	pclose(pipe);

	return 0;
}

/* globusrun_status: Get status of the job by contact. */
int globusrun_status(char *contact, char *status)
{
	char cmd[GB_CMD_LEN];
	FILE *pipe;

	sprintf(cmd, "globusrun -status %s", contact);
	if ((pipe = popen(cmd, "r")) == NULL)
	{
		error(0, 0, "popen() failed");
		return -1;
	}
	
	fscanf(pipe, "%s", status);

	pclose(pipe);

	return 0;
}

/* globus-url-copy; Globus Toolkit globus-url-copy launcher. */
int globus_url_copy(char *src, char *dest)
{
	char cmd[GB_CMD_LEN];
	FILE *pipe;

	sprintf(cmd, "globus-url-copy %s %s", src, dest);

	if ((pipe = popen(cmd, "r")) == NULL)
	{
		error(0, 0, "popen() failed");
		return -1;
	}

	if (fscanf(pipe, "%s", cmd) != EOF)
		return -1;

	pclose(pipe);

	return 0;
}

/*
 * Netmon functions.
 */

/* getnettime: Return time to send msg with 'size' from hsot1 to host2. */
double getnettime(char *host1, char *host2, double size)
{
	double time;

	/*
	pthread_mutex_lock(&Netmon_mutex);
	*/
	time = nm_gettime(host1, host2, size);
	/*
	pthread_mutex_unlock(&Netmon_mutex);
	*/

	return time;
}

/* getnettimeforfile: Return time to send file from hsot1 to host2. */
double getnettimeforfile(char *host1, char *host2, char *file)
{
	double time;

	/*
	pthread_mutex_lock(&Netmon_mutex);
	*/
	time = nm_gettimeforfile(host1, host2, file);
	/*
	pthread_mutex_unlock(&Netmon_mutex);
	*/

	return time;
}

/*
 * Dcsmon functions.
 */

/* getprocmap: Get map of free processors on host. */
procmap_t *getprocmap(char *host, int *nnodes)
{
	return dm_getprocmap(host, nnodes);
}

/* getnjobs: Get number of jobs (running and queued). */
int getnjobs(char *host, int *nrunning, int *nqueued)
{
	return dm_getnjobs(host, nrunning, nqueued);
}

/* getnnodes: Get total number of nodes. */
int getnnodes(char *host)
{
	return dm_getnnodes(host);
}

/* getnnodes: Get number of processors per node. */
int getppn(char *host)
{
	return dm_getppn(host);
}

/* getfreenodes: Get number of free nodes. */
int getfreenodes(char *host)
{
	return dm_getfreenodes(host);
}

/* getnprocs: Get total number of processors. */
int getnprocs(char *host)
{
	return dm_getnprocs(host);
}

/* getfreeprocs: Get number of free processors. */
int getfreeprocs(char *host)
{
	return dm_getfreeprocs(host);
}

/* getwalltime: Get queue walltime. */
int getwalltime(char *host)
{
	return dm_getwalltime(host);
}
