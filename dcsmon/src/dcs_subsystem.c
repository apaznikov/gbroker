/*
 * dm_client.c: Dcsmon client functions
 *
 * (c) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <getopt.h>

#include "dcsmon.h"
#include "dm_torque.h"
#include "tcp.h"
#include "error.h"

/* getnnodesppn: Get count of nodes and ppn from job rank for host. */
static int getnodesppn(char *pbsnodes_path, int rank, int *nodes, int *ppn);

/* getprocmap: Get map of free processors on host. */
procmap_t *getprocmap(char *host, int *nnodes);

/* getnnodes: Get number of processors per node. */
int getppn(char *host);

/* cmpint: Comparison function for qsort. */
static int cmpproc(const void *a, const void *b);

/* check_usage: */
static int check_usage(int argc, char **argv, int *count, char *pbsnodes_path);

int main(int argc, char **argv)
{
	int count, nodes, ppn;
	char pbsnodes_path[DM_PATH_LEN];

	if (check_usage(argc, argv, &count, pbsnodes_path) < 0)
		return 1;

	if (access(pbsnodes_path, X_OK) < 0)
	{
		printf("Error 2: path %s is invalid!\n", pbsnodes_path);
		return 1;
	}

	if (getnodesppn(pbsnodes_path, count, &nodes, &ppn) < 0)
	{
		printf("Error 3: getnodesppn() failed for %d\n", count);
		return 1;
	}

	printf("%d %d\n", nodes, ppn);

	/* Debug */
	/*
	fp = fopen("/tmp/ddd", "w");
	fprintf(fp, "%d %d %d\n", count, nodes, ppn);
	fclose(fp);
	*/

	return 0;
}

/* getnnodesppn: Get count of nodes and ppn from job rank for host. */
static int getnodesppn(char *pbsnodes_path, int rank, int *nodes, int *ppn)
{
	int allnodes, p, nnodes, inode, hostppn, i;
	procmap_t *procmap;

	if ((procmap = torque_getprocmap(pbsnodes_path, &allnodes)) == NULL)
	{
		printf("torque_getprocmap() failed: %s\n", strerror(errno));
		return -1;
	}
	
	#ifdef _SCHEDULER_DEBUG
	printf("SCHEDULER: %s:    ", shname);
	for (i = 0; i < allnodes; i++)
		printf(stderr, "%3d", procmap[i]);
	#endif

	if (torque_getppn(pbsnodes_path, &hostppn) < 0)
	{
		printf("torque_getppn() failed: %s\n", strerror(errno));
		return -1;
	}

	if (allnodes * hostppn < rank)
		return -1;

	qsort(procmap, allnodes, sizeof(*procmap), cmpproc);

	/* Find symmetric subsystem on free processors. */
	for (p = hostppn; p > 0; p--)
	{
		if (rank % p == 0)
		{
			nnodes = 0;

			for (inode = 0; inode < allnodes; inode++)
			{
				if (procmap[inode] >= p)
				{
					nnodes++;
					if (nnodes >= rank / p)
					{
						*nodes = nnodes;
						*ppn = p;
						return 0;
					}
				}
			}
		}
	}

	/* 
	 * Can't allocate subsystem on free processors.
	 * So let all processors available and do the same. 
	 */

	for (i = 0; i < allnodes; i++)
		procmap[i] = hostppn;

	for (p = hostppn; p > 0; p--)
	{
		if (rank % p == 0)
		{
			nnodes = 0;

			for (inode = 0; inode < allnodes; inode++)
			{
				if (procmap[inode] >= p)
				{
					nnodes++;
					if (nnodes >= rank / p)
					{
						*nodes = nnodes;
						*ppn = p;
						return 0;
					}
				}
			}
		}
	}

	return -1;
}

/* cmpint: Comparison function for qsort. */
static int cmpproc(const void *a, const void *b)
{
    return ( *(procmap_t*)a - *(procmap_t*)b );
}

/* check_usage: */
static int check_usage(int argc, char **argv, int *count, char *pbsnodes_path)
{
	int this_option_optind = optind ? optind : 1, option_index = 0;
	char opt;
	int countflag = 0, pathflag = 0;

	static struct option long_options[] = { {"--pbs-bindir", 1, 0, 0},
	                                        {"--count", 1, 0, 0} };

	for (;;) 
	{
		this_option_optind = optind ? optind : 1;
		opt = getopt_long(argc, argv, "pc", long_options, &option_index);
		if (opt == -1)
			break;
		switch (opt) 
		{
			case 'p':
				sprintf(pbsnodes_path, "%s/pbsnodes", argv[optind]);
				pathflag = 1;
				break;
			case 'c':
				*count = atoi(argv[optind]);
				countflag = 1;
				break;
			case '?':
				printf("Unrecognized option: -%c\n", optopt);
				goto lbl_error;
		}
	}

	if ((!countflag) || (!pathflag))
		goto lbl_error;

	return 0;

lbl_error:
	printf("Error 1: checkusage() failed\n");
	printf("Usage: dcs_subsystem -p <pbs-path> -c <nprocs>\n");
	printf("-p <directory>\t\tPBS Torque binary path\n"); 
	printf("-c <nprocs>\t\tnumber of processors\n"); 
	return -1;
}
