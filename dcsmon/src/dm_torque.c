/*
 * dm_torque.c: Functions for accumulate info from TORQUE.
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>

#include "dcsmon.h"
#include "dm_torque.h"
#include "error.h"

int Cpufreq = 0;

/* getnodecpufreq: Get CPU frequency of node. */
static int getnodecpufreq(char *host, int *cpufreq);

/* torque_getprocmap: Get info about free/busy processors. */
procmap_t *torque_getprocmap(char *pbsnodes_path, int *nnodes)
{
	char exec_cmd[DM_PATH_LEN], tok[DM_TOKEN_LEN], c;
	FILE *pipe;
	int inode, ppnflag, downstate;
	procmap_t *procmap;

	if (torque_getallnodes(pbsnodes_path, nnodes) < 0)
	{
		error(0, errno, "torque_getallnodes() failed");
		return NULL;
	}

	if ((procmap = malloc(sizeof(procmap_t) * (*nnodes))) == NULL)
	{
		error(0, errno, "malloc() failed");
		return NULL;
	}

	sprintf(exec_cmd, "%s", pbsnodes_path);
	if ((pipe = popen(exec_cmd, "r")) == NULL)
	{
		error(0, errno, "Can't execute %s", exec_cmd);
		return NULL;
	}

	inode = -1;
	ppnflag = 0;
	downstate = 1;
	/* Make map of free/busy processes. */
	for (;;)
	{
		if (fscanf(pipe, "%s", tok) == EOF)
			break;

		if (!strcmp(tok, "state"))
		{
			fscanf(pipe, "%s", tok);
			fscanf(pipe, "%s", tok);

			if (!strcmp(tok, "down"))
				downstate = 1;
			else
				downstate = 0;
		}
		else if (!downstate)
		{
			if (!strcmp(tok, "np"))
			{
				if (!ppnflag)
				{
					int i, ppn;

					fscanf(pipe, "%s", tok);
					fscanf(pipe, "%s", tok); /* np value */

					ppn = atoi(tok);

					for (i = 0; i < *nnodes; i++)
						procmap[i] = ppn;

					ppnflag = 1;
				}
				inode++;
			}
			else if (!strcmp(tok, "jobs"))
				/* Jobs running on current node. */
			{
				fscanf(pipe, "%s", tok);
				fscanf(pipe, "%s", tok); 

				procmap[inode]--;
				while ((c = fgetc(pipe)) != '\n')
				{
					if (c == ' ')
						procmap[inode]--;
				}
			}
		}
	}

	pclose(pipe);

	return procmap;
}

/* torque_getallnodes: Get total number of nodes. */
int torque_getallnodes(char *pbsnodes_path, int *nnodes)
{
	char exec_cmd[DM_PATH_LEN];
	FILE *pipe;

	sprintf(exec_cmd, "%s -l up", pbsnodes_path);
	if ((pipe = popen(exec_cmd, "r")) == NULL)
	{
		error(0, errno, "Can't execute %s", exec_cmd);
		return -1;
	}

	*nnodes = 0;
	while (fgetc(pipe) != EOF)
	{
		while (fgetc(pipe) != '\n');
		(*nnodes)++;
	}

	pclose(pipe);

	return 0;
}


/* torque_getppn: Get number of processors per node. */
int torque_getppn(char *pbsnodes_path, int *ppn)
{
	char exec_cmd[DM_PATH_LEN];
	char tok[DM_TOKEN_LEN];
	FILE *pipe;

	sprintf(exec_cmd, "%s", pbsnodes_path);
	if ((pipe = popen(exec_cmd, "r")) == NULL)
	{
		error(0, errno, "Can't execute %s", exec_cmd);
		return -1;
	}

	do {
		fscanf(pipe, "%s", tok);
	} while (strcmp(tok, "np"));

	fscanf(pipe, "%s", tok);
	fscanf(pipe, "%s", tok); /* np value */

	*ppn = atoi(tok);

	pclose(pipe);

	return 0;
}

/* torque_getnprocs: Get number of free processors. */
int torque_getfreeprocs(char *pbsnodes_path, int *nprocs)
{
	int nbusy = 0;
	char tok[DM_TOKEN_LEN];
	FILE *pipe;
	char c;
	char exec_cmd[DM_PATH_LEN];

	torque_getallprocs(pbsnodes_path, nprocs);

	sprintf(exec_cmd, "%s", pbsnodes_path);
	if ((pipe = popen(exec_cmd, "r")) == NULL)
	{
		error(0, errno, "Can't execute %s", exec_cmd);
		return -1;
	}

	for (;;)
	{
		do 
		{
			if (fscanf(pipe, "%s", tok) == EOF)
			{
				*nprocs -= nbusy;
				pclose(pipe);
				return 0;
			}
		} while (strcmp(tok, "jobs"));
		fscanf(pipe, "%s", tok);
		fscanf(pipe, "%s", tok); 

		nbusy++;
		while ((c = fgetc(pipe)) != '\n')
			if (c == ' ')
				nbusy++;
	}

	pclose(pipe);

	return 0;
}

/* torque_getnprocs: Get total number of processors. */
int torque_getallprocs(char *pbsnodes_path, int *nprocs)
{
	int nnodes, ppn;

	torque_getallnodes(pbsnodes_path, &nnodes);
	torque_getppn(pbsnodes_path, &ppn);
	*nprocs = nnodes * ppn;

	return 0;
}

/* torque_getfreenodes: Get upper bound to serve queue (walltime). */
int torque_getfreenodes(char *pbsnodes_path, int *nnodes)
{
	char exec_cmd[DM_PATH_LEN];
	FILE *pipe;

	sprintf(exec_cmd, "%s -l free", pbsnodes_path);
	if ((pipe = popen(exec_cmd, "r")) == NULL)
	{
		error(0, errno, "Can't execute %s", exec_cmd);
		return -1;
	}

	*nnodes = 0;
	while (fgetc(pipe) != EOF)
	{
		while (fgetc(pipe) != '\n');
		(*nnodes)++;
	}

	pclose(pipe);

	return 0;
}

/* torque_getnjobs: Get number of jobs running and waiting in queue. */
int torque_getnjobs(char *qstat_path, int *nrunning, int *nqueued)
{
	FILE *pipe;
	char exec_cmd[DM_PATH_LEN];
	char tok1[DM_TOKEN_LEN], tok2[DM_TOKEN_LEN], tok3[DM_TOKEN_LEN];

	sprintf(exec_cmd, "%s -q", qstat_path);
	if ((pipe = popen(exec_cmd, "r")) == NULL)
	{
		error(0, errno, "can't execute %s", exec_cmd);
		return -1;
	}

	for (;;)
	{
		if (fscanf(pipe, "%s", tok1) == EOF)
		{
			*nrunning = atoi(tok2);
			*nqueued = atoi(tok3);
			break;
		}

		if (fscanf(pipe, "%s", tok2) == EOF)
		{
			*nrunning = atoi(tok3);
			*nqueued = atoi(tok1);
			break;
		}

		if (fscanf(pipe, "%s", tok3) == EOF)
		{
			*nrunning = atoi(tok1);
			*nqueued = atoi(tok2);
			break;
		}
	}

	pclose(pipe);

	return 0;
}

/* torque_getqueuewalltime: Get summary walltime. */
int torque_getqueuewalltime(char *qstat_path, int *time)
{
	enum { N_FIRST_LINES = 4, N_SPACES_BEFORE_RTIME = 8, HHLEN = 5, MMLEN = 5 };
	FILE *pipe;
	int i, time_req, time_elap;
	char c, hh[HHLEN], mm[MMLEN], exec_cmd[DM_PATH_LEN];

	sprintf(exec_cmd, "%s -a", qstat_path);
	if ((pipe = popen(exec_cmd, "r")) == NULL)
	{
		error(0, errno, "Can't execute %s", exec_cmd);
		return -1;
	}

	*time = 0;
	
	if (fgetc(pipe) == EOF)
	{
		pclose(pipe);
		return 0;
	}

	/* Skip first lines in header. */
	for (i = 0; i < N_FIRST_LINES; i++)
		while (fgetc(pipe) != '\n');

	while ((c = fgetc(pipe)) != EOF)
	{
		/* Skip columns before time. */
		for (i = 0; i < N_SPACES_BEFORE_RTIME; i++)
		{
			while (!isspace(c = fgetc(pipe)));
			while (isspace(c = fgetc(pipe)));
		}

		/* Parse req time. */
		memset(hh, (int) '\0', HHLEN);
		memset(mm, (int) '\0', MMLEN);

		hh[0] = c;
		i = 1;
		while (isdigit(c = fgetc(pipe)))
		{
			hh[i] = c;
			i++;
		}

		mm[0] = fgetc(pipe);
		i = 1;
		while (isdigit(c = fgetc(pipe)))
		{
			mm[i] = c;
			i++;
		}
		time_req = atoi(hh) * 3600 + atoi(mm) * 60;
		/* fprintf(stderr, "req %s:%s(%d)\t", hh, mm, time_req); */

		c = fgetc(pipe);	
		if ((c == 'R') || (c == 'Q'))
		{	
			fgetc(pipe);
			memset(hh, (int) '\0', HHLEN);
			memset(mm, (int) '\0', MMLEN);

			/* Parse elap time. */
			if (isdigit(c = fgetc(pipe)))
			{
				hh[0] = c;
				i = 1;
				while (isdigit(c = fgetc(pipe)))
				{
					hh[i] = c;
					i++;
				}

				mm[0] = fgetc(pipe);
				i = 1;
				while (isdigit(c = fgetc(pipe)))
				{
					mm[i] = c;
					i++;
				}
			}

			time_elap = atoi(hh) * 3600 + atoi(mm) * 60;
			*time += time_req - time_elap;

			/* fprintf(stderr, "elap %s:%s(%d)\n", hh, mm, time_elap); */
		}

		while ((fgetc(pipe) != '\n') && (fgetc(pipe) != EOF));
	}

	pclose(pipe);

	return 0;
}

/* getcpufreq: Get CPU frequency. */
int getcpufreq(int *cpufreq)
{
	*cpufreq = Cpufreq;
	return 0;
}

/* cpufreqinit: Initialize CPU frequency value. */
int cpufreqinit(char *pbsnodes_path)
{
	FILE *pipe;
	char exec_cmd[DM_PATH_LEN], c, host[DM_HNAME_LEN], state[DM_TOKEN_LEN];

	sprintf(exec_cmd, "%s", pbsnodes_path);
	if ((pipe = popen(exec_cmd, "r")) == NULL)
	{
		error(0, errno, "Can't execute %s", exec_cmd);
		return -1;
	}

	fscanf(pipe, "%s", host);
	fscanf(pipe, "%s", state);
	fscanf(pipe, "%s", state);
	fscanf(pipe, "%s", state);
	if (strcmp(state, "down"))
	{
		pclose(pipe);
		getnodecpufreq(host, &Cpufreq);
		return 0;
	}

	while ((c = fgetc(pipe)) != EOF)
	{
		if (c == '\n')
		{
			if (fgetc(pipe) == '\n')
			{
				fscanf(pipe, "%s", host);
				fscanf(pipe, "%s", state);
				fscanf(pipe, "%s", state);
				fscanf(pipe, "%s", state);

				if (strcmp(state, "down"))
				{
					pclose(pipe);
					if (getnodecpufreq(host, &Cpufreq) < 0)
					{
						error(0, 0, "getnodecpufreq() failed");
						return -1;
					}
					return 0;
				}
			}
		}
	}

	pclose(pipe);
	return -1;
}

/* getnodecpufreq: Get CPU frequency of node. */
static int getnodecpufreq(char *host, int *cpufreq)
{
	char exec_cmd[DM_PATH_LEN], tok[DM_TOKEN_LEN];
	FILE *pipe;

	sprintf(exec_cmd, "ssh %s cat /proc/cpuinfo", host);
	if ((pipe = popen(exec_cmd, "r")) == NULL)
	{
		error(0, errno, "Can't execute %s", exec_cmd);
		return -1;
	}

	while (fscanf(pipe, "%s", tok) != EOF)
	{
		if (!strcmp(tok, "model"))
		{
			while (fgetc(pipe) != '@');
			fscanf(pipe, "%s", tok);
			*cpufreq = atof(tok) * 1000;
			return 0;
		}
	}

	pclose(pipe);

	return -1;
}
