/*
 * dcsmon.c: Main dcsmon module
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <pthread.h>
#include <string.h>
#include <getopt.h>

#include "error.h"
#include "daemon.h"
#include "dm_torque.h"
#include "dm_sysinfo.h"
#include "dcsmon.h"
#include "dm_gw.h"

/*
#define _DEBUG
#define _CPUFREQ_ENABLE
*/

char Dcsmon_home[DM_PATH_LEN];
char Thishost[DM_HNAME_LEN];
char GWhome[DM_PATH_LEN];
char Qstat_path[DM_PATH_LEN], Pbsnodes_path[DM_PATH_LEN];
int  GWflag = 0;
char Lockfile[BUFLEN];

static void server(void);
static void *client(void *arg);

static void checkusage(int argc, char **argv);
static void memfree(void);
static void set_sighandlers(void);
static void set_thishost(void);
static void sigterm_handler(int signo);
static void sigint_handler(int signo);
static void set_sighandlers(void);
static void common_exit(void);

int main(int argc, char **argv)
{
	pthread_t client_tid;

	checkusage(argc, argv);
	if (initlog(Dcsmon_home) < 0)
	{
		fprintf(stderr, "initlog() failed");
		return -1;
	}

	/* Make sure, that no daemons copy has been already started. */
	if (isrunning(Dcsmon_home))
		error(1, 0, "daemon has been already started");

	#ifndef _DEBUG
	daemonize();
	#endif

	set_sighandlers();
	set_thishost();

	if (atexit(common_exit) != 0)
		error(1, errno, "atexit() failed");

	#ifdef _CPUFREQ_ENABLE
	if (cpufreqinit(Pbsnodes_path) < 0)
		error(0, 0, "cpufreqinit() failed");
	#endif 

	if (GWflag)
	{
		if ((pthread_create(&client_tid, NULL, client, NULL) != 0))
		{
			memfree();
			error(1, errno, "pthread_create() failed");
		}
	}

	server();

	return 0;
}

/* server_thread: Listen port, process clients. */
static void server(void)
{
	SOCKET sserver, sclient;
	struct sockaddr_in peer;
	socklen_t peerlen = sizeof(peer);
	char serv_msg[DM_SERV_MSG_LEN];

	int nnodes;              /* Number of nodes. */
	int ppn;                 /* Number of processors per node. */
	int freenodes;           /* Numbver of free nodes. */
	int nprocs;              /* Number of processors. */
	int freeprocs;           /* Number of free processors. */
	int walltime;            /* Total queue walltime in sec. */
	int nrunning;            /* Number of running jobs. */
	int nqueued;             /* Number of jobs waiting in queue. */
	int cpufreq;             /* CPU frequency. */
	char arch[DM_ARCH_LEN];  /* System architecture. */
	procmap_t *procmap;      /* Map of free/busy processors. */

	sserver = tcp_server(Thishost, DCSMON_PORT);

	for (;;)
	{
		if ((sclient = accept(sserver, (struct sockaddr *) &peer, 
		                      (socklen_t *) &peerlen)) < 0)
			error(0, errno, "accept() failed");

		/* Receive request from client. */

		if (recvn(sclient, serv_msg, DM_SERV_MSG_LEN) != DM_SERV_MSG_LEN)
		{
			close(sclient);
			error(0, errno, "recvn() failed");
		}

		if (!strncmp(serv_msg, NNODES_RQST, DM_SERV_MSG_LEN))
		{
			if (torque_getallnodes(Pbsnodes_path, &nnodes) < 0)
				error(0, errno, "torque_getallnodes() failed");

			if (sendn(sclient, (char *) &nnodes, sizeof(nnodes)) 
				!= sizeof(nnodes)) 
				error(0, errno, "sendn() failed");
		}

		else if (!strncmp(serv_msg, PPN_RQST, DM_SERV_MSG_LEN))
		{
			if (torque_getppn(Pbsnodes_path, &ppn) < 0)
				error(0, errno, "torque_getallnodes() failed");

			if (sendn(sclient, (char *) &ppn, sizeof(ppn)) != sizeof(ppn)) 
				error(0, errno, "sendn() failed");
		}

		else if (!strncmp(serv_msg, FREENODES_RQST, DM_SERV_MSG_LEN))
		{
			if (torque_getfreenodes(Pbsnodes_path, &freenodes) < 0)
				error(0, errno, "torque_getfreenodes() failed");

			if (sendn(sclient, (char *) &freenodes, sizeof(freenodes)) != 
				sizeof(freenodes)) 
				error(0, errno, "sendn() failed");
		}

		else if (!strncmp(serv_msg, NPROCS_RQST, DM_SERV_MSG_LEN))
		{
			if (torque_getallprocs(Pbsnodes_path, &nprocs) < 0)
				error(0, errno, "torque_getallprocs() failed");

			if (sendn(sclient, (char *) &nprocs, sizeof(nprocs)) 
				!= sizeof(nprocs)) 
				error(0, errno, "sendn() failed");
		}

		else if (!strncmp(serv_msg, FREEPROCS_RQST, DM_SERV_MSG_LEN))
		{
			if (torque_getfreeprocs(Pbsnodes_path, &freeprocs) < 0)
				error(0, errno, "torque_getfreeprocs() failed");

			if (sendn(sclient, (char *) &freeprocs, sizeof(freeprocs)) 
				!= sizeof(freeprocs)) 
				error(0, errno, "sendn() failed");
		}

		else if (!strncmp(serv_msg, WALLTIME_RQST, DM_SERV_MSG_LEN))
		{
			if (torque_getqueuewalltime(Qstat_path, &walltime) < 0)
				error(0, errno, "torque_getqueuewalltime() failed");
			else
			{
				if (sendn(sclient, (char *) &walltime, sizeof(walltime)) 
					!= sizeof(walltime)) 
					error(0, errno, "sendn() failed");
			}
		}

		else if (!strncmp(serv_msg, NJOBS_RQST, DM_SERV_MSG_LEN))
		{
			if (torque_getnjobs(Qstat_path, &nrunning, &nqueued) < 0)
				error(0, errno, "torque_getnjobs() failed");
			else
			{
				if (sendn(sclient, (char *) &nrunning, sizeof(nrunning)) 
					!= sizeof(nrunning)) 
					error(0, errno, "sendn() failed");
				if (sendn(sclient, (char *) &nqueued, sizeof(nqueued)) 
					!= sizeof(nqueued)) 
					error(0, errno, "sendn() failed");
			}
		}

		else if (!strncmp(serv_msg, PROCMAP_RQST, DM_SERV_MSG_LEN))
		{
			if ((procmap = torque_getprocmap(Pbsnodes_path, &nnodes)) == NULL)
				error(0, errno, "torque_getprocmap() failed");
			else
			{
				if (sendn(sclient, (char *) &nnodes, sizeof(nnodes)) 
					!= sizeof(nnodes)) 
					error(0, errno, "sendn() failed");

				if (sendn(sclient, (char *) procmap, nnodes * sizeof(procmap_t))
					!= nnodes * sizeof(procmap_t)) 
					error(0, errno, "sendn() failed");

				free(procmap);
			}
		}

		else if (!strncmp(serv_msg, CPUFREQ_RQST, DM_SERV_MSG_LEN))
		{
			if (getcpufreq(&cpufreq) < 0)
				error(0, errno, "getcpufreq() failed");
			else
			{
				if (sendn(sclient, (char *) &cpufreq, sizeof(cpufreq)) 
					!= sizeof(cpufreq)) 
					error(0, errno, "sendn() failed");
			}
		}

		else if (!strncmp(serv_msg, ARCH_RQST, DM_SERV_MSG_LEN))
		{
			if (getarch(arch) < 0)
				error(0, errno, "getarch() failed");
			else
			{
				if (sendn(sclient, arch, DM_ARCH_LEN) != DM_ARCH_LEN) 
					error(0, errno, "sendn() failed");
			}
		}

		close(sclient);
	}
}

/* client_thread: Connect to remote dcsmon daemon, get info. */
static void *client(void *arg)
{
	if (gwreadhosts() < 0)
	{
		error(0, 0, "readgwhosts() failed");
		return NULL;
	}

	sleep(DM_GW_TIMEOUT); /* wait for other demons start */

	for (;;)
	{
		if (gwattrupdate() < 0)
			error(0, 0, "gwattrupdate() failed");
		
		if (gwwriteattrs() < 0)
			error(0, 0, "gwwriteattrs() failed");

		sleep(DM_GW_TIMEOUT);
	}

	return NULL;
}


/* checkusage: Check command line usage. */
static void checkusage(int argc, char **argv)
{
	char *dcsmon_home_tmp, *gwhome_tmp;
	FILE *pipe;
	int this_option_optind = optind ? optind : 1, option_index = 0;
	char opt;

	static struct option long_options[] = { {"--gridway", 1, 0, 0} };

	for (;;) 
	{
		this_option_optind = optind ? optind : 1;
		opt = getopt_long(argc, argv, "g", long_options, &option_index);

		if (opt == -1)
			break;
		switch (opt) 
		{
			case 'g':
				GWflag = 1;
				break;
			case '?':
				error(0, 0, "Unrecognized option: -%c\n", optopt);
				exit(EXIT_FAILURE);
		}
	}

	if (GWflag) 
	{
		if ((gwhome_tmp = getenv("GW_LOCATION")) == NULL) 
		{
			fprintf(stderr, 
			        "Environment variable GW_LOCATION must be set up\n");
			exit(EXIT_FAILURE);
		}

		sprintf(GWhome, gwhome_tmp);
	}

	if ((dcsmon_home_tmp = getenv("DCSMON_HOME")) == NULL) 
	{
		fprintf(stderr, "Environment variable DCSMON_HOME must be set up\n");
		exit(EXIT_FAILURE);
	}

	sprintf(Dcsmon_home, dcsmon_home_tmp);

	if ((pipe = popen("which pbsnodes", "r")) == NULL)
	{
		fprintf(stderr, "Can't execute 'which pbsnodes'");
		exit(EXIT_FAILURE);
	}
	fscanf(pipe, "%s", Pbsnodes_path);
	pclose(pipe);

	if ((pipe = popen("which qstat", "r")) == NULL)
	{
		fprintf(stderr, "Can't execute 'which qstat'");
		exit(EXIT_FAILURE);
	}
	fscanf(pipe, "%s", Qstat_path);
	pclose(pipe);
}

/* memfree: Free memory of global variables. */
static void memfree(void)
{
	if (GWflag)
		gwhostfreemem();
	return;
}

/* set_sighandlers: Set signal handlers. */
static void set_sighandlers(void)
{
	struct sigaction sa;

	sigset_t mask;

	sigfillset(&mask);
	sigdelset(&mask, SIGINT);
	sigdelset(&mask, SIGTERM);
	sigprocmask(SIG_SETMASK, &mask, NULL);

	sa.sa_handler = sigint_handler;
	sigaction(SIGINT, &sa, NULL);
	sa.sa_handler = sigterm_handler;
	sigaction(SIGTERM, &sa, NULL);
}

/* set_thithost: Set Thishost variable (hostname). */
static void set_thishost(void)
{
	FILE *fp;
	char hostnamefile[DM_PATH_LEN];

	sprintf(hostnamefile, "%s/etc/hostname", Dcsmon_home);

	if ((fp = fopen(hostnamefile, "r")) != NULL) 
	{
		fscanf(fp, "%s", Thishost);
		fclose(fp);

		if (strlen(Thishost) > 0)
			return;
	}

	if (gethostname(Thishost, DM_HNAME_LEN) < 0)
		error(1, errno, "gethostnamej() failed");
}

/* sigterm: Handler for the signal SIGTERM. */
static void sigterm_handler(int signo)
{
	error(1, 0, "catch signal SIGTERM - server shutdown");
}

/* sigint_handler: Handler for the signal SIGINT. */
static void sigint_handler(int signo)
{
	error(1, 0, "catch signal SIGINT - server shutdown");
}

/* common_exit: At exit function. */
static void common_exit(void)
{
	memfree();
	unlink(Lockfile);
}
