/*
 * dm_torque.c: Functions for accumulate info from TORQUE.
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/utsname.h>

#include "dcsmon.h"
#include "dm_sysinfo.h"
#include "error.h"

/* getarch: Get system architecture (i686, x86_64, etc) */
int getarch(char *arch)
{
	struct utsname buf;

	if (uname(&buf) < 0)
	{
		error(0, errno, "uname() failed");
		return -1;
	}

	sprintf(arch, "%s", buf.machine);
	return 0;
}
