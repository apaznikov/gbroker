/*
 * error.c: Log and error functions.
 *
 * (C) 2008-2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>
#include <syslog.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

#include "dcsmon.h"
#include "error.h"

char Logname[DM_PATH_LEN];
int  Islogging = 0;			/* Flag: logging enable. */

/* initlog: Initialize log. */
int initlog(const char *home_path)
{
	int fd;
	FILE *flog;

	sprintf(Logname, "%s/var", home_path);

	/* Check if directory exists. */
	if ((fd = open(Logname, O_EXCL)) < 0)
	{
		/* Create directory. */
		if (mkdir(Logname, 0700) < 0)
		{
			fprintf(stderr, "mkdir() failed for %s: %s\n", 
				Logname, strerror(errno));
			return -1;
		}
	}
	close(fd);

	sprintf(Logname, "%s/var/dcsmon.log", home_path);

	if ((flog = fopen(Logname, "w")) == NULL)
	{
		fprintf(stderr, "fopen() failed for %s: %s\n", 
			Logname, strerror(errno));
		return -1;
	}
	fclose(flog);

	Islogging = 1;

	return 0;
}

/* error: Print error information. */
void error(int status, int err, char *fmt, ...)
{
	char *tmp, date[LOG_DATE_LEN], logmsg[LOG_STR_LEN];
	va_list ap;
	FILE *flog;
	time_t t;

	if (Islogging)
	{
		if ((flog = fopen(Logname, "a")) == NULL)
		{
			fprintf(stderr, "fopen() failed for %s: %s\n", 
				Logname, strerror(errno));
			return;
		}
	}

	/* Get and prepare date and time to write into log file. */
	if (Islogging)
	{
		t = time(NULL);
		tmp = ctime(&t);
		tmp = strchr(tmp, ' ');
		tmp++;
		snprintf(date, strlen(tmp) - 5, "%s: ", tmp);
		sprintf(date, "%s: ", date);
	}
	else
		date[0] = '\0';

	va_start(ap, fmt);
	vsprintf(logmsg, fmt, ap);
	va_end(ap);

	if (err)
	{
		fprintf(stderr, "%s", date);
		vfprintf(stderr, logmsg, ap);
		fprintf(stderr, ": %s (%d)\n", strerror(err), err);

		if (Islogging)
		{
			fprintf(flog, "%s", date);
			vfprintf(flog, logmsg, ap);
			fprintf(flog, ": %s (%d)\n", strerror(err), err);
		}
	}
	else
	{
		fprintf(stderr, "%s", date);
		vfprintf(stderr, logmsg, ap);
		fprintf(stderr, "\n");

		if (Islogging)
		{
			fprintf(flog, "%s", date);
			vfprintf(flog, logmsg, ap);
			fprintf(flog, "\n");
		}
	}

	if (Islogging)
		fclose(flog);

	if (status > 0)
		exit(status);
}

/* applog: Print to log file. */
void applog(char *fmt, ...)
{
	char logmsg[LOG_STR_LEN], *tmp, date[LOG_DATE_LEN];
	time_t t;
	va_list ap;
	FILE *flog;

	va_start(ap, fmt);
	vsprintf(logmsg, fmt, ap);
	va_end(ap);

	/* Get and prepare date and time to write into log file. */
	t = time(NULL);
	tmp = ctime(&t);
	tmp = strchr(tmp, ' ');
	tmp++;
	snprintf(date, strlen(tmp) - 4, "%s", tmp);

	if (Islogging)
	{
		if ((flog = fopen(Logname, "a")) == NULL)
		{
			error(0, errno, "APPLOG fopen() failed for %s", Logname);
			return;
		}
	}
	//fprintf(flog, "%spid %d: %s\n", date, getpid(), logmsg);
	//fprintf(stderr, "%spid %d: %s\n", date, getpid(), logmsg);
	if (Islogging)
		fprintf(flog, "%s: %s\n", date, logmsg);

	fprintf(stderr, "%s: %s\n", date, logmsg);

	if (Islogging)
		fclose(flog);
}
