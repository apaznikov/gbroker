/*
 * dcsmon.c: Main dcsmon module
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "dcsmon.h"
#include "dm_stat.h"
#include "dm_client.h"

static int check_usage(int argc, char **argv, char *host, char *cmd);

int main(int argc, char **argv)
{
	char host[DM_HNAME_LEN], cmd[DM_CMD_LEN];
	int rc;

	if (check_usage(argc, argv, host, cmd) < 0)
		return 1;

	if (!strcmp(cmd, "nnodes"))
	{
		if ((rc = dm_getnnodes(host)) >= 0)
			printf("%d\n", rc);
		else
			goto error;

		return 0;
	}

	else if (!strcmp(cmd, "ppn"))
	{
		if ((rc = dm_getppn(host)) >= 0)
			printf("%d\n", rc);
		else
			goto error;

		return 0;
	}

	else if (!strcmp(cmd, "freenodes"))
	{
		if ((rc = dm_getfreenodes(host)) >= 0)
			printf("%d\n", rc);
		else
			goto error;

		return 0;
	}

	else if (!strcmp(cmd, "nprocs"))
	{
		if ((rc = dm_getnprocs(host)) >= 0)
			printf("%d\n", rc);
		else
			goto error;

		return 0;
	}

	else if (!strcmp(cmd, "freeprocs"))
	{
		if ((rc = dm_getfreeprocs(host)) >= 0)
			printf("%d\n", rc);
		else
			goto error;

		return 0;
	}
	else if (!strcmp(cmd, "procmap"))
	{
		procmap_t *procmap;
		int nnodes, i;

		if ((procmap = dm_getprocmap(host, &nnodes)) != NULL)
		{
			for (i = 0; i < nnodes; i++)
				printf("%d ", procmap[i]);
			printf("\n");
			free(procmap);
		}
		else
			goto error;

		return 0;
	}
	else if (!strcmp(cmd, "njobs"))
	{
		int nrunned, nqueued;

		if ((rc = dm_getnjobs(host, &nrunned, &nqueued)) >= 0)
			printf("%d %d\n", nrunned, nqueued);
		else
			goto error;

		return 0;
	}
	else if (!strcmp(cmd, "cpufreq"))
	{
		if ((rc = dm_getcpufreq(host)) >= 0)
			printf("%d\n", rc);
		else
			goto error;

		return 0;
	}
	else if (!strcmp(cmd, "walltime"))
	{
		if ((rc = dm_getwalltime(host)) >= 0)
			printf("%d\n", rc);
		else
			goto error;

		return 0;
	}
	else if (!strcmp(cmd, "arch"))
	{
		char arch[DM_ARCH_LEN];

		if (dm_getarch(host, arch) >= 0)
			printf("%s\n", arch);
		else
			goto error;

		return 0;
	}
	else
	{
		fprintf(stderr, "where cmd:\n");
		fprintf(stderr, "\tnnodes, ppn, freenodes, nprocs, freeprocs, ");
		fprintf(stderr, "procmap, njobs, walltime, cpufreq, arch.\n");
		return -1;
	}

error:
	fprintf(stderr, "Error! Failed connection to dcsmon on %s\n", host);

	return -1;
}

/* check_usage: Check program usage. */
static int check_usage(int argc, char **argv, char *host, char *cmd)
{
	enum { NARGS = 3 };

	if (argc != NARGS)
	{
		fprintf(stderr, "Usage:\tdcsstat <host> <cmd>\n");
		fprintf(stderr, "where cmd:\n");
		fprintf(stderr, "nnodes, ppn, freenodes, nprocs, freeprocs, ");
		fprintf(stderr, "procmap, njobs, walltime, cpufreq, arch.\n");
		return -1;
	}

	sprintf(host, "%s", argv[1]);
	sprintf(cmd, "%s", argv[2]);

	return 0;
}
