#!/bin/sh

#
# $1 - command,
# $2 - test interval,
# $3 - number of intervals,
# $4 - print if no reply.
#

trapped=""
trap 'trapped=yes' SIGCHLD
$1 &

for ((i = 0; i < $3; i++))
do
	[ -z "`ps | grep $!`" ] && exit
	usleep $2
done

[ -z "$trapped" ] && kill $! 2>/dev/null && echo $4
