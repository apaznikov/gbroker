/*
 * pi_mpi.c: MPI simple integration example.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "mpi.h"

int main(int argc, char *argv[])
{
    double PI25DT = 3.141592653589793238462643;
    int i, rank, commsize;
    double nsteps, step, local_pi, pi, sum, x;
    double time = 0.0;
    int namelen;
    char processor_name[MPI_MAX_PROCESSOR_NAME];

    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD, &commsize);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Get_processor_name(processor_name, &namelen);

    fprintf(stdout,"Process %d of %d is on %s\n",
            rank, commsize, processor_name);
    fflush(stdout);

    nsteps = (argc > 1) ? atoi(argv[1]) : 1000000;
    
    if (rank == 0)
    	time -= MPI_Wtime();

    MPI_Bcast(&nsteps, 1, MPI_INT, 0, MPI_COMM_WORLD);

    step = 1.0 / (double)nsteps;
    sum = 0.0;

    for (i = rank + 1; i <= nsteps; i += commsize)
    {
        x = ((double)i - 0.5) * step;
        sum += 4.0 / (1.0 + x * x);
    }
    local_pi = nsteps * sum;

    MPI_Reduce(&local_pi, &pi, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

    if (rank == 0) {
	    time += MPI_Wtime();
	    printf("PI is approximately %.16f, Error is %.16f\n",
	           pi, fabs(pi - PI25DT));
	    printf("(nsteps = %f, step = %f)\n", nsteps, step);	       
	    printf("Elapsed time = %.4f sec.\n", time);	       
    	fflush(stdout);
    }

    MPI_Finalize();
    return 0;
}
