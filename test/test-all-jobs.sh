#!/bin/sh

../obj/gbrun -h xeon80.vpn.local ~/model/jobs/wrf/wrf.jsdl.template
../obj/gbrun -h xeon80.vpn.local ~/model/jobs/nwchem/nwchem.jsdl.template
../obj/gbrun -h xeon80.vpn.local ~/model/jobs/lammps/lammps.jsdl.template
../obj/gbrun -h xeon80.vpn.local ~/model/jobs/pop/pop.jsdl.template
../obj/gbrun -h xeon80.vpn.local ~/model/jobs/raxml/raxml.jsdl.template
