#!/bin/sh

cd ../src
make
cd ../test

N=1
[ -n "$1" ] && N=$1

for i in `seq 1 $N`; do
	#../obj/gbrun -h xeon80.vpn.local pi_mpi-r4.jsdl 
	#../obj/gbrun -h xeon80.vpn.local wrf-long.jsdl 
	../obj/gbrun -h xeon80.vpn.local ~/model/jobs/wrf/wrf.jsdl.template
	usleep 100000
done
