#!/bin/sh

HOSTS="xeon16.cpct.sibsutis.ru xeon32.cpct.sibsutis.ru xeon80.cpct.sibsutis.ru l400a1.vpn.local l400a2.vpn.local l400l1.vpn.local"

cd src
make 
cd ..

if [ -f obj/gbroker ]; then
	cp  obj/gbroker          /opt/grid/gbroker/bin/
	cp  obj/gclient          /opt/grid/gbroker/bin/
	cp  obj/gbrun            /opt/grid/gbroker/bin/
	cp  obj/gbps             /opt/grid/gbroker/bin/
	cp  bin/gb_runtimeout.sh /opt/grid/gbroker/bin/
	cp  etc/gbroker-xeon80.cpct.sibsutis.ru.conf \
	    /opt/grid/gbroker/etc/gbroker.conf

	for host in $HOSTS; do
		scp obj/gbroker          gdev@$host:/opt/grid/gbroker/bin/
		scp obj/gclient          gdev@$host:/opt/grid/gbroker/bin/
		scp obj/gbrun            gdev@$host:/opt/grid/gbroker/bin/
		scp obj/gbps             gdev@$host:/opt/grid/gbroker/bin/
		scp bin/gb_runtimeout.sh gdev@$host:/opt/grid/gbroker/bin/
		scp etc/gbroker-$host.conf \
			gdev@$host:/opt/grid/gbroker/etc/gbroker.conf
	done
fi
