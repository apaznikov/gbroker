/*
 * gb_host.c: Functions for work with hosts.
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com> 
 */

#include <stdio.h>

#include "gbroker.h"
#include "gb_host.h"
#include "gb_launcher.h"
#include "gbroker.h"
#include "error.h"

gb_host_t *Hosts;

/* hosts_init: Init hosts' array. */
int hosts_init()
{
	int i;

	for (i = 0; i < Nhosts; i++)
	{
		Hosts[i].adj_jobnum = 0;
		Hosts[i].adj_procnum = 0;

		/* Total processors. */
		if (getarch_quick(Hosts[i].hname, Hosts[i].arch) < 0)
		{
			error(0, 0, "getarch() failed for %s", Hosts[i].hname);
			sprintf(Hosts[i].arch, "%s", GB_DEFAULT_ARCH);
		}
	}
	return 0;
}
