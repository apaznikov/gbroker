/*
 * gclient.c: Gbroker client module.
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

#include "gbroker.h"
#include "gclient.h"
#include "tcp.h"
#include "error.h"
#include "daemon.h"

char Gclient_home[GB_PATH_LEN];
char Thishost[GB_HNAME_LEN];

/* listener: Listen port, serve clients. */
static void listener(void);

/* server: Recv job from client, submit. */
static void *server(void *arg);

/* checkusage: Check command line usage. */
static void checkusage(void);

/* set_thithost: Set Thishost variable (hostname). */
static void set_thishost(void);

/* set_sighandlers: Set signal handlers. */
static void set_sighandlers(void);

/* sigterm: Handler for the signal SIGTERM. */
static void sigterm_handler(int signo);

/* sigint_handler: Handler for the signal SIGINT. */
static void sigint_handler(int signo);

/* memfree: Free memory of global variables. */
static void memfree(void);

int main(int argc, char **argv)
{
	checkusage();
	initlog(Gclient_home, GCLIENT_LOGNAME);

	#ifndef GB_GCLIENT_DEBUG
	daemonize(Gclient_home);
	#endif

	set_sighandlers();
	set_thishost();

	listener();

	return 0;
}

/* listener: Listen port, serve clients. */
static void listener(void)
{
	SOCKET sserver, sclient;
	struct sockaddr_in peer;
	socklen_t peerlen = sizeof(peer);
	server_args_t server_args;
	pthread_t server_tid;

	sserver = tcp_server(Thishost, GCLIENT_PORT);

	for (;;)
	{
		if ((sclient = accept(sserver, (struct sockaddr *) &peer, 
		                      (socklen_t *) &peerlen)) < 0)
			error(1, errno, "accept() failed");

		server_args.sock = sclient;

		if (pthread_create(&server_tid, NULL, server, &server_args) != 0) 
			error(1, errno, "pthread_create() failed for server");
	}
}

/* server: Recv job from client, submit. */
static void *server(void *arg)
{
	server_args_t *server_args = (server_args_t *) arg;
	SOCKET sock = server_args->sock;

	close(sock);

	return NULL;
}

/* checkusage: Check command line usage. */
static void checkusage(void)
{
	char *gclient_home_tmp;

	if ((gclient_home_tmp = getenv("GBROKER_HOME")) == NULL) 
	{
		fprintf(stderr, "Environment variable GBROKER_HOME must be set up\n");
		exit(EXIT_FAILURE);
	}

	sprintf(Gclient_home, gclient_home_tmp);
}

/* set_thithost: Set Thishost variable (hostname). */
static void set_thishost(void)
{
	FILE *fp;
	char hostnamefile[GB_PATH_LEN];

	sprintf(hostnamefile, "%s/etc/hostname", Gclient_home);

	if ((fp = fopen(hostnamefile, "r")) != NULL) 
	{
		fscanf(fp, "%s", Thishost);
		fclose(fp);

		if (strlen(Thishost) > 0)
			return;
	}

	if (gethostname(Thishost, GB_HNAME_LEN) < 0)
		error(1, errno, "gethostnamej() failed");
}

/* set_sighandlers: Set signal handlers. */
static void set_sighandlers(void)
{
	struct sigaction sa;

	sigset_t mask;

	sigfillset(&mask);
	sigdelset(&mask, SIGINT);
	sigdelset(&mask, SIGTERM);
	sigprocmask(SIG_SETMASK, &mask, NULL);

	sa.sa_handler = sigint_handler;
	sigaction(SIGINT, &sa, NULL);
	sa.sa_handler = sigterm_handler;
	sigaction(SIGTERM, &sa, NULL);
}
/* sigterm: Handler for the signal SIGTERM. */
static void sigterm_handler(int signo)
{
	char lockfile[BUFLEN];

	memfree();
	sprintf(lockfile, "%s/var/gbroker.pid", Gclient_home);
	unlink(lockfile);
	error(1, 0, "catch signal SIGTERM - server shutdown");
}

/* sigint_handler: Handler for the signal SIGINT. */
static void sigint_handler(int signo)
{
	char lockfile[BUFLEN];

	memfree();
	sprintf(lockfile, "%s/var/gbroker.pid", Gclient_home);
	unlink(lockfile);
	error(1, 0, "catch signal SIGINT - server shutdown");
}

/* memfree: Free memory of global variables. */
static void memfree(void)
{
	/* clearjobs(); */
	return;
}
