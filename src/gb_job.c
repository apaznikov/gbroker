/*
 * gb_job.c: Functions connected with job.
 *
 * (c) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "gbroker.h"
#include "gb_job.h"
#include "error.h"

int Nhosts;

/* job_resource_add_adj: Make resource state adjustment. */
void job_resource_add_adj(gb_job_t *job, gb_host_t *host)
{
	host->adj_jobnum++;
	host->adj_procnum += job->rank;
	job->res_adj_flags[host->id] = 1;

	/*
	fprintf(stderr, "%s + jobnum = %d\n", host->shname, host->adj_jobnum);
	fprintf(stderr, "%s + proc = %d\n\n", host->shname, host->adj_procnum);
	*/
}

/* job_resource_remove_adj: Make resource state adjustment. */
void job_resource_remove_adj(gb_job_t *job, gb_host_t *host)
{
	if (job->res_adj_flags[host->id])
	{
		host->adj_jobnum--;
		host->adj_procnum -= job->rank;
		job->res_adj_flags[host->id] = 0;

		/*
		fprintf(stderr, "%s - jobnum = %d\n", host->shname, host->adj_jobnum);
		fprintf(stderr, "%s - proc = %d\n\n", host->shname, host->adj_procnum);
		*/
	}
}

/* jobmalloc: Allocate memory for job (hostcount-dependent attributes). */
int jobmalloc(gb_job_t *job)
{
	int i;

	if ((job->contact = malloc(sizeof(char *) * Nhosts)) == NULL)
	{
		error(0, errno, "malloc() failed for job->contact");
		jobfree(job);
		return -1;
	}

	for (i = 0; i < Nhosts; i++)
		if ((job->contact[i] = malloc(sizeof(char) * GB_CONTACT_LEN)) == NULL)
		{
			error(0, errno, "malloc() failed for job->contact[%d]", i);
			jobfree(job);
			return -1;
		}

	if ((job->status = malloc(sizeof(char *) * Nhosts)) == NULL)
	{
		error(0, errno, "malloc() failed for job->status");
		jobfree(job);
		return -1;
	}

	for (i = 0; i < Nhosts; i++)
		if ((job->status[i] = malloc(sizeof(char) * GB_STATUS_LEN)) == NULL)
		{
			error(0, errno, "malloc() failed for job->status[%d]", i);
			jobfree(job);
			return -1;
		}

	if ((job->rsl = malloc(sizeof(char *) * Nhosts)) == NULL)
	{
		error(0, errno, "malloc() failed for job->rsl");
		jobfree(job);
		return -1;
	}

	for (i = 0; i < Nhosts; i++)
		if ((job->rsl[i] = malloc(sizeof(char) * GB_PATH_LEN)) == NULL)
		{
			error(0, errno, "malloc() failed for job->rsl[%d]", i);
			jobfree(job);
			return -1;
		}

	if ((job->submithosts = malloc(sizeof(gb_host_t *) * Nhosts)) == NULL)
	{
		error(0, errno, "malloc() failed for job->submithosts");
		jobfree(job);
		return -1;
	}

	if ((job->res_adj_flags = malloc(sizeof(int) * Nhosts)) == NULL)
	{
		error(0, errno, "malloc() failed for job->res_adf_flags");
		jobfree(job);
		return -1;
	}

	if ((job->schedhosts = malloc(sizeof(gb_schedhosts_t) * Nhosts)) == NULL)
	{
		error(0, errno, "malloc() failed for job->schedhosts");
		jobfree(job);
		return -1;
	}

	for (i = 0; i < Nhosts; i++)
	{
		job->submithosts[i] = NULL;
		job->res_adj_flags[i] = 0;
	}
	job->exechost = NULL;

	if ((job->nodes = malloc(sizeof(int *) * Nhosts)) == NULL)
	{
		error(0, errno, "malloc() failed for job->nodes");
		jobfree(job);
		return -1;
	}

	if ((job->ppn = malloc(sizeof(int *) * Nhosts)) == NULL)
	{
		error(0, errno, "malloc() failed for job->ppn");
		jobfree(job);
		return -1;
	}

	return 0;
}

/* jobfree: Free memory for job. */
void jobfree(gb_job_t *job)
{
	int i;

	free(job->stages);

	for (i = 0; i < Nhosts; i++)
		if (job->contact[i] != NULL) 
			free(job->contact[i]);
		else
			break;

	if (job->contact != NULL)
		free(job->contact);

	for (i = 0; i < Nhosts; i++)
		if (job->status[i] != NULL) 
			free(job->status[i]);
		else
			break;
	
	if (job->status != NULL)
		free(job->status);

	for (i = 0; i < Nhosts; i++)
		if (job->rsl[i] != NULL) 
			free(job->rsl[i]);
		else
			break;

	if (job->rsl != NULL)
		free(job->rsl);

	if (job->submithosts != NULL)
		free(job->submithosts);

	if (job->res_adj_flags != NULL)
		free(job->res_adj_flags);

	if (job->schedhosts != NULL)
		free(job->schedhosts);
	
	if (job->nodes != NULL)
		free(job->nodes);

	if (job->ppn != NULL)
		free(job->ppn);

	#ifdef _EXEC_HARNESS 
	if (job->execlist)
		free(job->execlist);
	#endif /* _EXEC_HARNESS */
}

