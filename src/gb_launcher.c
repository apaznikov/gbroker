#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include "gbroker.h"
#include "nm_client.h"
#include "dm_client.h"
#include "gb_launcher.h"
#include "gb_utils.h"
#include "error.h"

/*
 * TODO: dcsmon and netmon function sometimes fails
 *       therefore NETMON_NATTEMPTS and DCSMON_NATTEMPTS was introduced
 */

/*
 * Globus Toolkit functions
 */

/* globusrun: Globus Toolkit globusrun launcher. */
int globusrun(char *resource, char *job, char *contact)
{
	char cmd[GB_CMD_LEN], tok[GB_TOKEN_LEN];
	FILE *pipe;
	int i;

	sprintf(cmd, "globusrun -F -r %s -f %s", resource, job);
	
	if ((pipe = popen(cmd, "r")) == NULL)
	{
		error(0, 0, "popen() failed");
		return -1;
	}

	for (i = 0; i < 6; i++)
		fscanf(pipe, "%s", tok);

	fscanf(pipe, "%s", contact);

	/* Find word 'failed' in the output. */
	while (fscanf(pipe, "%s", tok) != EOF)
		if (!strcmp(tok, "GLOBUS_GRAM_PROTOCOL_JOB_STATE_FAILED"))
		{
			pclose(pipe);
			return -1;
		}

	pclose(pipe);

	return 0;
}

/* globuskill: Kill job by Globus Toolkit globusrun. */
int globuskill(char *contact)
{
	char cmd[GB_CMD_LEN];
	/*
	char tok[GB_TOKEN_LEN];
	FILE *pipe;
	*/

	fprintf(stderr, "GLOBUSRUN -q -k '%s' &\n", contact);
	sprintf(cmd, "globusrun -q -k %s &", contact);
	if (system(cmd) < 0)
	{
		error(0, errno, "system() failed for %s", cmd);
		return -1;
	}

	/*
	if ((pipe = popen(cmd, "r")) == NULL)
	{
		error(0, 0, "popen() failed");
		return -1;
	}
	*/

	/* Find word 'failed' in the output. */
	/*
	while (fscanf(pipe, "%s", tok) != EOF)
		if (!strcmp(tok, "Error"))
		{
			pclose(pipe);
			return -1;
		}
	

	pclose(pipe);
	*/

	return 0;
}

/* globusrun_status: Get status of the job by contact. */
int globusrun_status(char *contact, char *status)
{
	char cmd[GB_CMD_LEN];
	FILE *pipe;

	/*
	   sprintf(cmd, "globusrun -status %s", contact);
	 */
	sprintf(cmd, "gb_runtimeout.sh \"globusrun -status %s\" %d %d %s", 
	        contact, GB_TIMEOUT_INTERVAL, GB_TIMEOUT_NINTERVALS,
			GB_JOB_STATUS_INDEF);
	if ((pipe = popen(cmd, "r")) == NULL)
	{
		error(0, 0, "popen() failed");
		return -1;
	}
	
	fscanf(pipe, "%s", status);
	pclose(pipe);

	return 0;
}

/* globus-url-copy: Globus Toolkit globus-url-copy launcher. */
int globus_url_copy(char *src, char *dest)
{
	char cmd[GB_CMD_LEN];
	FILE *pipe;

	sprintf(cmd, "globus-url-copy %s %s 2>&1", src, dest);

	if ((pipe = popen(cmd, "r")) == NULL)
	{
		error(0, 0, "popen() failed for %s", cmd);
		return -1;
	}

	if (fscanf(pipe, "%s", cmd) != EOF)
	{
		pclose(pipe);
		return -1;
	}

	pclose(pipe);

	return 0;
}

/* remote_mkdir: Remote make dir on host. */
int remote_mkdir(char *host, char *dir)
{
	char src[GB_CMD_LEN] = "-cd /dev/null";
	char dest[GB_CMD_LEN];

	sprintf(dest, "gsiftp://%s/%s/null", host, dir);
	if (globus_url_copy(src, dest) < 0)
		return -1;

	return 0;
}

/*
 * Netmon functions.
 */

/* getnettime: Return time to send msg with 'size' from hsot1 to host2. */
double getnettime(char *host1, char *host2, double size)
{
	int i;
	double time;

	/*
	pthread_mutex_lock(&Netmon_mutex);
	*/
	for (i = 0; i < NETMON_NATTEMPTS; i++)
		if ((time = nm_gettime(host1, host2, size)) >= 0)
			break;
	/*
	pthread_mutex_unlock(&Netmon_mutex);
	*/

	return time;
}

/* getnettimeforfile: Return time to send file from hsot1 to host2. */
double getnettimeforfile(char *host1, char *host2, char *file)
{
	double time;
	int i;

	for (i = 0; i < NETMON_NATTEMPTS; i++)
		if ((time = nm_gettimeforfile(host1, host2, file)) >= 0)
			break;

	return time;
}

/* get_nettime_for_table: Get time for list of files and list of hosts. */
int get_nettime_for_table(char *host, hoststage_t *hoststages,
                          gb_host_t *hosts, int nhosts, double *stagetimes)
{
	int i;
	nm_hostlist_t *hostlist = NULL;

	/* Allocate memory for hostlist. */
	if ((hostlist = malloc(nhosts * sizeof(nm_hostlist_t))) == NULL)
	{
		error(0, 0, "malloc() failed for hostlist");
		return -1;
	}

	/* Copy hostnames from hosts to hostlist. */
	for (i = 0; i < nhosts; i++)
	{
		sprintf(hostlist[i].hname, "%s", hosts[i].hname);
		sprintf(hostlist[i].arch, "%s", hosts[i].arch);
	}

	/* Call netmon client function. Try N times. */
	for (i = 0; i < NETMON_NATTEMPTS; i++)
		if (nm_get_nettime_for_table(host, 
			                         hoststages->file, hoststages->nfiles,
			                         hostlist, nhosts, 
									 stagetimes) >= 0)
		{
			free(hostlist);
			return 0;
		}

	free(hostlist);
	return -1;
}

/*
 * Dcsmon functions.
 */

/* getprocmap: Get map of free processors on host. */
procmap_t *getprocmap(char *host, int *nnodes)
{
	int i;
	procmap_t *procmap;

	for (i = 0; i < DCSMON_NATTEMPTS; i++)
		if ((procmap = dm_getprocmap(host, nnodes)) != NULL)
			break;
	
	return procmap;
}

/* getnjobs: Get number of jobs (running and queued). */
int getnjobs(char *host, int *nrunning, int *nqueued)
{
	int i, rc;

	for (i = 0; i < DCSMON_NATTEMPTS; i++)
		if ((rc = dm_getnjobs(host, nrunning, nqueued)) >= 0)
			break;

	return rc;
}

/* getnnodes: Get total number of nodes. */
int getnnodes(char *host)
{
	int i, rc;

	for (i = 0; i < DCSMON_NATTEMPTS; i++)
		if ((rc = dm_getnnodes(host)) >= 0)
			break;

	return rc;
}

/* getnnodes: Get number of processors per node. */
int getppn(char *host)
{
	int i, rc;

	for (i = 0; i < DCSMON_NATTEMPTS; i++)
		if ((rc = dm_getppn(host)) >= 0)
			break;

	return rc;
}

/* getfreenodes: Get number of free nodes. */
int getfreenodes(char *host)
{
	int i, rc;

	for (i = 0; i < DCSMON_NATTEMPTS; i++)
		if ((rc = dm_getfreenodes(host)) >= 0)
			break;

	return rc;
}

/* getnprocs: Get total number of processors. */
int getnprocs(char *host)
{
	int rc, i;

	for (i = 0; i < DCSMON_NATTEMPTS; i++)
		if ((rc = dm_getnprocs(host)) >= 0)
			break;

	return rc;
}

/* getnprocs_quick: Get total number of processors. ONE attempt only. */
int getnprocs_quick(char *host)
{
	return dm_getnprocs(host);
}

/* getfreeprocs: Get number of free processors. */
int getfreeprocs(char *host)
{
	int i, rc;

	for (i = 0; i < DCSMON_NATTEMPTS; i++)
		if ((rc = dm_getfreeprocs(host)) >= 0)
			break;

	return rc;
}

/* getwalltime: Get queue walltime. */
int getwalltime(char *host)
{
	int i, rc;

	for (i = 0; i < DCSMON_NATTEMPTS; i++)
		if ((rc = dm_getwalltime(host)) >= 0)
			break;

	return rc;
}

/* getarch: Get system architecture. */
int getarch(char *host, char *arch)
{
	int i, rc;

	for (i = 0; i < DCSMON_NATTEMPTS; i++)
		if ((rc = dm_getarch(host, arch)) >= 0)
			break;

	return rc;
}

/* getarch_quick: Get system architecture. One attempt only. */
int getarch_quick(char *host, char *arch)
{
	return dm_getarch(host, arch);
}
