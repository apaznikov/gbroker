/*
 * tcp.c: Library of common used functions used in client-server application.
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <netdb.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <syslog.h>
#include <fcntl.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>

#include "gbroker.h"
#include "tcp.h"
#include "error.h"

/* That is in order to make declaration of function 'inet_aton' explicit. */
/* int inet_aton(const char *cp, struct in_addr *inp); */

/* getfilesize: Get size of file by filename. */
static int getfilesize(char *filename);

/* connect_nonb: non-blocking connect(). */
static int connect_nonb(int s, const struct sockaddr *peer, socklen_t salen, 
                        int nsec);

/* tcp_server: Common used tcp server code */
SOCKET tcp_server(char *hname, char *sname)
{
	struct sockaddr_in local;
	SOCKET s;
	const int on = 1;

	if (set_address(hname, sname, &local, "tcp") < 0)
		error(1, errno, "set_address() failed");

	s = socket(AF_INET, SOCK_STREAM, 0);

	if (s < 0)
		error(1, errno, "socket() failed");

	if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof(on)))
		error(1, errno, "setsockopt() failed");

	if (bind(s, (struct sockaddr *)&local, sizeof(local)))
		error(1, errno, "bind() failed");

	/* applog("Listen to %s:%s...", hname, sname); */
	if (listen(s, NLISTEN))
		error(1, errno, "listen() failed");

	return s;
}

/* tcp_client: Try to connect. Don't exit, if failed. */
SOCKET tcp_client(char *hname, char *sname)
{
	struct sockaddr_in peer;
	SOCKET s;

	/* applog("Trying %s:%s...", hname, sname); */

	if (set_address(hname, sname, &peer, "tcp") < 0)
		return -1;

	s = socket(AF_INET, SOCK_STREAM, 0);
	if (s < 0)
		error(0, errno, "socket() failed");

	if (connect_nonb(s, (const struct sockaddr *) &peer, sizeof(peer), 
		             CONNECT_TIMEOUT) < 0)
		return -1;
	else
		return s;

	/*
	if (connect(s, (struct sockaddr *)&peer, sizeof(peer)))
		return -1;
	else
		return s;
	*/
}

/* connect_nonb: non-blocking connect(). */
static int connect_nonb(int s, const struct sockaddr *peer, socklen_t salen, 
                        int nsec)
{
	int flags, n, err;
	socklen_t len;
	fd_set rset, wset;
	struct timeval tval;

	flags = fcntl(s, F_GETFL, 0);
	fcntl(s, F_SETFL, flags | O_NONBLOCK);

	err = 0;
	if ((n = connect(s, peer, salen)) < 0)
	{
		if (errno != EINPROGRESS)
		{
			error(0, errno, "connect() failed");
			return -1;
		}
	}

	if (n == 0)
		goto done;		/* connect completed immediately */

	FD_ZERO(&rset);
	FD_SET(s, &rset);
	wset = rset;
	tval.tv_sec = nsec;
	tval.tv_usec = 0;

	if ((n = select(s + 1, &rset, &wset, NULL, nsec ? &tval : NULL)) == 0)
	{
		close(s);
		errno = ETIMEDOUT;
		/* error(0, errno, "select() failed"); */
		return -1;
	}

	if (FD_ISSET(s, &rset) || FD_ISSET(s, &wset))
	{
		len = sizeof(err);
		if (getsockopt(s, SOL_SOCKET, SO_ERROR, &err, &len) < 0)
		{
			error(0, errno, "getsockopt() failed");
			return -1;
		}
	}
	else
	{
		error(1, errno, "FD_ISSET failed(): socket not set");
		return -1;
	}

	done:
		fcntl(s, F_SETFL, flags);

		if (err)
		{
			close(s);
			errno = err;
			return -1;
		}
		return 0;
}

/* set_address: Fill sap with hname and sname */
int set_address(char *hname, char *sname,
                struct sockaddr_in *sap, char *protocol)
{
	struct servent *sp;
	struct hostent *hp;
	char *endptr;
	short port;

	bzero(sap, sizeof(*sap));
	sap->sin_family = AF_INET;

	if (hname != NULL)
	{
		if (!inet_aton(hname, &sap->sin_addr))
		{
			hp = gethostbyname(hname);
			if (hp == NULL)
			{
				error(0, errno, "Unknown host: %s", hname);
				return -1;
			}
			sap->sin_addr = *(struct in_addr * )hp->h_addr_list[0];
		}
	}
	else
		sap->sin_addr.s_addr = htonl(INADDR_ANY);

	port = strtol(sname, &endptr, 0);
	if (*endptr == '\0')
		sap->sin_port = htons(port);
	else
	{
		sp = getservbyname(sname, protocol);
		if (sp == NULL)
		{
			error(0, errno, "Unknown service: %s", sname);
			return -1;
		}
		sap->sin_port = sp->s_port;
	}

	return 0;
}

/* recvn: Read len bytes to buffer bp */
int recvn(SOCKET s, char *bp, size_t len)
{
	int cnt;
	int rc = 0;

	cnt = len;
	while (cnt > 0)
	{
		rc = recv(s, bp, cnt, 0);
		if (rc < 0)			       /* Read error? */
		{
			if (errno == EINTR)	   /* Call interrupt? */
				continue;	       /* Return error code */
			return -1;
		}
		if (rc == 0)			   /* End of file? */
			return len - cnt;	   /* Return incomplete counter */
		/* fprintf(stderr, "RECV %d '%s', rc = %d\n", id, bp, rc); */
		bp += rc;
		cnt -= rc;
	}
	return len;
}

/* sendn: Send bytes of buffer bp */
int sendn(SOCKET s, char *bp, size_t len)
{
	int count;
	int rc;

	count = len;
	while (count > 0)
	{
		rc = send(s, bp, count, 0);
		if (rc < 0)
		{
			if (errno == EINTR)
				continue;
			return -1;
		}
		else if (rc == 0)
			return len - count;
		bp += rc;
		count -= rc;
	}

	return len;
}

/* sendfile: Send file filename to socket. */
int sendfile(int sock, char *filename)
{
	int filesize;
	FILE *fin;
	char buf[BUF_LEN];
	int rc;

	/* Synchronize. */
	if (recvn(sock, buf, TCP_ACK_SIZE) != TCP_ACK_SIZE)
		error(0, errno, "recvn() failed for TCP_ACK");

	if ((filesize = getfilesize(filename)) == -1)
	{
		error(0, errno, "getfilesize() failed for %s", filename);
		return -1;
	}

	/* fprintf(stderr, "sending %d...", filesize); */

	if (sendn(sock, (char *) &filesize, sizeof(filesize)) != sizeof(filesize))
	{
		error(0, errno, "sendn failed for filesize");
		return -1;
	}

	if ((fin = fopen(filename, "r")) == NULL)
	{
		error(0, errno, "fopen() failed for %s", filename);
		return -1;
	}

	while ((rc = fread(buf, sizeof(*buf), BUF_LEN, fin)) > 0) 
	{
		if (sendn(sock, buf, rc) != rc) 
		{
			fclose(fin);
			return -1;
		}
		/* fprintf(stderr, "sended %d\n", rc); */
	}

	fclose(fin);

	/* Synchronize. */
	if (recvn(sock, buf, TCP_ACK_SIZE) != TCP_ACK_SIZE)
		error(0, errno, "recvn() failed for TCP_ACK");

	return filesize;
}

/* recvfile: Receive file. */
int recvfile(int sock, char *filename)
{
	FILE *fout;
	char buf[BUF_LEN];
	int filesize, received, rc;

	/* Synchronize. */
	if (sendn(sock, TCP_ACK, TCP_ACK_SIZE) != TCP_ACK_SIZE)
		error(0, errno, "sendn() failed for TCP_CACK");
	
	if (recvn(sock, (char *) &filesize, sizeof(filesize)) != sizeof(filesize))
		error(0, errno, "recvn() failed");

	/* fprintf(stderr, "receiving %d...", filesize); */

	if ((fout = fopen(filename, "w")) == NULL)
		return -1;
	
	received = 0;
	while (received < filesize) 
	{
		if ((rc = recv(sock, buf, BUF_LEN, 0)) < 0)
		{
			error(0, errno, "recv() failed");
			return -1;
		}

		fwrite(buf, sizeof(*buf), rc, fout);
		received += rc;
		/* fprintf(stderr, "rc %d sum %d\n", rc, received); */
	}

	fclose(fout);

	if (received != filesize)
		return -1;

	/* Synchronize. */
	if (sendn(sock, TCP_ACK, TCP_ACK_SIZE) != TCP_ACK_SIZE)
		error(0, errno, "sendn() failed for TCP_CACK");

	return filesize;
}

/* getfilesize: Get size of file by filename. */
static int getfilesize(char *filename)
{
	struct stat buf;
	return (stat(filename, &buf) == -1) ? (-1) : (buf.st_size);
}


/* getshortname: Get short hostname by full hostname (foo.bar.com -> foo). */ 
void getshorthname(const char *full_hname, char *short_hname)
{
	int dotloc;

	/* Find first occurence of '.' in hname. */
	for (dotloc = 0; dotloc < strlen(full_hname); dotloc++)
		if (full_hname[dotloc] == '.')
			break;
	dotloc++;

	snprintf(short_hname, dotloc, "%s", full_hname);
}
