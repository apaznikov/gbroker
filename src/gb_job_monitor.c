/*
 * gb_job_monitor.c: Job monitor.
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/time.h>

#include "gbroker.h"
#include "gb_job_monitor.h"
#include "gb_job_list.h"
#include "gb_launcher.h"
#include "gb_dispatch.h"
#include "gb_stat.h"
#include "gb_utils.h"
#include "gb_job.h"
#include "error.h"

pthread_mutex_t Jobmoncond_mutex;
pthread_cond_t  Jobmon_cond;

gb_joblist_t *Rjobs_head, *Rjobs_tail;  /* Running jobs. */
gb_joblist_t *Cjobs_head, *Cjobs_tail;  /* Completed jobs. */

/* Job list mutexes. */
pthread_mutex_t Rjobs_mutex;
pthread_mutex_t Cjobs_mutex;

int Nrunned = 0, Ncompl = 0;

/* 
 * job_resource_check_adj: Check job state and fix resource adjustments if 
 * state is not stage_in or unknown. 
 */
static void job_resource_check_adj(gb_job_t *job, char *status, int hid);

/* jobmon: Job monitor. */
void *jobmon(void *arg)
{
	gb_joblist_iter_t iter;
	gb_job_t *job;
	char curstatus[GB_STATUS_LEN];
	int i;

	pthread_mutex_init(&Jobmoncond_mutex, NULL);
	pthread_cond_init(&Jobmon_cond, NULL);

	for (;;)
	{
		int njobs_dbg;

		/* Wait until some at least one job is running. */
		pthread_mutex_lock(&Jobmoncond_mutex);
		while (Nrunned == 0)
		{
			#ifdef GB_JOBMON_DEBUG
			fprintf(stderr, "JOBMON waits for jobs...\n");
			#endif
			pthread_cond_wait(&Jobmon_cond, &Jobmoncond_mutex);
		}
		pthread_mutex_unlock(&Jobmoncond_mutex);

		/*
		#ifdef GB_JOBMON_DEBUG
		if (cur != NULL)
			fprintf(stderr, "\n");
		#endif
		*/

		/*
		pthread_mutex_lock(&Rjobs_mutex);
		*/

		njobs_dbg = 0;
		for (runjoblist_iter_init(&iter); !joblist_iter_isend(&iter);
		     joblist_iter_getnext(&iter))
		{
			njobs_dbg++;

			job = joblist_iter_getjob(&iter);

			/* Loop by hosts, where job is submitted. */
			for (i = 0; i < Nhosts; i++)
			{
				if (job->submithosts[i] != NULL)
					/* Check host only if job was submitted to it. */
				{
					globusrun_status(job->contact[i], curstatus);
					job_resource_check_adj(job, curstatus, i);

					#ifdef GB_JOBMON_DEBUG
					applog("\t%d (%s): %s", 
					       job->id, job->submithosts[i]->shname, curstatus);
					#endif

					if (is_done(curstatus))
						/* 
						 * Job is completed: 
						 * 1. Kill the rest executed copies of job.
						 * 2. Signal about job complete by semaphore.
						 * 3. Add job to list of completed jobs.
						 * 4. Del job from list of running jobs.
						 */
					{
						/* Statistics. */
						gbstat_servtime(job);

						if ((!is_active(job->status[i])) && (!job->execflag))
							gbstat_queuetime(job);

						if (killrest(job, i) < 0)
							error(0, 0, "killrest() failed");

						if (job->outputflag)
							sem_post(&job->complete_sem);

						addcompljob(job);
						delrunjob(&iter);

						njobs_dbg--;

						break;
					}
					else if (!is_active(job->status[i]) && is_active(curstatus))
						/* 
						 * Job was pending and now is executing:
						 * 1. Kill the rest executed copies of job.
						 */
					{
						/* Statistics. */
						if (!job->execflag)
						{
							gbstat_queuetime(job);
							job->execflag = 1;
						}

						gettimeofday(&job->exectime, NULL);

						if (killrest(job, i) < 0)
							error(0, 0, "killrest() failed");

						sprintf(job->status[i], "%s", curstatus);

						break;
					}

					#ifdef GB_MIGRATE_ENABLE
					else if ((GB_NSUBMITS < Nhosts) && 
						     is_pending(curstatus) && 
						    (gettimepast(job->migrtime) > GB_MIGR_INTERVAL))
						/* 
						 * Job is waiting too long in pending state.
						 * 1. Try to migrate.
						 * 2. Set time of migrating attempt.
						 */
					{
						if (migrate(job, i) < 0)
							error(0, 0, "migrate() failed");

						gettimeofday(&job->migrtime, NULL);

						break;
					}
					#endif /* GB_MIGRATE_ENABLE */
					else
						sprintf(job->status[i], "%s", curstatus);

					/* 
					 * State didn't change:
					 * Don't do anything (just refresh state).
					 */
				}

			}
		}

		#ifdef GB_JOBMON_DEBUG
		fprintf(stderr, "Nrunned = %d\n", Nrunned);
		fprintf(stderr, "Ncompl = %d\n", Ncompl);
		fprintf(stderr, "\nJOBMON: ");
		printrunjobs();
		fprintf(stderr, "JOBMON: ");
		printcompljobs();
		#endif

		if (njobs_dbg != Nrunned)
		{
			applog("njobs_dbg (%d) != Nrunned (%d) !\n", njobs_dbg, Nrunned);
			fprintf(stderr, "\nJOBMON: ");
			printrunjobs();
			fprintf(stderr, "JOBMON: ");
			printcompljobs();
			exit(1);
		}

		/*
		pthread_mutex_unlock(&Rjobs_mutex);
		*/

		sleep(GB_JOBMON_TIMEOUT);
	}

	return NULL;
}

/* jobmon_addjob: Process job add. */
void jobmon_addjob(void)
{
	/* Signal about job has came. */
	pthread_mutex_lock(&Jobmoncond_mutex);
	if (Nrunned == 0)
		pthread_cond_signal(&Jobmon_cond);
	Nrunned++;
	pthread_mutex_unlock(&Jobmoncond_mutex);
}

/* 
 * job_resource_check_adj: Check job state and fix resource adjustments if 
 * state is not stage_in or unknown. 
 */
static void job_resource_check_adj(gb_job_t *job, char *status, int hid)
{
	if (!is_stagein(status) && !is_indef(status))
		/* Resource adjustment. */
	{
		/* fprintf(stderr, "job %d, host = %s = '%s'\n", 
		        job->id, Hosts[hid].shname, status); */
		job_resource_remove_adj(job, job->submithosts[hid]);
	}
}

/* is_active: True if job is ACTIVE (running). */
int is_active(char *status)
{
	return (!strcmp(status, GB_JOB_STATUS_ACTIVE));
	/* return (!strncmp(status, "ACTIVE", strlen("ACTIVE"))); */
}

/* is_pending: True if job is PENDING. */
int is_pending(char *status)
{
	return (!strcmp(status, GB_JOB_STATUS_PENDING));
	/* return (!strncmp(status, "PENDING", strlen("PENDING"))); */
}

/* is_done: True if job is DONE. */
int is_done(char *status)
{
	return ((!strcmp(status, GB_JOB_STATUS_DONE)) || 
	        (!strcmp(status, GB_JOB_STATUS_UNKNOWN)));
	/*
	return (!strncmp(status, "DONE", strlen("DONE")) || 
	        !strncmp(status, "UNKNOWN", strlen("UNKNOWN")));
	*/
}

/* is_indef: True if job state if INDEFINITE. */
int is_indef(char *status)
{
	return (!strcmp(status, GB_JOB_STATUS_INDEF));
}

/* is_stagein: True if job state if STAGE_IN. */
int is_stagein(char *status)
{
	return (!strcmp(status, GB_JOB_STATUS_STAGE_IN));
}
