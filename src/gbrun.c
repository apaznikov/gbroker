/*
 * gbrun.c: GBroker client interface. 
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <getopt.h>
#include <errno.h>
#include <sys/types.h>
#include <pwd.h>

#include "gbroker.h"
#include "gclient.h"
#include "gb_job_parser.h"
#include "gb_dispatch.h"
#include "gb_utils.h"
#include "tcp.h"
#include "error.h"

char Gclient_home[GB_PATH_LEN];
char Thishost[GB_HNAME_LEN];

/* sendjobtogclient: Send job to gclient. */
int sendjobtogclient(gb_job_t *job);

/* printoutput: Print output of the job. */
static int printoutput(gb_job_t *job);

/* printfile: Print file. */
static int printfile(char *fname);

/* recvoutput: Recv output from gbroker. */
static int recvoutput(SOCKET sock, gb_job_t *job);

/* set_thishost: Set Thishost variable (hostname). */
static void set_thishost(void);

/* checkusage: Check command line. */
static int checkusage(int argc, char **argv, char *hname, char *jobfile, 
                      int *outputflag, int *getoutputflag, char *jid);

int main(int argc, char **argv)
{
	char hname[GB_HNAME_LEN], jobfile[GB_PATH_LEN], jid[GB_GID_LEN];
	char fname[GB_PATH_LEN];
	int getoutputflag;
	struct passwd *pwd;
	SOCKET sock;
	gb_job_t job;

	if (checkusage(argc, argv, hname, jobfile, 
		           &job.outputflag, &getoutputflag, jid) < 0)
		error(1, 0, "checkusage() failed");

	set_thishost();

	if (getoutputflag)	
		/* 
		 * Get output of the job. 
		 */
	{
		gb_jobstate_t jobstate;

		if (recvoutputbyjid(jid, fname, &jobstate) < 0)
			error(1, 0, "recvoutputbyjid() failed");

		switch (jobstate)
		{
			case GB_JOBRUNNING_ST:
				fprintf(stderr, "Job is still running\n");
				break;
			case GB_JOBCOMPLETE_ST:
				if (printfile(fname) < 0)
					error(1, 0, "printfile() failed");
				break;
			case GB_UNKNOWN_ST:
				fprintf(stderr, "Invalid job id\n");
				break;
		}
	}

	else
		/* 
		 * Parse job and send to gbroker. 
		 */
	{
		if (parsejsdl(jobfile, &job) < 0)
		{
			freejob(&job);
			error(1, 0, "parsejsdl() failed");
		}

		if ((pwd = getpwuid(getuid())) == NULL)
			error(1, errno, "getpwuid() failed");
		sprintf(job.username, "%s", pwd->pw_name);

		/*
		if (sendjobtogclient(&job) < 0)
		{
			freejob(&job);
			error(1, 0, "sendjobtogclient() failed");
		}
		*/

		if (sendjob(hname, &job, &sock) < 0)
		{
			freejob(&job);
			error(1, 0, "sendjob() failed");
		}

		if (job.outputflag)
			/* 
			 * Interactive job: recv and print job output. 
			 */
		{
			if (recvoutput(sock, &job) < 0)
			{
				freejob(&job);
				error(1, 0, "recvoutput() failed");
			}
			else if (printoutput(&job) < 0)
			{
				freejob(&job);
				error(1, 0, "printoutput() failed");
			}
		}
		else
			/*
			 * Non-interactive job: recv job ID.
			 */
		{
			fprintf(stderr, "%s\n", job.gid);
		}

		freejob(&job);
	}

	return 0;
}

/* sendjobtogclient: Send job to gclient. */
int sendjobtogclient(gb_job_t *job)
{
	SOCKET sock;

	if ((sock = tcp_client(Thishost, GCLIENT_PORT)) < 0)
		return -1;

	return 0;
}

/* printoutput: Print output of the job. */
static int printoutput(gb_job_t *job)
{
	FILE *file;
	char c;

	if ((file = fopen(job->stout, "r")) == NULL)
	{
		error(0, errno, "fopen() failed for %s", job->stout);
		return -1;
	}

	while ((c = fgetc(file)) != EOF)
		putchar(c);

	fclose(file);
	return 0;
}

/* printfile: Print file. */
static int printfile(char *fname)
{
	FILE *file;
	char c;

	if ((file = fopen(fname, "r")) == NULL)
	{
		error(0, errno, "fopen() failed for %s", fname);
		return -1;
	}

	while ((c = fgetc(file)) != EOF)
		putchar(c);

	fclose(file);
	return 0;
}

/* recvoutput: Recv output from gbroker. */
static int recvoutput(SOCKET sock, gb_job_t *job)
{
	if (recvfile(sock, pathtoname(job->stout)) < 0)
		return -1;
	return 0;
}

/* set_thishost: Set Thishost variable (hostname). */
static void set_thishost(void)
{
	FILE *fp;
	char hostnamefile[GB_PATH_LEN];

	if (getenv("GBROKER_HOME") != NULL) 
	{
		sprintf(hostnamefile, "%s/etc/hostname", Gclient_home);

		if ((fp = fopen(hostnamefile, "r")) != NULL) 
		{
			fscanf(fp, "%s", Thishost);
			fclose(fp);

			if (strlen(Thishost) > 0)
				return;
		}
	}

	if (gethostname(Thishost, GB_HNAME_LEN) < 0)
		error(1, errno, "gethostnamej() failed");
}

/* check_usage: Check program usage. */
static int checkusage(int argc, char **argv, char *hname, char *jobfile, 
                      int *outputflag, int *getoutputflag, char *jid)
{
	enum 
	{
		GETOUTPUT_NARGS = 3,
		OUTPUT_NARGS    = 5,
		DEF_NARGS       = 4
	};

	int this_option_optind = optind ? optind : 1, option_index = 0, nargs;
	int hoptind = 0, goptind = 0;
	char opt;
	char *gclient_home_tmp;

	static struct option long_options[] = { {"--output-enable", 1, 0, 0}, 
	                                        {"--get-output", 1, 0, 0},
	                                        {"--host", 1, 0, 0} };

	*outputflag = 0;
	*getoutputflag = 0;
	for (;;) 
	{
		this_option_optind = optind ? optind : 1;
		opt = getopt_long(argc, argv, "ogh", long_options, &option_index);
		if (opt == -1)
			break;
		switch (opt) 
		{
			case 'o':
				*outputflag = 1;	
				break;
			case 'g':
				*getoutputflag = 1;	
				goptind = optind;
				break;
			case 'h':
				hoptind = optind;
				break;
			case '?':
				error(0, 0, "Unrecognized option: -%c\n", optopt);
				return -1;
		}
	}

	if (*getoutputflag)
		nargs = GETOUTPUT_NARGS;
	else if (*outputflag)
		nargs = OUTPUT_NARGS;
	else
		nargs = DEF_NARGS;
	
	if (argc != nargs) 
	{
		fprintf(stderr, "Usage:\tgbrun -h host [options] <job>\n");
		fprintf(stderr, "Options\n");
		fprintf(stderr, "\t-o | --output-enable\n\tOutput enable\n");
		fprintf(stderr, 
		        "\t-g | --get-output\n\tGet job output (Interactive mode)\n");
		return -1;
	}

	if (hoptind)
		sprintf(hname, argv[hoptind]);

	if (goptind)
		sprintf(jid, argv[goptind]);
	else
		sprintf(jobfile, argv[argc - 1]);

	if ((gclient_home_tmp = getenv("GBROKER_HOME")) == NULL) 
		fprintf(stderr, "Environment variable GCLIENT_HOME is not set up\n");
	else
		sprintf(Gclient_home, "%s", gclient_home_tmp);

	return 0;
}
