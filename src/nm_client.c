/*
 * nm_client.h: Netmon client functions. 
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include "gbroker.h"
#include "netmon.h"
#include "nm_client.h"
#include "error.h"
#include "tcp.h"

/* nm_get_nettime_for_table: Get time for list of files and list of hosts. */
int nm_get_nettime_for_table(char *host, 
                             char **filelist, int nfiles,
                             nm_hostlist_t *hostlist, int nhosts, 
							 double *stagetimes)
{
	/*
	 * 1. Send request.
	 * 2. Send number of files.
	 * 3. Send files.
	 * 4. Send number of hosts.
	 * 5. Send hostnames.
	 */
	int i;
	SOCKET sock;
	struct timeval tv;

	if ((sock = tcp_client(host, NETMON_PORT)) < 0)
	{
		error(0, errno, "Failed to connect to %s", host);
		return -1;
	}

	/* Set recv timeout interval, because of strage. */
	tv.tv_sec = NM_SOCK_RCVTIMEOUT_SEC;
	tv.tv_usec = NM_SOCK_RCVTIMEOUT_USEC;
	setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));

	/* Send request to get info. */
	if (sendn(sock, GETTABLE_RQST, NM_SERV_MSG_LEN) != NM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed for GETTABLE_RQST");
		return -1;
	}

	/* Send number of files. */
	if (sendn(sock, (char *) &nfiles, sizeof(nfiles)) != sizeof(nfiles))
	{
		close(sock);
		error(0, errno, "sendn() failed for nfiles");
		return -1;
	}

	/* Send number of filenames. */
	for (i = 0; i < nfiles; i++)
		if (sendn(sock, filelist[i], NM_PATH_LEN) != NM_PATH_LEN)
		{
			close(sock);
			error(0, errno, "sendn() failed for filelist[%d]", i);
			return -1;
		}

	/* Send number of hosts. */
	if (sendn(sock, (char *) &nhosts, sizeof(nhosts)) != sizeof(nhosts))
	{
		close(sock);
		error(0, errno, "sendn() failed for nhosts");
		return -1;
	}

	/* Send hostnames and architectures. */
	if (sendn(sock, (char *) hostlist, nhosts * sizeof(nm_hostlist_t)) 
		!= nhosts * sizeof(nm_hostlist_t))
	{
		close(sock);
		error(0, errno, "sendn() failed for hostlist[%d]", i);
		return -1;
	}

	/* Recv stagetimes. */
	if (recvn(sock, (char *) stagetimes, nhosts * sizeof(*stagetimes)) 
		!= nhosts * sizeof(*stagetimes))
	{
		close(sock);
		error(0, errno, "recvn() failed for stagetimes");
		return -1;
	}
	close(sock);

	return 0;
}

/* nm_gettime: Return time to send size. */
double nm_gettime(char *host1, char *host2, double size)
{
	SOCKET sock;
	double des_time;
	char serv_msg[NM_SERV_MSG_LEN];
	struct timeval tv;

	if ((sock = tcp_client(host1, NETMON_PORT)) < 0)
	{
		error(0, errno, "Failed to connect to %s", host1);
		return -1;
	}

	/* Set recv timeout interval, because of strage. */
	tv.tv_sec = NM_SOCK_RCVTIMEOUT_SEC;
	tv.tv_usec = NM_SOCK_RCVTIMEOUT_USEC;
	setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));

	/* Send request to get info. */
	if (sendn(sock, GETINFO_RQST, NM_SERV_MSG_LEN) != NM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	if (sendn(sock, host2, NM_HNAME_LEN) != NM_HNAME_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	/* Recv reply. */
	if (recvn(sock, serv_msg, NM_SERV_MSG_LEN) != NM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "recvn() failed");
		return -1;
	}

	if (!strncmp(serv_msg, GETINFO_NOHOST, NM_SERV_MSG_LEN))
	{
		close(sock);
		/*
		error(0, 0, "No info about bandwidth between %s and %s available",
		      host1, host2);
		*/
		return -1;
	}

	if (sendn(sock, GETBAND_RQST, NM_SERV_MSG_LEN) != NM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	if (sendn(sock, (char *) &size, sizeof(size)) != sizeof(size))
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	/* Receive time. */
	if (recvn(sock, (char *) &des_time, sizeof(des_time)) != sizeof(des_time))
	{
		close(sock);
		error(0, errno, "recvn() failed");
		return -1;
	}

	close(sock);

	return des_time;
}

/* nm_gettimeforfile: Return time to send file. */
double nm_gettimeforfile(char *host1, char *host2, char *file)
{
	SOCKET sock;
	double des_time;
	char serv_msg[NM_SERV_MSG_LEN];
	int rc;
	struct timeval tv;

	if ((sock = tcp_client(host1, NETMON_PORT)) < 0)
	{
		error(0, errno, "Failed to connect to %s", host1);
		return -1;
	}

	/* Set recv timeout interval, because of strage. */
	tv.tv_sec = NM_SOCK_RCVTIMEOUT_SEC;
	tv.tv_usec = NM_SOCK_RCVTIMEOUT_USEC;
	setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));

	/* Send request to get info. */
	if (sendn(sock, GETINFO_RQST, NM_SERV_MSG_LEN) != NM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	if (sendn(sock, host2, NM_HNAME_LEN) != NM_HNAME_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	/* Recv reply. */
	if ((rc = recvn(sock, serv_msg, NM_SERV_MSG_LEN)) != NM_SERV_MSG_LEN)
	{
		error(0, errno, "recvn() failed");
		close(sock);
		return -1;
	}

	if (strncmp(serv_msg, GETINFO_OK, NM_SERV_MSG_LEN))
	{
		close(sock);
		/*
		error(0, 0, "No info about bandwidth between %s and %s available",
		      host1, host2);
		*/
		return -1;
	}

	if (sendn(sock, GETBANDFORFILE_RQST, NM_SERV_MSG_LEN) != NM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	if (sendn(sock, file, NM_PATH_LEN) != NM_PATH_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	/* Receive time. */
	if (recvn(sock, (char *) &des_time, sizeof(des_time)) != sizeof(des_time))
	{
		close(sock);
		error(0, errno, "recvn() failed");
		return -1;
	}

	close(sock);

	return des_time;
}

/* nm_gettable: Return pointer to bandwidth table. */
char *nm_gettable(char *host1, char *host2)
{
	SOCKET sock;
	char *table;
	int table_size;
	char serv_msg[NM_SERV_MSG_LEN];
	struct timeval tv;

	if ((sock = tcp_client(host1, NETMON_PORT)) < 0)
	{
		error(0, errno, "Failed to connect to %s", host1);
		return NULL;
	}

	/* Set recv timeout interval, because of strage. */
	tv.tv_sec = NM_SOCK_RCVTIMEOUT_SEC;
	tv.tv_usec = NM_SOCK_RCVTIMEOUT_USEC;
	setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));

	/* Send request to get info. */
	if (sendn(sock, GETINFO_RQST, NM_SERV_MSG_LEN) != NM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return NULL;
	}

	if (sendn(sock, host2, NM_HNAME_LEN) != NM_HNAME_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return NULL;
	}

	/* Recv reply. */
	if (recvn(sock, serv_msg, NM_SERV_MSG_LEN) != NM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "recvn() failed");
		return NULL;
	}

	if (!strncmp(serv_msg, GETINFO_NOHOST, NM_SERV_MSG_LEN))
	{
		close(sock);
		/*
		error(0, 0, "No info about bandwidth between %s and %s available",
		      host1, host2);
		*/
		return NULL;
	}

	if (sendn(sock, GETTABLE_RQST, NM_SERV_MSG_LEN) != NM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return NULL;
	}

	/* Receive table. */
	if (recvn(sock, (char *) &table_size, sizeof(table_size)) 
		!= sizeof(table_size))
	{
		close(sock);
		error(0, errno, "recvn() failed");
		return NULL;
	}

	if ((table = (char *) malloc(table_size)) == NULL)
	{
		close(sock);
		error(0, errno, "malloc() failed\n");
		return NULL;
	}

	/* Receive table. */
	if (recvn(sock, table, table_size) != table_size)
	{
		close(sock);
		error(0, errno, "recvn() failed");
		return NULL;
	}

	close(sock);

	return table;
}
