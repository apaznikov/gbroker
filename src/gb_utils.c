/*
 * gb_utils.c: Gbroker utils.
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <stdio.h>
#include <string.h>
#include <sys/time.h>

#include "gbroker.h"
#include "gb_subst.h"

char Thishost[GB_HNAME_LEN];

/* strreplace: Find s2 in s1 and replace it by s3. */
static void strreplace(char *s1, char *s2, char *s3);

/* gettimepast: Get time past from 'past' to this moment. */
double gettimepast(struct timeval past)
{
	struct timeval now;

	gettimeofday(&now, NULL);

    return (now.tv_sec * 1E6 + now.tv_usec -
	        past.tv_sec * 1E6 - past.tv_usec) / 1E6;
}

/* pathtoname: Get filename from path. */
char *pathtoname(char *s)
{
	char *ss;

	if ((ss = strrchr(s, '/')) == NULL)
		return s;
	else 
		return ++ss;
}

/* jsdlsubst: Make substitutions with line. */
void jsdlsubst(char *ssrc, gb_host_t *host, char *sdest)
{
	sprintf(sdest, "%s", ssrc);

	strreplace(sdest, GB_JSDLSUBST_ARCH,   host->arch);
	strreplace(sdest, GB_JSDLSUBST_GBHOST, Thishost);
	strreplace(sdest, GB_JSDLSUBST_HOME,   GB_RSLSUBST_HOME);
}

/* strreplace: Find s2 in s1 and replace it by s3. */
static void strreplace(char *s1, char *s2, char *s3)
{
	char s[GB_JOBDESCR_LEN], *p = NULL;

	while ((p = strstr(s1, s2)) != NULL)
	{
		snprintf(s, strlen(s1) - strlen(p) + 1, "%s", s1);
		strcat(s, s3);
		p += strlen(s2);
		strcat(s, p);
		sprintf(s1, "%s", s);
	}
}

/* urltodir: Convert url to dir path (gsiftp://path/file to gsiftp://path/. */
void urltodir(char *urlfile, char *urldir)
{
	char *p;

	p = rindex(urlfile, '/');
	snprintf(urldir, strlen(p), "%s", urldir);
}
