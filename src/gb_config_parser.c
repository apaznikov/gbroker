/*
 * gb_config_parser.c: Gbroker config parser.
 *
 * (C) 2008-2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <libxml/parser.h>
#include <libxml/xmlmemory.h>

#include "gbroker.h"
#include "gb_config_parser.h"
#include "gb_launcher.h"
#include "error.h"
#include "tcp.h"

/* cmpgbhosts: Comparison function for hosts (by number of processors). */
static int cmpgbhosts(const void *host1, const void *host2);

/* readcfg: Read configuration file. */
gb_host_t *readcfg(char *gbroker_home, int *nhosts)
{
	char cfg_file[GB_PATH_LEN];
	int ihost, i;
	gb_host_t *hosts = NULL;
	xmlDocPtr doc;
	xmlNodePtr cur;
	xmlChar *key;

	sprintf(cfg_file, "%s/etc/gbroker.conf", gbroker_home);

	doc = xmlParseFile(cfg_file);

	if (doc == NULL)
	{
		fprintf(stderr, "xmlParseFile() failed\n");
		exit(EXIT_FAILURE);
	}

	cur = xmlDocGetRootElement(doc);

	if (cur == NULL)
	{
		fprintf(stderr, "xmlDocGetRootElement() failed\n");
		xmlFreeDoc(doc);
		exit(EXIT_FAILURE);
	}

	if (xmlStrcmp(cur->name, (const xmlChar *) "gbroker-config"))
	{
		fprintf(stderr, "Wrong config file\n");
		xmlFreeDoc(doc);
		exit(EXIT_FAILURE);
	}

	for (cur = cur->xmlChildrenNode; cur->next != NULL; cur = cur->next)
	{
		if (!xmlStrcmp(cur->name, (const xmlChar *) "host-list"))
		{
			*nhosts = 0;
			for (cur = cur->xmlChildrenNode; cur->next != NULL; cur = cur->next)
			{
				if (!xmlStrcmp(cur->name, (const xmlChar *) "host"))
					(*nhosts)++;
			}
			cur = cur->parent;

			if ((hosts = (gb_host_t *) malloc(sizeof(gb_host_t) * 
				                              (*nhosts))) == NULL)
			{
				fprintf(stderr, "malloc() failed\n");
				xmlFreeDoc(doc);
				exit(EXIT_FAILURE);
			}

			ihost = 0;
			for (cur = cur->xmlChildrenNode; cur->next != NULL; cur = cur->next)
			{
				if (!xmlStrcmp(cur->name, (const xmlChar *) "host"))
				{
					key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
					sprintf(hosts[ihost].hname, "%s", (char *) key);
					getshorthname(hosts[ihost].hname, hosts[ihost].shname);
					xmlFree(key);
					ihost++;
				}
			}
			cur = cur->parent;
		}
	}

	/* Sort and init hosts' IDs. */
	if (hosts != NULL)
	{
		for (i = 0; i < *nhosts; i++)
		{	
			if ((hosts[i].nprocs = getnprocs_quick(hosts[i].hname)) < 0)
			{
				/* error(0, 0, "getnprocs() failed for %s", hosts[i].hname); */
				hosts[i].nprocs = 0;
			}
		}

		qsort(hosts, *nhosts, sizeof(gb_host_t), cmpgbhosts);

		for (i = 0; i < *nhosts; i++)
			hosts[i].id = i;
	}

	/*
	for (ihost = 0; ihost < *nhosts; ihost++)
		printf("host[%d] = '%s'\n", ihost, hosts[ihost].hname);
	*/

	xmlFreeDoc(doc);
	xmlCleanupParser();

	return hosts;
}

/* cmpgbhosts: Comparison function for hosts (by number of processors). */
static int cmpgbhosts(const void *host1, const void *host2)
{
	const gb_host_t *h1 = (const gb_host_t *) host1;
	const gb_host_t *h2 = (const gb_host_t *) host2;

	return h2->nprocs - h1->nprocs;
}
