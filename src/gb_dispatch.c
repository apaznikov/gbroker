/*
 * gb_dispatch.c: Gbroker job dispatching functions.
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include <pthread.h>
#include <dirent.h>
#include <pwd.h>

#include "gbroker.h"
#include "gb_job_parser.h"
#include "gb_job_list.h"
#include "gb_job_monitor.h"
#include "gb_job.h"
#include "gb_launcher.h"
#include "gb_sched.h"
#include "gb_dispatch.h"
#include "gb_utils.h"
#include "tcp.h"
#include "error.h"

char Gbroker_home[GB_PATH_LEN];
char Thishost[GB_HNAME_LEN];
int  Jid = 1;

pthread_mutex_t Jid_mutex;
pthread_mutex_t Jobsubmit_mutex;

/* makersl: Make RSL description file from job structure. */
static int makersl(gb_job_t *job, char *rsljob, gb_host_t *host);

/* 
 * makejobdirs: Make job dir on remote hosts and old dirs in stage-out 
 *              destinations
 */
int makejobdirs(gb_job_t *job, gb_host_t *host);

/* initjobstruct: Initialize job structure. */
static int initjobstruct(gb_job_t *job, gb_host_t *host,
                         gb_schedhosts_t *schedhost);

/* recvjid: Get global job id from gbroker. */
static int recvjid(SOCKET sock, gb_job_t *job);

/* sendjid: Send global job id to client. */
static int sendjid(SOCKET sock, gb_job_t *job);

#ifdef _EXEC_HARNESS 

/* findexec: Find execution files (possibly for many architectures!). */
static int findexec(gb_job_t *job, SOCKET *sock);

/* sendexec: Send execution files (possibly for many architectures). */
static int sendexec(gb_job_t *job, SOCKET *sock);

/* recvexec: Recv all execution files (possibly for many architectures). */
static int recvexec(SOCKET sock, gb_job_t *job);

#endif /* _EXEC_HARNESS */

/* makedirs: Make temporary dirs in the user's home. */
static int makedirs(gb_job_t *job);

/* submittohost: Submit job to host, return contact. */
int submittohost(gb_job_t *job, gb_host_t *host, gb_schedhosts_t *schedhost)
{
	/*
	 * 1. Allocate subsystem on free processors.
	 * 2. Make rsl from job structure.
	 * 3. Execute globusrun.
	 * 4. Init job stucture.
	 * 5. Add job to job list.
	 */

	if (getnodesppn(host->hname, job->rank, 
		            &job->nodes[host->id], &job->ppn[host->id]) < 0)
	{
		error(0, 0, "getnodesppn() failed");
		return -1;
	}

	if (makejobdirs(job, host) < 0)
	{
		error(0, 0, "makejobdirs failed for job %d", job->id);
		return -1;
	}

	if (makersl(job, job->rsl[host->id], host) < 0)
	{
		error(0, 0, "makersl() failed");
		return -1;
	}

	job_resource_add_adj(job, host);

	if (globusrun(host->hname, job->rsl[host->id], job->contact[host->id]) < 0)
	{
		error(0, 0, "globusrun() failed");
		return -1;
	}

	if (initjobstruct(job, host, schedhost) < 0)
	{
		error(0, 0, "initjobstruct() failed");
		return -1;
	}

	return 0;
}

/* migrate: Try to migrate job from cureent host to better host. */
int migrate(gb_job_t *job, int currhid)
{
	char prevcontact[GB_CONTACT_LEN];
	gb_host_t *prevhost;
	gb_schedhosts_t *schedhosts;
	int i, newhid;
	double newhost_obj, currhost_obj;

	/*
	 * 1. Find suitable host, make schedule decision.
	 * 2. If this host differs from current, submit job on new host.
	 * 3. Kill old job. 
	 */

	prevhost = job->submithosts[currhid];

	applog("Try to migrate job %d (%s) from %s...", job->id, job->name, 
	                                                prevhost->hname);

	job->migrhost = currhid;
	if (dosched(job, &schedhosts) < 0)
	{
		error(0, 0, "dosched() failed");
		return -1;
	}

	if (schedhosts[0].obj < 0)
	{
		error(0, 0, "No suitable subsystem for job %s to migrate", job->name);
		return 0;
	}

	newhid = schedhosts[0].hid;
	newhost_obj = schedhosts[0].obj;

	/* Find current host's object value in schedhosts. */
	for (i = 0; i < Nhosts; i++)
		if (schedhosts[i].hid == currhid)
		{
			currhost_obj = schedhosts[i].obj;
			break;
		}

	/*
	fprintf(stderr, "prev[%d] %f and new[%d] %f\n", currhid, currhost_obj,
				                                    newhid, newhost_obj);
	*/

	/*
	 * If new host is the current host or 
	 * obj function gain is too little, don't migrate.
	 */
	applog("Cur = %f, New = %f\n", currhost_obj, newhost_obj);

	if ((currhid == newhid) || (currhost_obj < newhost_obj) || 
	    (currhost_obj - newhost_obj < GB_MIGRATE_OBJ_CRIT))
		return 0;

	/* Migrate job */
	sprintf(prevcontact, "%s", job->contact[currhid]);

	job->submithosts[currhid] = NULL;

	job_resource_remove_adj(job, prevhost);
	
	if (globuskill(prevcontact) < 0)
	{
		error(0, 0, "globuskill() failed for job %s\n", job->name);
		return -1;
	}

	if (submittohost(job, &Hosts[newhid], &schedhosts[0]) < 0)
	{
		error(0, 0, "submittohost() failed for host %s", Hosts[newhid].hname);
		return -1;
	}

	applog("Job %d (%s) was migrated from %s to %s", 
	       job->id, job->name, prevhost->hname, Hosts[newhid].hname);

	return 0;
}

/* killrest: Kill all jobs, not submitted to hid. */
int killrest(gb_job_t *job, int hid)
{
	int i;

	if (GB_NSUBMITS > 1)
	{
		for (i = 0; i < Nhosts; i++)
		{
			if ((i != hid) && (job->submithosts[i] != NULL))
			{
				job_resource_remove_adj(job, job->submithosts[i]);

				if (globuskill(job->contact[i]) < 0)
					error(0, 0, "globuskill() failed for job %s\n", job->name);

				job->submithosts[i] = NULL;
			}
		}
	}

	job->exechost = job->submithosts[hid];

	return 0;
}

/* initjobstruct: Initialize job structure. */
static int initjobstruct(gb_job_t *job, gb_host_t *host, 
                         gb_schedhosts_t *schedhost)
{
	gettimeofday(&job->hostsubmittime, NULL);
	gettimeofday(&job->migrtime, NULL);

	job->submithosts[host->id] = host;

	/* Init sumibtted host state in past. */
	job->schedhosts[host->id] = *schedhost;

	sprintf(job->status[host->id], GB_JOB_STATUS_INDEF);

	sem_init(&job->complete_sem, 0, 0);

	job->execflag = 0;

	return 0;
}

/* sendjob: Send job to gbroker. */
int sendjob(char *host, gb_job_t *job, SOCKET *sock)
{
	if ((*sock = tcp_client(host, GBROKER_PORT)) < 0)
		return -1;

	/*
	 * 1. Send job structure.
	 * 2. Send number of stages. 
	 * 3. Send stages fields. 
	 * 4. Send executable file
	 */

	if (sendn(*sock, GB_JOBSUBMIT_RQST, GB_SRVMSG_LEN) != GB_SRVMSG_LEN)
	{
		error(0, errno, "sendn() failed for GB_JOBSUBMIT_RQST");
		return -1;
	}
	
	if (sendn(*sock, (char *) job, sizeof(gb_job_t)) != sizeof(gb_job_t)) 
	{
		error(0, errno, "sendn() failed for job");
		return -1;
	}

	if (sendn(*sock, (char *) &(job->nstages), sizeof(job->nstages)) 
		!= sizeof(job->nstages)) 
	{
		error(0, errno, "sendn() failed for nstages");
		return -1;
	}

	if (sendn(*sock, (char *) job->stages, sizeof(gb_stage_t) * job->nstages) 
		!= sizeof(gb_stage_t) * job->nstages) 
	{
		error(0, errno, "sendn() failed for stages");
		return -1;
	}

	/*
	if (findexec(job, sock) < 0)
	{
		error(0, 0, "findexec() failed for job %s", job->name);
		return -1;
	}

	if (sendexec(job, sock) < 0)
	{
		error(0, 0, "sendexec() failed for job %s", job->name);
		return -1;
	}
	*/

	if (recvjid(*sock, job) < 0)
	{
		error(0, 0, "recvjid() failed");
		return -1;
	}

	return 0;
}

#ifdef _EXEC_HARNESS
/* findexec: Find execution files (possibly for many architectures!). */
static int findexec(gb_job_t *job, SOCKET *sock)
{
	char execdir[GB_PATH_LEN], path[GB_PATH_LEN], *execname, *arch;
	int execname_len;
	DIR *d;
	struct dirent *dir;
	int i;

	/* Get path from exec name. */
	execname = pathtoname(job->exec);
	execname_len = strlen(execname);

	if ((job->exec[0] != '/') && \
		!((job->exec[0] == '.') && (job->exec[1] == '/')))
		snprintf(execdir, strlen(job->exec) - execname_len + 3, 
				 "./%s", job->exec); 
	else
		snprintf(execdir, strlen(job->exec) - execname_len + 1, 
				 "%s", job->exec); 

	/* Read exec dir to get all execution files (for architectures). */
	fprintf(stderr, "execdir = %s\n", execdir);
	if ((d = opendir(execdir)) == NULL)
		error(0, errno, "opendir() failed for %s", execdir);
	else
	{
		sprintf(execname, "%s.", execname);

		/* Count number of architectures. */
		while ((dir = readdir(d)) != NULL)
			if (strstr(dir->d_name, execname) 
				&& (strlen(dir->d_name) > execname_len))
				job->narchs++;

		/* Allocate memory for job->execlist. */
		if ((job->execlist = malloc(job->narchs * sizeof(gb_execlist_t)))==NULL)
		{
			error(0, errno, "malloc() failed for job->execlist");
			return -1;
		}

		/* Look over dir and get all exec files' names. */
		rewinddir(d);
		i = 0;
		while ((dir = readdir(d)) != NULL)
		{
			if (strstr(dir->d_name, execname))
			{
				sprintf(path, "%s/%s", execdir, dir->d_name);
				fprintf(stderr, "access(%s)\n", dir->d_name);
				if (access(path, X_OK) < 0)
					error(0, 0, "%s must be executable", dir->d_name);
				else
				{
					sprintf(job->execlist[i].exec, "%s", path);
					arch = rindex(dir->d_name, '.');
					arch++;
					sprintf(job->execlist[i].arch, "%s", arch);
					i++;
				}
			}
		}
		job->narchs = i;

		if (access(job->exec, X_OK) < 0)
		{
			job->is_defexec_exists = 0;

			if (job->narchs == 0)
			{
				error(0, 0, "%s must be executable", dir->d_name);
				error(0, 0, "Fail! There is no executable file.");
				return -1;
			}
		}

		closedir(d);
	}

	return 0;
}

/* sendexec: Send execution files (possibly for many architectures). */
static int sendexec(gb_job_t *job, SOCKET *sock)
{
	int i;

	/* Number of exec files. */
	if (sendn(*sock, (char *) &job->narchs, sizeof(job->narchs)) 
		!= sizeof(job->narchs))
	{
		error(0, errno, "sendn() failed for job->narchs");
		return -1;
	}

	/* Send exec files' names. */

	if (sendn(*sock, (char *)job->execlist, sizeof(gb_execlist_t) * job->narchs)
		!= sizeof(gb_execlist_t) * job->narchs)
	{
		error(0, errno, "sendn() failed for execlist");
		return -1;
	}

	/* Send exec files. */
	for (i = 0; i < job->narchs; i++)
	{
		if (sendfile(*sock, job->execlist[i].exec) < 0)
		{
			error(0, errno, "sendfile() failed for %s", job->execlist[i]);
			return -1;
		}
	}

	/* If default exec file is specified. */
	if (sendn(*sock, (char *) &job->is_defexec_exists, 
		sizeof(job->is_defexec_exists)) != sizeof(job->is_defexec_exists))
	{
		error(0, errno, "sendn() failed for GB_TRUE");
		return -1;
	}

	if (job->is_defexec_exists)
	{
		if (sendfile(*sock, job->exec) < 0)
		{
			error(0, errno, "sendfile() failed for %s", job->exec);
			return -1;
		}
	}

	return 0;
}

/* recvexec: Recv all execution files (possibly for many architectures). */
static int recvexec(SOCKET sock, gb_job_t *job)
{
	int i;
	char execname[GB_JOBDESCR_LEN];     

	/* Recv number of exec files. */
	if (recvn(sock, (char *) &job->narchs, sizeof(job->narchs)) != 
		sizeof(job->narchs))
	{
		error(0, 0, "recvn() failed for job->narchs");
		return -1;
	}

	/* Allocate memory for job->execlist. */
	if ((job->execlist = malloc(job->narchs * sizeof(gb_execlist_t))) == NULL)
	{
		error(0, errno, "malloc() failed for job->execlist");
		return -1;
	}

	/* Recv exec files. */
	if (recvn(sock, (char *) job->execlist, job->narchs * sizeof(gb_execlist_t))
		!= job->narchs * sizeof(gb_execlist_t))
	{
		error(0, 0, "recvn() failed for execlist");
		return -1;
	}

	for (i = 0; i < job->narchs; i++)
	{
		sprintf(execname, "%s", pathtoname(job->execlist[i].exec));
		sprintf(job->execlist[i].exec, "%s/%s", job->jobdir, execname);
	}

	/* Recv exec files. */
	for (i = 0; i < job->narchs; i++)
	{
		if (recvfile(sock, job->execlist[i].exec) < 0)
		{
			error(0, errno, "recvfile() failed for %s", job->execlist[i].exec);
			return -1;
		}

		if (chmod(job->execlist[i].exec, 0700) < 0)
		{
			error(0, 0, "chmod() failed for %s", job->execlist[i].exec);
			return -1;
		}
	}

	/* If default exec file is specified. */
	if (recvn(sock, (char *) &job->is_defexec_exists, 
		sizeof(job->is_defexec_exists)) != sizeof(job->is_defexec_exists))
	{
		error(1, errno, "recvn() failed for is_defexec_exists");
		return -1;
	}

	if (job->is_defexec_exists)
	{
		sprintf(job->execname, "%s", job->exec);
		sprintf(job->exec, "%s/%s", job->jobdir, execname);

		if (recvfile(sock, job->exec) < 0)
		{
			error(0, 0, "recvfile() failed for %s", job->exec);
			return -1;
		}

		if (chmod(job->exec, 0700) < 0)
		{
			error(0, 0, "chmod() failed for %s", job->exec);
			return -1;
		}
	}

	return 0;
}

#endif /* _EXEC_HARNESS */

/* recvjid: Get global job id from gbroker. */
static int recvjid(SOCKET sock, gb_job_t *job)
{
	if (recvn(sock, job->gid, GB_GID_LEN) != GB_GID_LEN)
		return -1;
	
	return 0;
}

/* recvjob: Recv job from client. */
int recvjob(SOCKET sock, gb_job_t *job)
{
	int i;

	/*
	 * 1. Recv job structure.
	 * 2. Recv number of job stages.
	 * 3. Malloc for stages.
	 * 4. Recv job stages fields.
	 * 5. Recv executable file.
	 * 6. Set submithosts and exechost by NULL.
	 */

	if (recvn(sock, (char *) job, sizeof(gb_job_t)) != sizeof(gb_job_t)) 
	{
		error(0, errno, "recvn() failed for job");
		return -1;
	}

	if (recvn(sock, (char *) &(job->nstages), sizeof(job->nstages)) 
		!= sizeof(job->nstages)) 
	{
		error(0, errno, "recvn() failed for nstages");
		return -1;
	}

	if ((job->stages = (gb_stage_t *) malloc(sizeof(gb_stage_t) * 
		                                     job->nstages)) == NULL)
	{
		error(0, errno, "malloc() failed for stages");
		return -1;
	}

	if (recvn(sock, (char *) job->stages, sizeof(gb_stage_t) * job->nstages) 
		!= sizeof(gb_stage_t) * job->nstages) 
	{
		error(0, errno, "recvn() failed for stages");
		return -1;
	}

	if (jobmalloc(job) < 0)
	{
		error(0, 0, "jobmalloc() failed for job %s", job->name);
		return -1;
	}

	pthread_mutex_lock(&Jid_mutex);
	job->id = Jid;
	Jid++;
	pthread_mutex_unlock(&Jid_mutex);

	if (makedirs(job) < 0)
	{
		error(0, 0, "makedirs() failed");
		return -1;
	}

	/*
	if (recvexec(sock, job) < 0)
	{
		error(0, 0, "recvexec() failed for %s", job->name);
		return -1;
	}
	*/

	for (i = 0; i < Nhosts; i++)
		job->submithosts[i] = NULL;
	job->exechost = NULL;

	sprintf(job->gid, "%s@%d", Thishost, job->id);

	if (sendjid(sock, job) < 0)
	{
		error(0, 0, "sendjid() failed");
		close(sock);
		return -1;
	}

	return 0;
}

/* makedirs: Make temporary dirs in the user's home. */
static int makedirs(gb_job_t *job)
{
	struct passwd *pwd;
	char user_gbdir[GB_PATH_LEN];
	#ifdef _GBHOME_CLEANUP
	char cmd[GB_CMD_LEN];
	#endif /* _GBHOME_CLEANUP */
	int fd;

	if ((pwd = getpwnam(job->username)) == NULL)
	{
		error(0, errno, "getpwnam() failed for %s", job->username);
		return -1;
	}

	sprintf(user_gbdir, "%s/.gbroker", pwd->pw_dir);

	/* Clean-up .gbroker dir in user home (only when job is first!). */
	#ifdef _GBHOME_CLEANUP
	if (job->id == 1)
	{
		sprintf(cmd, "rm -r %s/*-%s*", user_gbdir, Thishost);
		system(cmd);
	}
	#endif /* _GBHOME_CLEANUP */

	if ((fd = open(user_gbdir, O_EXCL)) < 0)
		if (mkdir(user_gbdir, 0700) < 0)
			error(0, errno, "mkdir() failed for %s", user_gbdir);

	sprintf(job->locdir, "%s/%d-%s", user_gbdir, job->id, Thishost);

	if ((fd = open(job->locdir, O_EXCL)) < 0)
		if (mkdir(job->locdir, 0700) < 0)
		{
			error(0, errno, "mkdir() failed for %s", job->locdir);
			return -1;
		}
	close(fd);

	return 0;
}


/* sendjid: Send global job id to client. */
static int sendjid(SOCKET sock, gb_job_t *job)
{
	if (sendn(sock, job->gid, GB_GID_LEN) != GB_GID_LEN)
	{
		error(0, errno, "sendn() failed for %s", job->gid);
		return -1;
	}
	return 0;
}

/* sendoutput: Send job stdout to client. */
int sendoutput(SOCKET sock, gb_job_t *job)
{
	char stout[GB_PATH_LEN];

	sprintf(stout, "%s/%s", job->locdir, pathtoname(job->stout));

	if (sendfile(sock, stout) < 0)
	{	
		error(0, 0, "sendfile() failed");
		return -1;
	}

	return 0;
}

/* recvoutputbyjid: Recv output from gbroker by JID. */
int recvoutputbyjid(char *jid, char *stout, gb_jobstate_t *jobstate)
{
	char host[GB_HNAME_LEN] = "\0";
	SOCKET sock;
	int i, j = 0, id;

	for (i = 0; i < strlen(jid); i++)
	{
		if (jid[i] == '@')
			break;

		host[j] = jid[i];
		j++;
	}

	if ((sock = tcp_client(host, GBROKER_PORT)) < 0)
		return -1;

	if (sendn(sock, GB_GETOUTPUT_RQST, GB_SRVMSG_LEN) != GB_SRVMSG_LEN)
	{
		error(0, errno, "sendn() failed for GB_GETOUTPUT_RQST");
		return -1;
	}

	id = atoi(rindex(jid, '@') + 1);
	if (sendn(sock, (char *) &id, sizeof(id)) != sizeof(id))
	{
		error(0, errno, "sendn() failed for %d", id);
		return -1;
	}

	if (recvn(sock, (char *) jobstate, sizeof(*jobstate)) != sizeof(*jobstate))
	{
		error(0, errno, "recvn() failed for srvmsg");
		return -1;
	}

	if (*jobstate == GB_JOBCOMPLETE_ST)
	{
		if (recvn(sock, stout, GB_JOBDESCR_LEN) != GB_JOBDESCR_LEN)
		{
			error(0, errno, "recvn() failed for %s", stout);
			return -1;
		}

		if (recvfile(sock, stout) < 0)
			error(1, 0, "recvfile() failed");
	}

	return 0;
}

/* recvreply: Recv return code from the dispatcher. */
int recvreply(SOCKET sock, char *srvmsg)
{
	if (recvn(sock, srvmsg, GB_SRVMSG_LEN) != GB_SRVMSG_LEN)
	{
		error(1, errno, "recvn() failed for srvmsg");
		return -1;
	}

	return 0;
}

/* 
 * makejobdirs: Make job dir on remote hosts and old dirs in stage-out 
 *              destinations
 */
int makejobdirs(gb_job_t *job, gb_host_t *host)
{
	/* Make remote job dir. */
	sprintf(job->remdir, "~/.gbroker/%s-%s@%d", Thishost, host->hname, job->id);

	if (remote_mkdir(host->hname, job->remdir) < 0)
	{
		error(0, 0, "remote_mkdir() failed for (%s, %s)", 
		      host->hname, job->remdir);
		return -1;
	}

	return 0;
}

/* makersl: Make RSL description file from job structure. */
static int makersl(gb_job_t *job, char *rsljob, gb_host_t *host)
{
	FILE *frsl;
	int flag = 0, i;
	char withsubst[GB_JOBDESCR_LEN]; /* line with substitutions */
	/* int archfoundflag; */

	/* Make RSL. */
	sprintf(rsljob, "%s/rsl", job->locdir);
	if ((frsl = fopen(rsljob, "w")) == NULL)
		error(0, errno, "fopen() failed for %s", rsljob);

	jsdlsubst(job->exec, host, withsubst);
	fprintf(frsl, "&(executable=%s)\n", withsubst);
	/*
	for (i = 0; i < job->narchs; i++)
		if (!strcmp(job->execlist[i].arch, host->arch))
		{
			archfoundflag = 1;
			fprintf(frsl, "&(executable=gsiftp://%s%s)\n", Thishost, 
			        job->execlist[i].exec);
			break;
		}

	if ((!archfoundflag) && (job->is_defexec_exists))
		fprintf(frsl, "&(executable=gsiftp://%s/%s)\n", Thishost, job->exec);
	*/

	if (job->args[0] != '\0')
	{
		jsdlsubst(job->args, host, withsubst);
		fprintf(frsl, " (arguments=%s)\n", withsubst);
	}

	fprintf(frsl, " (queue=release)\n");
	fprintf(frsl, " (directory=$(HOME)/.gbroker/%s-%s@%d)\n", 
	        Thishost, host->hname, job->id);
	jsdlsubst(job->stout, host, withsubst);
	fprintf(frsl, " (stdout=%s)\n", withsubst);
	jsdlsubst(job->sterr, host, withsubst);
	fprintf(frsl, " (stderr=%s)\n", withsubst);
 
	if (job->rank > 1)
		fprintf(frsl, " (jobtype=mpi)\n");
	fprintf(frsl, " (host_count=%d)\n", job->nodes[host->id]);
	fprintf(frsl, " (count=%d)\n", job->ppn[host->id]);

	/*
	 * Stage-in 
	 */
	flag = 0;

	for (i = 0; i < job->nstages; i++)
		if (job->stages[i].dest[0] == '\0')
		{
			if (!flag)
			{
				fprintf(frsl, " (file_stage_in=");
				flag = 1;
			}

			
			jsdlsubst(job->stages[i].src, host, withsubst);
			fprintf(frsl, "(%s ", withsubst);
			jsdlsubst(job->stages[i].name, host, withsubst);
			fprintf(frsl, "%s)", withsubst);
		}

	if (flag)
		fprintf(frsl, ")\n");

	/*
	 * Stage-out 
	 */
	fprintf(frsl, " (file_stage_out=");
	jsdlsubst(job->stout, host, withsubst);
	fprintf(frsl, "(%s gsiftp://%s%s/%s)", 
	        withsubst, Thishost, job->locdir, pathtoname(withsubst));
	jsdlsubst(job->sterr, host, withsubst);
	fprintf(frsl, "(%s gsiftp://%s%s/%s)", 
	        withsubst, Thishost, job->locdir, pathtoname(withsubst));

	for (i = 0; i < job->nstages; i++)
		if (job->stages[i].src[0] == '\0')
		{
			jsdlsubst(job->stages[i].name, host, withsubst);
			fprintf(frsl, "(%s ", withsubst);
			jsdlsubst(job->stages[i].dest, host, withsubst);
			fprintf(frsl, "%s)", withsubst);
		}
	fprintf(frsl, ")\n");

	fclose(frsl);

	return 0;
}

/* clearjob: Remove temprorary job files and dir. */
int clearjob(gb_job_t *job)
{
	char file[GB_PATH_LEN];

	sprintf(file, "%s/%s", job->locdir, pathtoname(job->exec));
	if (unlink(file) < 0)
	{
		error(0, errno, "unlink() failed");
		return -1;
	}

	sprintf(file, "%s/rsl", job->locdir);
	if (unlink(file) < 0)
	{
		error(0, errno, "unlink() failed");
		return -1;
	}

	sprintf(file, "%s/%s", job->locdir, pathtoname(job->stout));
	if (unlink(file) < 0)
	{
		error(0, errno, "unlink() failed");
		return -1;
	}

	if (rmdir(job->locdir) < 0)
	{
		error(0, errno, "rmdir() failed");
		return -1;
	}

	return 0;
}

/* dispatch_init: Init job dispatcher. */
void dispatch_init(void)
{
	pthread_mutex_init(&Jid_mutex, NULL);
}
