/*
 * gb_sched.c: Scheduling functions.
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <math.h>

#include "gbroker.h"
#include "dcsmon.h"
#include "gb_launcher.h"
#include "gb_sched.h"
#include "gb_dispatch.h"
#include "gb_job_list.h"
#include "gb_utils.h"
#include "error.h"
#include "tcp.h"

char Thishost[GB_HNAME_LEN];
gb_host_t *Hosts;
int Nhosts;

struct stage_hosts_t
{
	char hname[GB_HNAME_LEN];
} *stage_hosts = NULL;

/* compjob: Compute objective function for schedhosts. */
static int compobj(gb_job_t *job, gb_schedhosts_t *schedhosts);

/* isexecuted: If job is executed on host with hid. */
static int isexecuted(gb_job_t *job, int hid);

/* getstat: Get info from subsystems to make scheduler decision. */
static int getstat(gb_job_t *job, gb_schedhosts_t **schedhosts);

/* parseURL: Parse URL to hostname and filename. */
static int parseURL(char *URL, char *hname, char *file);

/* gettimetostage: Get time to stage-in to host. */
static int gettimetostage(gb_job_t *job, gb_schedhosts_t *schedhosts);

/* make_hoststages: Count number of files, malloc memory. */
static int make_hoststages(gb_job_t *job, hoststage_t **hoststages,
                           int *n_stagein_hosts);

/* findhost: Return host number of hostname in stage_hosts. */
static int findhost(char *hname, struct stage_hosts_t *stage_hosts, int nhosts);

/* cmphosts: Comparison function for hosts (by value of objective func). */
static int cmphosts(const void *host1, const void *host2);

/* cmpint: Comparison function for qsort. */
static int cmpproc(const void *a, const void *b);

/* 
 * dosched: Make schedule decision. 
 *          migrhost is HID, from which job is migrating now. 
 */
int dosched(gb_job_t *job, gb_schedhosts_t **schedhosts)
{
	/*
	 * 0. Prepare sorted hosts array.
	 * 1. Get information about system.
	 * 2. Compute objective function and make schedule decision.
	 * 3. Sort hosts by decrease of objective function.
	 */

	if ((*schedhosts = malloc(sizeof(gb_schedhosts_t) * Nhosts)) == NULL)
	{
		error(0, errno, "malloc() failed for schedhosts");
		return -1;
	}

	if (getstat(job, schedhosts) < 0)
	{
		error(0, 0, "getstat() failed");
		return -1;
	}

	if (compobj(job, *schedhosts) < 0)
	{
		error(0, 0, "compobj() failed");
		return -1;
	}

	qsort(*schedhosts, Nhosts, sizeof(gb_schedhosts_t), cmphosts);

	return 0;
}

/* compobj: Compute objective function for schedhosts. */
static int compobj(gb_job_t *job, gb_schedhosts_t *schedhosts)
{
	double maxstage, maxnprocs, maxnjobs, *njobs, *nprocs_inv, *stagetime;
	int i, j;
	char logline[GB_LOGLINE_LEN];

	if ((njobs = malloc(sizeof(*njobs) * Nhosts)) == NULL)
	{
		error(0, errno, "malloc() failed for *njobs");
		return -1;
	}

	if ((stagetime = malloc(sizeof(*stagetime) * Nhosts)) == NULL)
	{
		error(0, errno, "malloc() failed for *stagetime");
		return -1;
	}

	if ((nprocs_inv = malloc(sizeof(*nprocs_inv) * Nhosts)) == NULL)
	{
		error(0, errno, "malloc() failed for *njobs");
		return -1;
	}

	if (job->migrhost >= 0)
		/* 
		 * If migration is initialized, fill host state, 
		 * from which job is migrated, by _old state_. 
		 */
	{
		/* 
		 * Previously was: 
		 * schedhosts[job->migrhost] = job->schedhosts[job->migrhost];
		 */

		/* 
		 * Suppose that _number of jobs_ is such like at the moment of 
		 * scheduling decision. 
		 * But _number of processors_ corresponds to the moment of 
		 * migration attempt.
		 */
		schedhosts[job->migrhost].nrjobs=job->schedhosts[job->migrhost].nrjobs;
		schedhosts[job->migrhost].nqjobs=job->schedhosts[job->migrhost].nqjobs;
		schedhosts[job->migrhost].totprocs = 
			job->schedhosts[job->migrhost].totprocs;

		/* All files have been already staged. */
		schedhosts[job->migrhost].stagetime = 0;
	}

	/* Compute objective function components. */
	for (i = 0; i < Nhosts; i++)
	{
		if ((!isexecuted(job, i)) && (schedhosts[i].totprocs >= job->rank))
			/* For those hosts, where job was not been submitted. */
		{
			/* 
			 * If time to stage is near-zero, 
			 * let it be some non-zero eps value. 
			 */
			if (schedhosts[i].stagetime < GB_STAGETIME_EPS)
				stagetime[i] = GB_STAGETIME_EPS;
			else
				stagetime[i] = schedhosts[i].stagetime;

			/* Initialize njobs and nprocs_inv. */
			/*
			 * Njobs is the number of jobs waiting in queue 
			 * PER processor in subsystem.
			 */
			#ifndef GB_FUNCOBJ_NJOBS_QR
			schedhosts[i].nrjobs = 0;
			#endif

			njobs[i] = (schedhosts[i].nrjobs + schedhosts[i].nqjobs)
			            / schedhosts[i].totprocs; 

			#ifndef GB_FUNCOBJ_FREEPROCS
			schedhosts[i].frprocs = 0;
			#endif

			if (schedhosts[i].frprocs < 1)
				nprocs_inv[i] = 1;
			else
				nprocs_inv[i] = 1 / schedhosts[i].frprocs;
		}
	}

	/* Find max values. */
	maxstage = 0;
	maxnprocs = 0;
	maxnjobs = 0;

	/* Find first suitable values. */
	for (j = 0; j < Nhosts; j++)
		if ((!isexecuted(job, j)) && (schedhosts[j].totprocs >= job->rank))
			/* For those hosts, where job was not been submitted. */
		{
			maxstage = stagetime[j];
			maxnprocs = nprocs_inv[j];
			maxnjobs = njobs[j];
			break;
		}

	/* Find max. */
	for (i = j + 1; i < Nhosts; i++)
		if ((!isexecuted(job, i)) && (schedhosts[i].totprocs >= job->rank))
			/* For those hosts, where job was not been submitted. */
		{
			if (stagetime[i] > maxstage)
				maxstage = stagetime[i];

			if (nprocs_inv[i] > maxnprocs)
				maxnprocs = nprocs_inv[i];

			if (njobs[i] > maxnjobs)
				maxnjobs = njobs[i];
		}

	/* Let max values to be non-zero. */
	if (maxstage < EPS)
		maxstage = 1;
	if (maxnprocs < EPS)
		maxnprocs  = 1;
	if (maxnjobs < EPS)
		maxnjobs = 1;
	
	/*
	 * Compute objective function
	 */
	for (i = 0; i < Nhosts; i++)
	{
		if ((!isexecuted(job, i)) && (schedhosts[i].totprocs >= job->rank))
			/* For those hosts, where job was not been submitted. */
		{
			if ((schedhosts[i].frprocs >= job->rank) && 
					 (schedhosts[i].nqjobs < 1))
				schedhosts[i].obj = stagetime[i] / maxstage;
			else
				schedhosts[i].obj = stagetime[i] / maxstage + 
									nprocs_inv[i] / maxnprocs + 
									njobs[i] / maxnjobs;
		}
		else
			schedhosts[i].obj = -1;
	}

	#ifdef GB_SCHEDULER_DEBUG
	applog("SCHEDULER: JOB %d", job->id);
	applog("SCHEDULER: host\tobj\tstage\t\tcpu\t\tqueue");
	for (i = 0; i < Nhosts; i++)
	{
		if (!isexecuted(job, i))
		{
			sprintf(logline, "SCHEDULER: %s:\t%0.2f\t", Hosts[i].shname, 
					schedhosts[i].obj);
			if (schedhosts[i].totprocs < job->rank) 
				continue;
			else if ((schedhosts[i].frprocs >= job->rank) && 
					 (schedhosts[i].nqjobs < 1))
				sprintf(logline, "%s%0.2f (%0.2f)\t0.00 (%0.0f)\t0.00 (0.00)", 
						logline, stagetime[i] / maxstage, stagetime[i], 
						schedhosts[i].frprocs);
			else
			{
				sprintf(logline, 
			    "%s%0.2f (%0.2f)\t%0.2f (%0.0f)\t%0.2f (%0.0f / %0.0f = %0.2f)",
						logline, stagetime[i] / maxstage, stagetime[i], 
						nprocs_inv[i] / maxnprocs, schedhosts[i].frprocs, 
						njobs[i] / maxnjobs, 
						schedhosts[i].nqjobs + schedhosts[i].nrjobs, 
						schedhosts[i].totprocs, njobs[i]);
			}
		}
		else
		{
			sprintf(logline, "SCHEDULER: %s:\t%0.2f\t", Hosts[i].shname, 
					schedhosts[i].obj);
			sprintf(logline, "%s--   (--)\t--   (--)\t--   (--)", logline);
		}

		applog(logline);
	}
	applog("");
	#endif

	free(njobs);
	free(stagetime);
	free(nprocs_inv);

	return 0;
}

/* isexecuted: If job is executed on host with hid. */
static int isexecuted(gb_job_t *job, int hid)
{
	return (job->submithosts[hid]) && !(hid == job->migrhost);
}

/* getstat: Get info from subsystems to make scheduler decision. */
static int getstat(gb_job_t *job, gb_schedhosts_t **schedhosts)
{

	int i, mon_nrjobs, mon_nqjobs;
	int netmon_err_flag, dcsmon_err_flag;

	if ((*schedhosts = malloc(sizeof(**schedhosts) * Nhosts)) == NULL)
	{
		error(0, errno, "malloc() failed for *stagetime");
		return -1;
	}

	/* 
	 * Time to stage-in. 
	 */
	if (gettimetostage(job, *schedhosts) < 0)
	{
		netmon_err_flag = 1;
		error(0, 0, "gettimetostage() failed");
	}

	/*
	for (i = 0; i < Nhosts; i++)
		applog("nettime[%d] = %f", i, (*schedhosts)[i].stagetime);
	applog("");
	*/

	/*
	 * Number of jobs and resources' state.
	 */
	for (i = 0; i < Nhosts; i++)
	{
		(*schedhosts)[i].hid = i;

		if (!isexecuted(job, i))
			/* Get info only for those hosts, where job was not executed. */
		{
			netmon_err_flag = 0;
			dcsmon_err_flag = 0;

			/* Free processors. */
			if (((*schedhosts)[i].frprocs = getfreeprocs(Hosts[i].hname)) < 0)
			{
				dcsmon_err_flag = 1;
				error(0, 0, "getfreeprocs() failed for %s", Hosts[i].hname);
			}

			/* Total processors. */
			if (!dcsmon_err_flag) 
			{
				if (((*schedhosts)[i].totprocs = getnprocs(Hosts[i].hname)) < 0)
				{
					dcsmon_err_flag = 1;
					error(0, 0, "getnprocs() failed for %s", Hosts[i].hname);
				}
			}

			/* Number of jobs. */
			if (!dcsmon_err_flag) 
			{
				if (getnjobs(Hosts[i].hname, &mon_nrjobs, &mon_nqjobs) < 0)
				{
					dcsmon_err_flag = 1;
					error(0, 0, "getfreeprocs() failed for %s", Hosts[i].hname);
				}
			}


			/* Error processing */
			if (netmon_err_flag)
				(*schedhosts)[i].stagetime = 0;

			if (dcsmon_err_flag)
			{
				(*schedhosts)[i].frprocs  = 0;
				(*schedhosts)[i].totprocs = 0;
				(*schedhosts)[i].nrjobs = 0;
				(*schedhosts)[i].nqjobs = 0;
			}
			else
			{
				/* Info from dcsmon. */
				(*schedhosts)[i].nrjobs = mon_nrjobs;
				(*schedhosts)[i].nqjobs = mon_nqjobs;

				/* 
				 * Adjustments for jobs was previsously submitted to resource,
				 * but have not executed yet
				 */
				(*schedhosts)[i].nqjobs += Hosts[i].adj_jobnum;
				(*schedhosts)[i].frprocs -= Hosts[i].adj_procnum;
				if ((*schedhosts)[i].frprocs < 0)
					(*schedhosts)[i].frprocs = 0;
			}
		}
	}

	return 0;
}

/* gettimetostage: Get time to stage-in to host. */
static int gettimetostage(gb_job_t *job, gb_schedhosts_t *schedhosts)
{
	hoststage_t *hoststages = NULL;
	double *stagetimes = NULL;
	int i, j, rc;
	int n_stagein_hosts; /* Number of file source hosts. */

	if ((stagetimes = malloc(Nhosts * sizeof(*stagetimes))) == NULL)
	{
		error(0, errno, "malloc() failed for stagetimes");
		goto lbl_err;
	}

	/* Prepare */ 
	if (make_hoststages(job, &hoststages, &n_stagein_hosts) < 0)
		goto lbl_err;

	/* Initialize stagetime field. */
	for (i = 0; i < Nhosts; i++)
		schedhosts[i].stagetime = 0;

	/* Get time for staging. */
	for (i = 0; i < n_stagein_hosts; i++)
	{
		if (hoststages[i].nfiles > 0)
		{
			rc = get_nettime_for_table(hoststages[i].hname, &hoststages[i], 
									   Hosts, Nhosts, stagetimes);
			if (rc < 0)
				error(0, 0, "get_nettime_for_table() failed for host %s", 
					  hoststages[i].hname);

			/*
			for (j = 0; j < Nhosts; j++)
				applog("schedhosts[%d].stagetime = %f", 
				       i, schedhosts[j].stagetime);
			*/

			for (j = 0; j < Nhosts; j++)
				schedhosts[j].stagetime += stagetimes[j];
		}
	}

	/*
	 * Error handling.
	 */
	/* Successful */
	for (i = 0; i < n_stagein_hosts; i++)
		if (hoststages[i].file != NULL)
		{
			for (j = 0; j < hoststages[i].nfiles; j++)
				if (hoststages[i].file[j] != NULL)
					free(hoststages[i].file[j]);
			free(hoststages[i].file);
		}

	if (hoststages != NULL)
		free(hoststages);
	if (stagetimes != NULL)
		free(stagetimes);

	return 0;

	/* Error */
lbl_err:
	for (i = 0; i < n_stagein_hosts; i++)
		if (hoststages[i].file != NULL)
		{
			for (j = 0; j < hoststages[i].nfiles; j++)
				if (hoststages[i].file[j] != NULL)
					free(hoststages[i].file[j]);
			free(hoststages[i].file);
		}
	if (hoststages != NULL)
		free(hoststages);
	if (stagetimes != NULL)
		free(stagetimes);
	return -1;
}

/* parseURL: Parse URL to hostname and filename. */
static int parseURL(char *URL, char *hname, char *file)
{
	char *srchostptr, *s;
	double time = 0;

	/* Parse file URL: get src host and file path from URL source line. */
	srchostptr = URL;
	do
	{
		srchostptr = index(srchostptr, '/');
		srchostptr++;
	} while (srchostptr[0] == '/');

	s = index(srchostptr, '/');
	strncpy(hname, srchostptr, strlen(srchostptr) - strlen(s));
	sprintf(file, "%s", s);

	return time;
}

/* make_hoststages: Count up number of files, malloc memory. */
static int make_hoststages(gb_job_t *job, hoststage_t **hoststages,
                           int *n_stagein_hosts)
{
	char hname[GB_HNAME_LEN] = "\0", filename[GB_PATH_LEN] = "\0";
	struct stage_hosts_t *stage_hosts = NULL;
	int i, j, fcurr, ihost;

	/* Count up number of unique hosts. */
	*n_stagein_hosts = 0;
	if ((stage_hosts = malloc(GB_MAX_HOSTS * 
		                      sizeof(struct stage_hosts_t))) == NULL)
	{
		error(0, errno, "malloc() failed for *stage_hosts");
		goto lbl_err;
	}

	for (i = 0; i < job->nstages; i++)
	{
		if (job->stages[i].dest[0] == '\0')
		{
			parseURL(job->stages[i].src, hname, filename);

			if (findhost(hname, stage_hosts, *n_stagein_hosts) < 0)
			{
				sprintf(stage_hosts[*n_stagein_hosts].hname, "%s", hname);
				(*n_stagein_hosts)++;
			}
		}
	}

	parseURL(job->exec, hname, filename);
	if (findhost(hname, stage_hosts, *n_stagein_hosts) < 0)
	{
		sprintf(stage_hosts[*n_stagein_hosts].hname, "%s", hname);
		(*n_stagein_hosts)++;
	}

	/* Allocate memory. */ 
	if ((*hoststages = malloc((*n_stagein_hosts)* sizeof(hoststage_t))) == NULL)
	{
		error(0, errno, "malloc() failed for hoststages");
		goto lbl_err;
	}

	for (i = 0; i < *n_stagein_hosts; i++)
	{
		(*hoststages)[i].nfiles = 0;
		(*hoststages)[i].fcurr = 0;
		(*hoststages)[i].file = NULL;
		sprintf((*hoststages)[i].hname, "%s", stage_hosts[i].hname);
	}

	for (i = 0; i < job->nstages; i++)
		if (job->stages[i].dest[0] == '\0')
		{
			parseURL(job->stages[i].src, hname, filename);
		   (*hoststages)[findhost(hname,stage_hosts,*n_stagein_hosts)].nfiles++;
		}

	parseURL(job->exec, hname, filename);
	(*hoststages)[findhost(hname, stage_hosts, *n_stagein_hosts)].nfiles++;
	
	/* Allocate memory. */
	for (i = 0; i < *n_stagein_hosts; i++)
	{
		if ((*hoststages)[i].nfiles > 0)
		{
			if (((*hoststages)[i].file = malloc((*hoststages)[i].nfiles * 
				                                sizeof(char *))) == NULL)
			{
				error(0, errno, "malloc() failed for hoststage[%d].file", i);
				return -1;
			}

			for (j = 0; j < (*hoststages)[i].nfiles; j++)
				if (((*hoststages)[i].file[j] = malloc(GB_PATH_LEN)) == NULL)
				{
					error(0, errno,"malloc() failed for hoststage[%d].file[%d]",
					      i, j);
					return -1;
				}
		}
	}

	/* Fill hoststages struct. */
	for (i = 0; i < job->nstages; i++)
	{
		if (job->stages[i].dest[0] == '\0')
		{
			parseURL(job->stages[i].src, hname, filename);

			ihost = findhost(hname, stage_hosts, *n_stagein_hosts);
			fcurr = (*hoststages)[ihost].fcurr;
			sprintf((*hoststages)[ihost].file[fcurr], "%s", filename);
			(*hoststages)[ihost].fcurr++;
		}
	}

	parseURL(job->exec, hname, filename);

	ihost = findhost(hname, stage_hosts, *n_stagein_hosts);
	fcurr = (*hoststages)[ihost].fcurr;
	sprintf((*hoststages)[ihost].file[fcurr], "%s", filename);
	(*hoststages)[ihost].fcurr++;

	/* Free memory. */
	if (stage_hosts != NULL)
		free(stage_hosts);
	return 0;

lbl_err:
	if (stage_hosts != NULL)
		free(stage_hosts);
	return -1;
}

/* findhost: Return host number of hostname in stage_hosts. */
static int findhost(char *hname, struct stage_hosts_t *stage_hosts, int nhosts)
{
	int i;

	for (i = 0; i < nhosts; i++)
		if (!strcmp(stage_hosts[i].hname, hname))
			return i;
	return -1;
}


/* getnnodesppn: Get count of nodes and ppn from job rank for host. */
int getnodesppn(char *host, int rank, int *nodes, int *ppn)
{
	int allnodes, p, nnodes, inode, hostppn, i;
	procmap_t *procmap;
	char shname[GB_HNAME_LEN];

	if ((procmap = getprocmap(host, &allnodes)) == NULL)
	{
		error(0, 0, "getprocmap() failed");
		return -1;
	}
	
	getshorthname(host, shname);
	/*
	#ifdef GB_SCHEDULER_DEBUG
	applog("SCHEDULER: %s:    ", shname);
	for (i = 0; i < allnodes; i++)
		fprintf(stderr, "%3d", procmap[i]);
	#endif
	*/

	if ((hostppn = getppn(host)) < 0)
	{
		error(0, 0, "getppn() failed");
		return -1;
	}

	if (allnodes * hostppn < rank)
	{
		error(0, 0, 
      "can't execute job with rank %d on host %s with %d processors available",
	  rank, host, allnodes * hostppn);
		return -1;
	}

	/* Return if odd. */
	/*
	if ((rank % 2 == 1) && (rank != 1))
	{
		error(0, 0, "invalid job rank %d", rank);
		return -1;
	}
	*/

	qsort(procmap, allnodes, sizeof(*procmap), cmpproc);

	/* Find symmetric subsystem on free processors. */
	for (p = hostppn; p > 0; p--)
	{
		if (rank % p == 0)
		{
			nnodes = 0;

			for (inode = 0; inode < allnodes; inode++)
			{
				if (procmap[inode] >= p)
				{
					nnodes++;
					if (nnodes >= rank / p)
					{
						*nodes = nnodes;
						*ppn = p;
						/*
						#ifdef GB_SCHEDULER_DEBUG
						fprintf(stderr, "    nodes=%d:ppn=%d\n\n", *nodes,*ppn);
						#endif
						*/
						return 0;
					}
				}
			}
		}
	}

	/* 
	 * Can't allocate subsystem on free processors.
	 * So let all processors available and do the same. 
	 */
	/*
	#ifdef GB_SCHEDULER_DEBUG
	fprintf(stderr, "\nSCHEDULER: %s:    ", shname);
	for (i = 0; i < allnodes; i++)
		fprintf(stderr, "%3d", procmap[i]);
	#endif
	*/

	for (i = 0; i < allnodes; i++)
		procmap[i] = hostppn;

	for (p = hostppn; p > 0; p--)
	{
		if (rank % p == 0)
		{
			nnodes = 0;

			for (inode = 0; inode < allnodes; inode++)
			{
				if (procmap[inode] >= p)
				{
					nnodes++;
					if (nnodes >= rank / p)
					{
						*nodes = nnodes;
						*ppn = p;
						/*
						#ifdef GB_SCHEDULER_DEBUG
						fprintf(stderr, "   nodes=%d:ppn=%d\n\n", *nodes, *ppn);
						#endif
						*/
						return 0;
					}
				}
			}
		}
	}

	error(0, 0, "can't find subsystem on host %s for job with rank %d",
	      host, rank);

	return -1;
}

/* cmpint: Comparison function for qsort. */
static int cmpproc(const void *a, const void *b)
{
    return ( *(procmap_t*)a - *(procmap_t*)b );
}

/* cmphosts: Comparison function for hosts (by value of objective func). */
static int cmphosts(const void *host1, const void *host2)
{
	const gb_schedhosts_t *h1 = (const gb_schedhosts_t *) host1;
	const gb_schedhosts_t *h2 = (const gb_schedhosts_t *) host2;

	if (h1->obj < 0)
		return 1;
	else if (h2->obj < 0)
		return -1;
	else
		return (double) (h1->obj - h2->obj) * 1000;
}
