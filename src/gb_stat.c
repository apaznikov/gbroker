/*
 * gb_stat.c: Statistic collector (for modelling).
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

#include "gbroker.h"
#include "gb_launcher.h"
#include "error.h"
#include "gb_utils.h"

char Gbroker_home[GB_PATH_LEN];

char Schedtime_log[GB_PATH_LEN];
char Queuetime_log[GB_PATH_LEN];
char Servtime_log[GB_PATH_LEN];
char Freenodes_log[GB_PATH_LEN];
char Freeproc_log[GB_PATH_LEN];
char Nrunjobs_log[GB_PATH_LEN];
char Nqueuejobs_log[GB_PATH_LEN];

char Thishost[GB_HNAME_LEN];

#define _GBSTAT_ENABLE

#ifdef _GBSTAT_ENABLE

enum { TIMELEN = 20 };

/* gbstat_freenodes: Write number of free nodes into log. */
static int gbstat_freenodes();

/* gbstat_freeproc: Write number of free processors into log. */
static int gbstat_freeproc();

/* gbstat_njobs: Write number of running and queued jobs into log. */
static int gbstat_njobs();

/* gettime: Get time inf HH:MM:SS. */
static void gettime(char *stime);

/* gbstat_init: Init log files. */
static int gbstat_init();

#endif

/* gbstat: Gbroker system stat collector. */
void *gbstat(void *arg)
{
#ifdef _GBSTAT_ENABLE
	if (gbstat_init() < 0)
	{
		error(0, 0, "gbstat_init() failed");
		return NULL;
	}

	for (;;)
	{
		if (gbstat_freenodes() < 0)
		{
			error(0, 0, "gbstat_freenodes() failed");
			return NULL;
		}

		if (gbstat_freeproc() < 0)
		{
			error(0, 0, "gbstat_freeproc() failed");
			return NULL;
		}

		if (gbstat_njobs() < 0)
		{
			error(0, 0, "gbstat_njobs() failed");
			return NULL;
		}

		sleep(GB_STAT_TIMEOUT);
	}
#else
	return NULL;
#endif
}

/* gbstat_schedtime: Write scheduling time into log. */
int gbstat_schedtime(gb_job_t *job)
{
#ifdef _GBSTAT_ENABLE
	FILE *fp;
	char now[TIMELEN];

	if ((fp = fopen(Schedtime_log, "a")) == NULL)
	{
		error(0, errno, "fopen() failed for %s", Schedtime_log);
		return -1;
	}

	gettime(now);
	fprintf(fp, "%s %f\n", now, gettimepast(job->submittime));

	fclose(fp);
	return 0;
#else
	return 0;
#endif
}

/* gbstat_queuetime: Write queue waiting time into log. */
int gbstat_queuetime(gb_job_t *job)
{
#ifdef _GBSTAT_ENABLE
	FILE *fp;
	char now[TIMELEN];

	if ((fp = fopen(Queuetime_log, "a")) == NULL)
	{
		error(0, errno, "fopen() failed for %s", Queuetime_log);
		return -1;
	}

	gettime(now);
	fprintf(fp, "%s %f\n", now, gettimepast(job->submittime));

	fclose(fp);
	return 0;
#else
	return 0;
#endif
}

/* gbstat_servtime: Write job service time into log. */
int gbstat_servtime(gb_job_t *job)
{
#ifdef _GBSTAT_ENABLE
	FILE *fp;
	char now[TIMELEN];

	if ((fp = fopen(Servtime_log, "a")) == NULL)
	{
		error(0, errno, "fopen() failed for %s", Servtime_log);
		return -1;
	}

	gettime(now);
	fprintf(fp, "%s %f\n", now, gettimepast(job->submittime));

	fclose(fp);
	return 0;
#else
	return 0;
#endif
}

#ifdef _GBSTAT_ENABLE

/* gbstat_freenodes: Write number of free nodes into log. */
static int gbstat_freenodes()
{
	FILE *fp;
	int freenodes;
	char now[TIMELEN];

	if ((fp = fopen(Freenodes_log, "a")) == NULL)
	{
		error(0, errno, "fopen() failed for %s", Freenodes_log);
		return -1;
	}

	if ((freenodes = getfreenodes(Thishost)) < 0)
	{
		error(0, 0, "getfreenodes() failed for %s", Thishost);
		return -1;
	}

	gettime(now);
	fprintf(fp, "%s %d\n", now, freenodes);

	fclose(fp);
	return 0;
}

/* gbstat_freeproc: Write number of free processors into log. */
static int gbstat_freeproc()
{
	FILE *fp;
	int freeprocs;
	char now[TIMELEN];

	if ((fp = fopen(Freeproc_log, "a")) == NULL)
	{
		error(0, errno, "fopen() failed for %s", Freeproc_log);
		return -1;
	}

	if ((freeprocs = getfreeprocs(Thishost)) < 0)
	{
		error(0, 0, "getfreeprocs() failed for %s", Thishost);
		return -1;
	}

	gettime(now);
	fprintf(fp, "%s %d\n", now, freeprocs);

	fclose(fp);
	return 0;
}

/* gbstat_njobs: Write number of running and queued jobs into log. */
static int gbstat_njobs()
{
	FILE *fp;
	char now[TIMELEN];
	int nrunjobs, nqueuejobs;

	if (getnjobs(Thishost, &nrunjobs, &nqueuejobs) < 0)
	{
		error(0, 0, "getnjobs() failed for %s", Thishost);
		return -1;
	}

	gettime(now);

	if ((fp = fopen(Nrunjobs_log, "a")) == NULL)
	{
		error(0, errno, "fopen() failed for %s", Nrunjobs_log);
		return -1;
	}

	fprintf(fp, "%s %d\n", now, nrunjobs);

	fclose(fp);

	if ((fp = fopen(Nqueuejobs_log, "a")) == NULL)
	{
		error(0, errno, "fopen() failed for %s", Nqueuejobs_log);
		return -1;
	}

	fprintf(fp, "%s %d\n", now, nqueuejobs);

	fclose(fp);
	return 0;
}

/* gettime: Get time inf HH:MM:SS. */
static void gettime(char *stime)
{
	struct tm *t;
	time_t rawtime;
	char smday[3], shour[3], smin[3], ssec[3];

	rawtime = time(NULL);
	t = localtime(&rawtime);

	sprintf(smday, "%d", t->tm_mday);
	sprintf(shour, "%d", t->tm_hour);

	if (t->tm_min < 10)
		sprintf(smin, "0%d", t->tm_min);
	else
		sprintf(smin, "%d", t->tm_min);

	if (t->tm_sec < 10)
		sprintf(ssec, "0%d", t->tm_sec);
	else
		sprintf(ssec, "%d", t->tm_sec);

	sprintf(stime, "%s %s:%s:%s", smday, shour, smin, ssec);
}

/* gbstat_init: Init log files. */
static int gbstat_init()
{
	FILE *fp;
	char dir[GB_PATH_LEN];
	int fd;

	sprintf(dir, "%s/var/stat", Gbroker_home);

	if ((fd = open(dir, O_EXCL)) < 0)
		if (mkdir(dir, 0755) < 0)
			error(0, errno, "mkdir() failed for %s", dir);
	close(fd);

	sprintf(Schedtime_log, "%s/var/stat/schedtime.log", Gbroker_home);

	if ((fp = fopen(Schedtime_log, "w")) == NULL)
	{
		error(0, errno, "fopen() failed for %s", Schedtime_log);
		return -1;
	}

	fclose(fp);

	sprintf(Queuetime_log, "%s/var/stat/queuetime.log", Gbroker_home);

	if ((fp = fopen(Queuetime_log, "w")) == NULL)
	{
		error(0, errno, "fopen() failed for %s", Queuetime_log);
		return -1;
	}

	fclose(fp);

	sprintf(Servtime_log, "%s/var/stat/servtime.log", Gbroker_home);

	if ((fp = fopen(Servtime_log, "w")) == NULL)
	{
		error(0, errno, "fopen() failed for %s", Servtime_log);
		return -1;
	}

	fclose(fp);

	sprintf(Freeproc_log, "%s/var/stat/freeproc.log", Gbroker_home);

	if ((fp = fopen(Freeproc_log, "w")) == NULL)
	{
		error(0, errno, "fopen() failed for %s", Freeproc_log);
		return -1;
	}

	fclose(fp);

	sprintf(Freenodes_log, "%s/var/stat/freenodes.log", Gbroker_home);

	if ((fp = fopen(Freenodes_log, "w")) == NULL)
	{
		error(0, errno, "fopen() failed for %s", Freenodes_log);
		return -1;
	}

	fclose(fp);

	sprintf(Nrunjobs_log, "%s/var/stat/nrunjobs.log", Gbroker_home);

	if ((fp = fopen(Nrunjobs_log, "w")) == NULL)
	{
		error(0, errno, "fopen() failed for %s", Nrunjobs_log);
		return -1;
	}

	fclose(fp);

	sprintf(Nqueuejobs_log, "%s/var/stat/nqueuejobs.log", Gbroker_home);

	if ((fp = fopen(Nqueuejobs_log, "w")) == NULL)
	{
		error(0, errno, "fopen() failed for %s", Nqueuejobs_log);
		return -1;
	}

	fclose(fp);

	return 0;
}

#endif
