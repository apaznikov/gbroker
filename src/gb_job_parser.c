/*
 * gb_job_parser.c: Parse JSDL-file
 *
 * (c) 2008-2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <libxml/parser.h>
#include <libxml/xmlmemory.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#include "gbroker.h"
#include "gb_job_parser.h"
#include "error.h"

char Gbroker_home[GB_PATH_LEN];
char Thishost[GB_HNAME_LEN];

/* parseApplication: Parse <Application> </Application>. */
void parseApplication(xmlDocPtr doc, xmlNodePtr cur, gb_job_t *job);

/* parseResources: Parse <Resources> </Resources>. */
void parseResources(xmlDocPtr doc, xmlNodePtr cur, gb_job_t *job);

/* parseDataStaging: <DataStaging> </DataStaging>. */
void parseDataStaging(xmlDocPtr doc, xmlNodePtr cur, gb_stage_t *stage);

/* printjob: */
void printjob(gb_job_t *job);

void printjob(gb_job_t *job)
{
	int i;

	printf("name = %s\n", job->name);
	printf("exec = %s\n", job->exec);
	printf("args = %s\n", job->args);
	printf("stdout = %s\n", job->stout);
	printf("stderr = %s\n", job->sterr);
	printf("rank = %d\n", job->rank);
	printf("wallt = %d\n\n", job->wallt);

	for (i = 0; i < job->nstages; i++)
	{
		printf("stage[%d].name = %s\n", i, job->stages[i].name);
		printf("stage[%d].src = %s\n", i, job->stages[i].src);
		printf("stage[%d].dest = %s\n\n", i, job->stages[i].dest);
	}
}

/* parsejsdl: Read job description. */
int parsejsdl(char *jobfile, gb_job_t *job)
{	
	xmlDocPtr doc;
	xmlNodePtr cur;

	doc = xmlParseFile(jobfile);

	job->stages = NULL;
	job->args[0] = '\0';

	if (doc == NULL)
	{
		error(0, 0, "xmlParseFile() failed");
		return -1;
	}

	cur = xmlDocGetRootElement(doc);

	if (cur == NULL)
	{
		error(0, 0, "xmlDocGetRootElement() failed");
		xmlFreeDoc(doc);
		return -1;
	}

	if (xmlStrcmp(cur->name, (const xmlChar *) "JobDefinition"))
	{
		error(0, 0, "wrong job description");
		xmlFreeDoc(doc);
		return -1;
	}

	for (cur = cur->xmlChildrenNode; cur->next != NULL; cur = cur->next)
	{
		if (!xmlStrcmp(cur->name, (const xmlChar *) "JobDescription"))
		{
			int istage = 0;

			job->nstages = 0;
			for (cur = cur->xmlChildrenNode; cur->next != NULL; cur = cur->next)
			{
				if (!xmlStrcmp(cur->name, (const xmlChar *) "Application"))
					parseApplication(doc, cur, job);
				else if (!xmlStrcmp(cur->name, (const xmlChar *) "Resources"))
					parseResources(doc, cur, job);
				else if (!xmlStrcmp(cur->name, (const xmlChar *) "DataStaging"))
					job->nstages++;
			}
			
			cur = cur->parent;

			if (job->nstages > 0)
			{
				if ((job->stages = (gb_stage_t *) malloc(sizeof(gb_stage_t) * 
					                                     job->nstages)) == NULL)
				{
					error(0, errno, "malloc() failed");
					return -1;
				}

				for (cur = cur->xmlChildrenNode; cur->next != NULL; 
				     cur = cur->next)
				{
					if (!xmlStrcmp(cur->name, (const xmlChar *) "DataStaging"))
					{
						parseDataStaging(doc, cur, &job->stages[istage]);
						istage++;
					}
				}

				cur = cur->parent;
			}
		}
	}
	/* print_job_descr(job); */

	xmlFreeDoc(doc);
	xmlCleanupParser();

	if (strlen(job->exec) == 0)
		return -1;

	/* printjob(*job); */

	return 0;
}

/* freejob: Free memory for job structure. */
void freejob(gb_job_t *job)
{
	if (job->stages != NULL)
		free(job->stages);
}

/* parseApplication: Parse <Application> </Application>. */
void parseApplication(xmlDocPtr doc, xmlNodePtr cur, gb_job_t *job)
{
	xmlChar *key;

	for (cur = cur->xmlChildrenNode; cur->next != NULL; cur = cur->next)
	{
		if (!xmlStrcmp(cur->name, (const xmlChar *) "ApplicationName"))
		{
			key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			sprintf(job->name, "%s", key);
			xmlFree(key);
		}
		else if (!xmlStrcmp(cur->name, (const xmlChar *) "POSIXApplication"))
		{
			for (cur = cur->xmlChildrenNode; cur->next != NULL;
			     cur = cur->next)
			{
				if (!xmlStrcmp(cur->name, (const xmlChar *) "Executable"))
				{
					key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
					sprintf(job->exec, "%s", key);
					xmlFree(key);
				}
				else if (!xmlStrcmp(cur->name, (const xmlChar *) "Argument"))
				{
					key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
					sprintf(job->args, "%s", key);
					xmlFree(key);

				}
				else if (!xmlStrcmp(cur->name, (const xmlChar *) "Output"))
				{
					key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
					sprintf(job->stout, "%s", key);
					xmlFree(key);
				}
				else if (!xmlStrcmp(cur->name, (const xmlChar *) "Error"))
				{
					key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
					sprintf(job->sterr, "%s", key);
					xmlFree(key);
				}
			}

			cur = cur->parent;
		}
	}
}

/* parseResources: Parse <Resources> </Resources>. */
void parseResources(xmlDocPtr doc, xmlNodePtr cur, gb_job_t *job)
{
	xmlChar *key;

	for (cur = cur->xmlChildrenNode; cur->next != NULL; cur = cur->next)
	{
		if (!xmlStrcmp(cur->name, (const xmlChar *) "TotalCPUCount"))
		{
			for (cur = cur->xmlChildrenNode; cur->next != NULL; cur = cur->next)
			{
				if (!xmlStrcmp(cur->name, (const xmlChar *) "Exact"))
				{
					key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
					job->rank = atoi((char *) key);
					xmlFree(key);
				}
			}

			cur = cur->parent;
		}

		else if (!xmlStrcmp(cur->name, (const xmlChar *) "TotalCPUTime"))
		{
			for (cur = cur->xmlChildrenNode; cur->next != NULL; cur = cur->next)
			{
				if (!xmlStrcmp(cur->name, (const xmlChar *) "UpperBoundedRange"))
				{
					key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
					job->wallt = atof((char *) key);
					xmlFree(key);
				}
			}

			cur = cur->parent;
		}
	}
}

/* parseDataStaging: <DataStaging> </DataStaging>. */
void parseDataStaging(xmlDocPtr doc, xmlNodePtr cur, gb_stage_t *stage)
{
	xmlChar *key;

	stage->src[0] = '\0';
	stage->dest[0] = '\0';

	for (cur = cur->xmlChildrenNode; cur->next != NULL; cur = cur->next)
	{
		if (!xmlStrcmp(cur->name, (const xmlChar *) "FileName"))
		{
			key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			sprintf(stage->name, "%s", key);
			xmlFree(key);
		}
		else if (!xmlStrcmp(cur->name, (const xmlChar *) "Source"))
		{
			for (cur = cur->xmlChildrenNode; cur->next != NULL; cur = cur->next)
			{
				if (!xmlStrcmp(cur->name, (const xmlChar *) "URI"))
				{
					key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
					sprintf(stage->src, "%s", key);
					xmlFree(key);
				}
			}

			cur = cur->parent;
		}
		else if (!xmlStrcmp(cur->name, (const xmlChar *) "Target"))
		{
			for (cur = cur->xmlChildrenNode; cur->next != NULL; cur = cur->next)
			{
				if (!xmlStrcmp(cur->name, (const xmlChar *) "URI"))
				{
					key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
					sprintf(stage->dest, "%s", key);
					xmlFree(key);
				}
			}

			cur = cur->parent;
		}
	}
}

