/*
 * gb_job_list.c: Job list functions.
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include <string.h>

#include "gb_job_list.h"
#include "gb_job_monitor.h"
#include "gb_job.h"
#include "gb_utils.h"
#include "error.h"
#include "gbps.h"

gb_joblist_t *Rjobs_head = NULL, *Rjobs_tail = NULL;  /* Running jobs. */
gb_joblist_t *Cjobs_head = NULL, *Cjobs_tail = NULL;  /* Completed jobs. */

pthread_mutex_t Rjobs_mutex;
pthread_mutex_t Cjobs_mutex;

int Nrunned, Ncompl;

/* gbps functions - begin */

/* gbpsprintrow: Print one line in ps table to psbuf. */
static void gbpsprintrow(gb_ps_t *psrow, char *psbuf);

/* getpstimepast: Get time past from 'past' to this moment in HH:MM:SS. */
static void getpstimepast(struct timeval tv, char *s);

/* cmppsrows: Comparison function for ps rows. */
static int cmppsrows(const void *row1, const void *row2);

/* gbps functions - end */

/* findinlist: Find job in list, return pointer to job. */
static gb_job_t *findinlist(gb_joblist_t **joblist_head, int id);

/* addjobtolist: Add job to job list. */
static int addjobtolist(gb_job_t *job, gb_joblist_t **joblist_head, 
                                       gb_joblist_t **joblist_tail);

/* printjoblist: Print all jobs being in job list. */
static void printjoblist(gb_joblist_t **joblist_head);

/* clearjoblist: Remove all jobs from job list. */
static void clearjoblist(gb_joblist_t **joblist_head);

/* joblist_getps: Write job list to buffer. */
int joblist_getps(char *psbuf)
{
	gb_joblist_iter_t iter;
	gb_job_t *job;
	int i, pscntr, njobs, submitflag;
	gb_ps_t *gbps_list;

	/* Lock running job list mutex. */
	pthread_mutex_lock(&Rjobs_mutex);

	njobs = Nrunned;
	if ((gbps_list = (gb_ps_t *) malloc((njobs) * sizeof(gb_ps_t))) == NULL)
	{
		error(0, errno, "malloc() failed for gbps_list");
		/* Unlock running job list mutex. */
		pthread_mutex_unlock(&Rjobs_mutex);
		return -1;
	}

	psbuf[0] = '\0';
	pscntr = 0;

	for (runjoblist_iter_init(&iter); !joblist_iter_isend(&iter);
	     joblist_iter_getnext(&iter))
	{
		job = joblist_iter_getjob(&iter);

		submitflag = 0;
		for (i = 0; i < Nhosts; i++)
		{
			if (job->submithosts[i] != NULL)
				/* Submit host is found. */
			{
				sprintf(gbps_list[pscntr].state, "%s", job->status[i]);
				
				if (!submitflag)
				{
					sprintf(gbps_list[pscntr].host, "%s", 
					        job->submithosts[i]->hname);
					submitflag = 1;
				}
				else
					sprintf(gbps_list[pscntr].host, "%s, %s", 
					        gbps_list[pscntr].host, job->submithosts[i]->hname);
			}
		}

		gbps_list[pscntr].id = job->id;
		sprintf(gbps_list[pscntr].name, job->name);
		getpstimepast(job->submittime, gbps_list[pscntr].tottime);

		if (!is_active(gbps_list[pscntr].state))
			sprintf(gbps_list[pscntr].exectime, "--");
		else
			getpstimepast(job->exectime, gbps_list[pscntr].exectime);

		pscntr++;
	}

	/* Unlock running job list mutex. */
	pthread_mutex_unlock(&Rjobs_mutex);

	qsort(gbps_list, pscntr, sizeof(gb_ps_t), cmppsrows);

	for (i = 0; i < pscntr; i++)
		gbpsprintrow(&gbps_list[i], psbuf);

	free(gbps_list);

	return 0;
}

/* gbpsprintrow: Print one line in ps table to psbuf. */
static void gbpsprintrow(gb_ps_t *psrow, char *psbuf)
{
	char id[GB_PSFIELD_LEN];
	int i;

	sprintf(id, "%d", psrow->id);
	strncat(psbuf, id, GB_PS_ID_FIELD);
	for (i = 0; i < GB_PS_ID_FIELD - strlen(id); i++)
		strcat(psbuf, " ");
	strcat(psbuf, " ");

	strncat(psbuf, psrow->name, GB_PS_NAME_FIELD);
	for (i = 0; i < GB_PS_NAME_FIELD - strlen(psrow->name); i++)
		strcat(psbuf, " ");
	strcat(psbuf, " ");

	strncat(psbuf, psrow->tottime, GB_PS_TOTTIME_FIELD);
	for (i = 0; i < GB_PS_TOTTIME_FIELD - strlen(psrow->tottime); i++)
		strcat(psbuf, " ");
	strcat(psbuf, " ");

	strncat(psbuf, psrow->exectime, GB_PS_EXECTIME_FIELD);
	for (i = 0; i < GB_PS_EXECTIME_FIELD - strlen(psrow->exectime); i++)
		strcat(psbuf, " ");
	strcat(psbuf, " ");

	strncat(psbuf, psrow->state, GB_PS_STATE_FIELD);
	for (i = 0; i < GB_PS_STATE_FIELD - strlen(psrow->state); i++)
		strcat(psbuf, " ");
	strcat(psbuf, " ");

	strcat(psbuf, psrow->host);
	strcat(psbuf, "\n");
}

/* getpstimepast: Get time past from 'past' to this moment in HH:MM:SS. */
static void getpstimepast(struct timeval tv, char *s)
{
	int timediff, hh, mm, ss;
	
	timediff = gettimepast(tv);
	ss = timediff % 60;
	mm = (timediff / 60) % 3600;
	hh = timediff / 3600;
	sprintf(s, "%d:", hh);
	if (mm < 10)
		sprintf(s, "%s0%d:", s, mm);
	else
		sprintf(s, "%s%d:", s, mm);
	if (ss < 10)
		sprintf(s, "%s0%d", s, ss);
	else
		sprintf(s, "%s%d", s, ss);
}

/* cmppsrows: Comparison function for ps rows. */
static int cmppsrows(const void *row1, const void *row2)
{
	const gb_ps_t *r1 = (const gb_ps_t *) row1;
	const gb_ps_t *r2 = (const gb_ps_t *) row2;

	return r1->id - r2->id;
}

/* joblist_iter_isend: Test if end of list is reached. */
int joblist_iter_isend(gb_joblist_iter_t *iter)
{
	return iter->cur == NULL;
}

/* runjoblist_iter_init: Init running job list iterator. */
void runjoblist_iter_init(gb_joblist_iter_t *iter)
{
	iter->prev = Rjobs_head;

	if (Rjobs_head == NULL)
		iter->cur = NULL;
	else
		iter->cur = iter->prev->next;
}

/* joblist_iter_getnext: Get next job in joblist. */
void joblist_iter_getnext(gb_joblist_iter_t *iter)
{
	iter->prev = iter->cur;
	iter->cur = iter->cur->next;
}

/* joblist_iter_getjob: Get job by iterator. */
gb_job_t *joblist_iter_getjob(gb_joblist_iter_t *iter)
{
	return iter->cur->job;
}

/* getjobstate: Return job state of the job. */
void getjobstate(int id, gb_job_t **job, gb_jobstate_t *jobstate)
{
	if ((*job = findinlist(&Rjobs_head, id)) != NULL)
		*jobstate = GB_JOBRUNNING_ST;
	else if ((*job = findinlist(&Cjobs_head, id)) != NULL)
		*jobstate = GB_JOBCOMPLETE_ST;
	else
		*jobstate = GB_UNKNOWN_ST;
}

/* addcompljob: Add job to job list of completed jobs. */
int addcompljob(gb_job_t *job)
{
	int rc;

	/* Add job to list of complete jobs. */
	pthread_mutex_lock(&Cjobs_mutex);

	rc = addjobtolist(job, &Cjobs_head, &Cjobs_tail);
	Ncompl++;

	pthread_mutex_unlock(&Cjobs_mutex);

	return rc;
}

/* addrunjob: Add job to job list of running jobs. */
int addrunjob(gb_job_t *job)
{
	int rc;

	/* Add job to list of running jobs. */
	pthread_mutex_lock(&Rjobs_mutex);

	rc = addjobtolist(job, &Rjobs_head, &Rjobs_tail);

	pthread_mutex_unlock(&Rjobs_mutex);

	/* Signal job monitor about job. */
	jobmon_addjob();

	return rc;
}

/* delrunjob: */
void delrunjob(gb_joblist_iter_t *iter)
{
	pthread_mutex_lock(&Rjobs_mutex);

	if (iter->cur->next == NULL)
		/* This is tail. */
	{
		iter->prev->next = NULL;
		Rjobs_tail = iter->prev;
	}
	else
		iter->prev->next = iter->cur->next;

	free(iter->cur); 
	iter->cur = iter->prev;

	Nrunned--;

	if (Nrunned == 0)
		Rjobs_head = NULL;

	pthread_mutex_unlock(&Rjobs_mutex);
}

/* printcompljobs: Print all completed jobs. */
void printcompljobs(void)
{
	pthread_mutex_lock(&Cjobs_mutex);

	fprintf(stderr, "Completed jobs:\t");
	printjoblist(&Cjobs_head);

	pthread_mutex_unlock(&Cjobs_mutex);
}

/* printrunjobs: Print all running jobs. */
void printrunjobs(void)
{
	pthread_mutex_lock(&Rjobs_mutex);

	fprintf(stderr, "Running jobs:\t");
	printjoblist(&Rjobs_head);

	pthread_mutex_unlock(&Rjobs_mutex);
}

/* clearcompljobs: Remove all completed jobs. */
void clearcompljobs(void)
{
	pthread_mutex_lock(&Cjobs_mutex);

	clearjoblist(&Cjobs_head);

	pthread_mutex_unlock(&Cjobs_mutex);
}

/* clearrunjobs: Remove all running jobs. */
void clearrunjobs(void)
{
	pthread_mutex_lock(&Rjobs_mutex);

	clearjoblist(&Rjobs_head);

	pthread_mutex_unlock(&Rjobs_mutex);
}

/* findinlist: Find job in list, return pointer to job. */
static gb_job_t *findinlist(gb_joblist_t **joblist_head, int id)
{
	gb_joblist_t *cur;

	if (*joblist_head == NULL)
		return NULL;

	cur = (*joblist_head)->next;
	while (cur != NULL)
	{
		if (cur->job->id == id)
			return cur->job;

		cur = cur->next;
	}
	
	return NULL;
}

/* printjoblist: Print all jobs being in job list. */
static void printjoblist(gb_joblist_t **joblist_head)
{
	gb_joblist_t *cur;

	if (*joblist_head == NULL)
	{
		fprintf(stderr, "\n");
		return;
	}

	cur = (*joblist_head)->next;
	while (cur != NULL)
	{
		fprintf(stderr, "%4d", cur->job->id);
		cur = cur->next;
	}
	fprintf(stderr, "\n");
}

/* joblist_init: Init job list. */
void joblist_init(void)
{
	pthread_mutexattr_t attr;

	pthread_mutexattr_init(&attr);
	pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE_NP);
	pthread_mutex_init(&Rjobs_mutex, &attr);
	pthread_mutex_init(&Cjobs_mutex, &attr);
	pthread_mutexattr_destroy(&attr);
}

/* addjobtolist: Add job to job list. */
static int addjobtolist(gb_job_t *job, gb_joblist_t **joblist_head, 
                                       gb_joblist_t **joblist_tail)
{
	gb_joblist_t *new;

	if ((new = (gb_joblist_t *) malloc(sizeof(gb_joblist_t))) == NULL)
	{
		error(0, errno, "malloc() failed for new item in job list");
		return -1;
	}
	new->next = NULL;
	new->job = job;
	
	if (*joblist_head == NULL)
	{
		if ((*joblist_head = (gb_joblist_t *) 
			                  malloc(sizeof(gb_joblist_t))) == NULL)
		{
			error(0, errno, "malloc() failed for joblist_head");
			return -1;
		}

		(*joblist_head)->next = new;
		*joblist_tail = new;
		/*
		(*joblist_tail)->next = NULL;
		*/
	}
	else
	{
		/*
		fprintf(stderr, "ADD to end %d\n", (*joblist_tail)->job->id);
		*/

		(*joblist_tail)->next = new;
		(*joblist_tail) = new;
		/*
		(*joblist_tail)->next = NULL;
		*/
	}

	return 0;
}

/* clearjoblist: Remove all jobs from job list. */
static void clearjoblist(gb_joblist_t **joblist_head)
{
	gb_joblist_t *cur;

	if (*joblist_head == NULL)
		return;

	cur = (*joblist_head)->next;
	while (cur != NULL)
	{
		jobfree(cur->job);
		free(cur);
		cur = cur->next;
	}
	(*joblist_head)->next = NULL;
}

