/*
 * gbroker.c: Main gbroker module.
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <pthread.h>
#include <string.h>
#include <sys/time.h>

#include "gbroker.h"
#include "tcp.h"
#include "error.h"
#include "daemon.h"
#include "gb_sched.h"
#include "gb_launcher.h"
#include "gb_dispatch.h"
#include "gb_job_list.h"
#include "gb_job_parser.h"
#include "gb_job_monitor.h"
#include "gb_config_parser.h"
#include "gb_stat.h"
#include "gb_host.h"

gb_host_t *Hosts;
char       Thishost[GB_HNAME_LEN];
int        Nhosts;
char       Lockfile[BUFLEN];

char       Gbroker_home[GB_PATH_LEN];

pthread_mutex_t Jobsubmit_mutex;

/* listener: Listen port, serve clients. */
static void listener(void);

/* server: Recv job from client, submit. */
static void *server(void *arg);

/* serv_jobsubmit: Process job submit. */
static void serv_jobsubmit(SOCKET sock);

/* serv_output: Get job output. */
static int serv_output(SOCKET sock);

/* serv_ps: Get job list. */
static int serv_ps(SOCKET sock);

/* senderror: Send error message to client. */
static void senderror(SOCKET sock, char *err);

/* checkusage: Check command line usage. */
static void checkusage(void);

/* cleanup: Clean up var/ dir in gbroker dir. */
void cleanup(void);

/* memfree: Free memory of global variables. */
static void memfree(void);

/* set_sighandlers: Set signal handlers. */
static void set_sighandlers(void);

/* set_thithost: Set Thishost variable (hostname). */
static void set_thishost(void);

/* sigterm: Handler for the signal SIGTERM. */
static void sigterm_handler(int signo);

/* sigint_handler: Handler for the signal SIGINT. */
static void sigint_handler(int signo);

/* common_exit: At exit function. */
static void common_exit(void);

int main(int argc, char **argv)
{
	checkusage();
	cleanup();
	Hosts = readcfg(Gbroker_home, &Nhosts);
	initlog(Gbroker_home, GBROKER_LOGNAME);

	#ifndef GB_DEBUG
	daemonize(Gbroker_home);
	#endif

	set_sighandlers();
	set_thishost();
	if (atexit(common_exit) != 0)
		error(1, errno, "atexit() failed");

	dispatch_init();
	joblist_init();
	hosts_init();

	/* Start main listener thread. */
	listener();

	return 0;
}

/* listener: Listen port, serve clients. */
static void listener(void)
{
	SOCKET sserver, sclient;
	struct sockaddr_in peer;
	socklen_t peerlen = sizeof(peer);
	server_args_t server_args;
	pthread_t server_tid, jobmon_tid, gbstat_tid;
	pthread_attr_t attr;

	if (pthread_create(&jobmon_tid, NULL, jobmon, NULL) != 0) 
		error(1, errno, "pthread_create() failed for jobmon");

	if (pthread_create(&gbstat_tid, NULL, gbstat, NULL) != 0) 
		error(1, errno, "pthread_create() failed for gbstat");

	sserver = tcp_server(Thishost, GBROKER_PORT);

	pthread_mutex_init(&Jobsubmit_mutex, NULL);

	for (;;)
	{
		if ((sclient = accept(sserver, (struct sockaddr *) &peer, 
		                      (socklen_t *) &peerlen)) < 0)
			error(1, errno, "accept() failed");

		server_args.sock = sclient;

		pthread_attr_init(&attr);
		pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

		if (pthread_create(&server_tid, &attr, server, &server_args) != 0) 
			error(1, errno, "pthread_create() failed for server");
	}
}

/* server: Recv job from client, submit. */
static void *server(void *arg)
{
	server_args_t *server_args = (server_args_t *) arg;
	SOCKET sock = server_args->sock;
	char servmsg[GB_SRVMSG_LEN];

	if (recvn(sock, servmsg, GB_SRVMSG_LEN) != GB_SRVMSG_LEN)
	{
		error(0, errno, "recvn() failed for servmsg");
		close(sock);
		return NULL;
	}

	if (!strncmp(servmsg, GB_JOBSUBMIT_RQST, GB_SRVMSG_LEN))
		serv_jobsubmit(sock);
	else if (!strncmp(servmsg, GB_GETOUTPUT_RQST, GB_SRVMSG_LEN))
	{
		if (serv_output(sock) < 0)
			error(0, 0, "serv_output() failed");
	}
	else if (!strncmp(servmsg, GB_PS_RQST, GB_SRVMSG_LEN))
	{
		if (serv_ps(sock) < 0)
			error(0, 0, "serv_ps() failed");
	}

	close(sock);

	return NULL;
}

/* serv_jobsubmit: Process job submit. */
static void serv_jobsubmit(SOCKET sock)
{
	gb_job_t *job;
	gb_schedhosts_t *schedhosts;
	int hid, i;
	struct timeval submittime;

	/*
	 * 1. Recv job.
	 * 2. Set global job id.
	 * 3. Make schedule decision.
	 * 4. Submit job to N best host.
	 * 5. If required, send to client output after job finished.
	 */

	gettimeofday(&submittime, NULL);

	if ((job = (gb_job_t *) malloc(sizeof(gb_job_t))) == NULL)
	{
		error(0, 0, "malloc() failed");
		close(sock);
		return;
	}

	if (recvjob(sock, job) < 0)
	{
		error(0, 0, "recvjob() failed");
		close(sock);
		return;
	}

	memcpy(&job->submittime, &submittime, sizeof(submittime));

	job->migrhost = -1;
	if (dosched(job, &schedhosts) < 0)
	{
		error(0, 0, "dosched() failed");
		senderror(sock, GB_JOBSUBMIT_ERROR);
		close(sock);
		return;
	}

	/* Statistics */
	gbstat_schedtime(job);

	if (schedhosts[0].obj < 0)
	{
		error(0, 0, "No suitable subsystem to execute job %s", job->name);
		senderror(sock, GB_JOBSUBMIT_ERROR);
		close(sock);
		return;
	}

	/*
	hostsort[0].hid = 2;
	hostsort[1].hid = 1;
	*/

	for (i = 0; i < GB_NSUBMITS; i++)
	{
		if ((i == Nhosts) || (schedhosts[i].obj < 0))
			break;

		hid = schedhosts[i].hid;

		if (submittohost(job, &Hosts[hid], &schedhosts[i]) < 0)
		{
			error(0, 0, "submittohost() failed for host %s", 
				  Hosts[hid].hname);
			/* senderror(sock, GB_JOBSUBMIT_ERROR); */
			free(schedhosts);
			close(sock);
			return;
		}

		applog("Job %d (%s) was submitted to %s", 
		       job->id, job->name, Hosts[hid].hname);
	}

	free(schedhosts);

	if (addrunjob(job) < 0)
	{
		error(0, 0, "addrunjob() failed");
		close(sock);
		return;
	}

	if (job->outputflag)
	{
		/* Wait until job will be computed. */
		sem_wait(&job->complete_sem);

		if (sendoutput(sock, job) < 0)
		{
			error(0, 0, "sendoutput() failed");
			close(sock);
			return;
		}
	}

	close(sock);
}

/* serv_output: Get job output. */
static int serv_output(SOCKET sock)
{
	int id;
	gb_job_t *job;
	gb_jobstate_t jobstate;

	if (recvn(sock, (char *) &id, sizeof(id)) != sizeof(id))
	{
		error(0, errno, "recvn() failed for %d", id);
		return -1;
	}

	getjobstate(id, &job, &jobstate);

	if (sendn(sock, (char *) &jobstate, sizeof(jobstate)) != sizeof(jobstate))
	{
		error(0, errno, "sendn() failed for GB_JOBSUBMIT_RUNNING");
		return -1;
	}

	if (jobstate == GB_JOBCOMPLETE_ST)
	{

		if (sendn(sock, job->stout, GB_JOBDESCR_LEN) != GB_JOBDESCR_LEN)
		{
			error(0, errno, "sendn() failed for %s", job->stout);
			return -1;
		}

		if (sendoutput(sock, job) < 0)
		{
			error(0, 0, "sendoutput() failed");
			return -1;
		}
	}

	return 0;
}

/* serv_ps: Get job list. */
static int serv_ps(SOCKET sock)
{
	char *psbuf;
	int buflen;

	if ((psbuf = malloc(GB_PSBUF_LEN)) == NULL)
	{
		error(0, errno, "malloc() failed for psbuf");
		return -1;
	}

	if (joblist_getps(psbuf) < 0)
	{
		error(0, errno, "joblist_getps() failed");
		free(psbuf);
		return -1;
	}

	buflen = strlen(psbuf);

	if (sendn(sock, (char *) &buflen, sizeof(buflen)) != sizeof(buflen))
	{
		error(0, errno, "sendn() failed for buflen");
		free(psbuf);
		return -1;
	}

	if (sendn(sock, psbuf, buflen) != buflen)
	{
		error(0, errno, "sendn() failed for psbuf");
		free(psbuf);
		return -1;
	}

	free(psbuf);
	return 0;
}

/* senderror: Send error message to client. */
static void senderror(SOCKET sock, char *err)
{
	if (sendn(sock, err, GB_SRVMSG_LEN) != GB_SRVMSG_LEN)
		error(0, errno, "senderror() failed");
}

/* checkusage: Check command line usage. */
static void checkusage(void)
{
	char *gbroker_home_tmp;

	if ((gbroker_home_tmp = getenv("GBROKER_HOME")) == NULL) 
	{
		fprintf(stderr, "Environment variable GBROKER_HOME must be set up\n");
		exit(EXIT_FAILURE);
	}

	sprintf(Gbroker_home, gbroker_home_tmp);
}

/* cleanup: Clean up var/ dir in gbroker dir. */
void cleanup(void)
{
	char cmd[GB_CMD_LEN];

	sprintf(cmd, "rm -rf %s/var/*", Gbroker_home);
	system(cmd);
}

/* memfree: Free memory of global variables. */
static void memfree(void)
{
	clearrunjobs();
	clearcompljobs();
	return;
}

/* set_sighandlers: Set signal handlers. */
static void set_sighandlers(void)
{
	struct sigaction sa;

	sigset_t mask;

	sigfillset(&mask);
	sigdelset(&mask, SIGINT);
	sigdelset(&mask, SIGTERM);
	sigprocmask(SIG_SETMASK, &mask, NULL);

	sa.sa_handler = sigint_handler;
	sigaction(SIGINT, &sa, NULL);
	sa.sa_handler = sigterm_handler;
	sigaction(SIGTERM, &sa, NULL);
}

/* set_thithost: Set Thishost variable (hostname). */
static void set_thishost(void)
{
	FILE *fp;
	char hostnamefile[GB_PATH_LEN];

	sprintf(hostnamefile, "%s/etc/hostname", Gbroker_home);

	if ((fp = fopen(hostnamefile, "r")) != NULL) 
	{
		fscanf(fp, "%s", Thishost);
		fclose(fp);

		if (strlen(Thishost) > 0)
			return;
	}

	if (gethostname(Thishost, GB_HNAME_LEN) < 0)
		error(1, errno, "gethostnamej() failed");
}

/* sigterm: Handler for the signal SIGTERM. */
static void sigterm_handler(int signo)
{
	error(1, 0, "catch signal SIGTERM - server shutdown");
}

/* sigint_handler: Handler for the signal SIGINT. */
static void sigint_handler(int signo)
{
	error(1, 0, "catch signal SIGINT - server shutdown");
}

/* common_exit: At exit function. */
static void common_exit(void)
{
	memfree();
	unlink(Lockfile);
}

/* 
 * Debug functions
 */

/*
static void testgetnodesppn()
{
	int rank;
	gb_job_t job;

	for (rank = 2; rank < 80; rank += 2)
	{
		fprintf(stderr, "\n%d:", rank);
		if ((getnodesppn("xeon80.cpct.sibsutis.ru", rank, 
						 &job.nodes, &job.ppn)) >= 0)
			fprintf(stderr, "nodes=%d:ppn=%d", job.nodes, job.ppn);
		else
			fprintf(stderr, "--");
	}
}
*/
