/*
 * gbrun.c: GBroker client interface. 
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <errno.h>

#include "gbroker.h"
#include "tcp.h"
#include "error.h"
#include "gbps.h"

/* printpsheader: */
static void printpsheader(void);

/* getps: Recv ps from gbroker. */
static int getps(char *host, char *psbuf);

/* checkusage: Check command line. */
static int checkusage(int argc, char **argv, char *hname);

int main(int argc, char **argv)
{
	char host[GB_HNAME_LEN];
	char *psbuf;

	if (checkusage(argc, argv, host) < 0)
		error(1, 0, "checkusage() failed");

	if ((psbuf = malloc(GB_PSBUF_LEN)) == NULL)
		error(1, errno, "malloc() failed for psbuf");

	if (getps(host, psbuf) < 0)
	{
		free(psbuf);
		error(1, 0, "getps() failed for host %s", host);
	}

	printpsheader();
	printf("%s", psbuf);

	free(psbuf);
	return 0;
}

/* printpsheader: Print job list. */
static void printpsheader(void)
{
	int i;

	printf("Id    Name          Total time  Exec time   ");
	printf("State     Host\n");

	for (i = 0; i < GB_PS_ID_FIELD; i++)
		putchar('-');
	putchar(' ');
	for (i = 0; i < GB_PS_NAME_FIELD; i++)
		putchar('-');
	putchar(' ');
	for (i = 0; i < GB_PS_TOTTIME_FIELD; i++)
		putchar('-');
	putchar(' ');
	for (i = 0; i < GB_PS_EXECTIME_FIELD; i++)
		putchar('-');
	putchar(' ');
	for (i = 0; i < GB_PS_STATE_FIELD; i++)
		putchar('-');
	putchar(' ');
	for (i = 0; i < GB_PS_HOST_FIELD; i++)
		putchar('-');
	putchar('\n');
}

/* getps: Recv ps from gbroker. */
static int getps(char *host, char *psbuf)
{
	SOCKET sock;
	int buflen;

	if ((sock = tcp_client(host, GBROKER_PORT)) < 0)
		return -1;

	if (sendn(sock, GB_PS_RQST, GB_SRVMSG_LEN) != GB_SRVMSG_LEN)
	{
		error(0, errno, "sendn() failed for GB_PS_RQST");
		goto getps_err;
	}

	if (recvn(sock, (char *) &buflen, sizeof(buflen)) != sizeof(buflen))
	{
		error(0, 0, "recvn() failed for buflen");
		goto getps_err;
	}

	if (recvn(sock, psbuf, buflen) != buflen)
	{
		error(0, 0, "recvn() failed for psbuf");
		goto getps_err;
	}

	return 0;

getps_err:
	close(sock);
	return -1;
}

/* check_usage: Check program usage. */
static int checkusage(int argc, char **argv, char *hname)
{
	enum { GBPS_NARGS = 3 };

	int this_option_optind = optind ? optind : 1, option_index = 0;
	int hoptind = 0;
	char opt;

	static struct option long_options[] = { {"--host", 1, 0, 0} }; 
	for (;;) 
	{
		this_option_optind = optind ? optind : 1;
		opt = getopt_long(argc, argv, "h", long_options, &option_index);
		if (opt == -1)
			break;
		switch (opt) 
		{
			case 'h':
				hoptind = optind;
				sprintf(hname, argv[hoptind]);
				break;
			case '?':
				error(0, 0, "Unrecognized option: -%c\n", optopt);
				goto cmd_err;
		}
	}

	if ((argc != GBPS_NARGS) || (!hoptind))
		goto cmd_err;

	return 0;

cmd_err:
	fprintf(stderr, "Usage:\tgbps -h <host>\n");
	return -1;
}
