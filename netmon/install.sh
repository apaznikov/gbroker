#!/bin/sh

NETMON_PATH=/opt/monitoring/netmon

#VPN_REMOTE_HOSTS="xeon32.cpct.sibsutis.ru xeon16.cpct.sibsutis.ru"
source ~/bin/header.sh

cd src
make 
cd ..

if [ -f obj/netmon ]; then
	cp  obj/netmon $NETMON_PATH/bin
	cp  obj/netmon_bw $NETMON_PATH/bin
#cp  etc/netmon-xeon80.vpn.local.conf \
#$NETMON_PATH/etc/netmon.conf

	chmod +s $NETMON_PATH/bin/netmon

  for host in $REMOTE_HOSTS; do
#for host in $VPN_REMOTE_HOSTS; do
		scp obj/netmon gdev@$host:/opt/monitoring/netmon/bin/
		scp obj/netmon_bw gdev@$host:/opt/monitoring/netmon/bin/
#scp etc/netmon-$host.conf \
#			gdev@$host:/opt/monitoring/netmon/etc/netmon.conf

		CMD="chmod +s $NETMON_PATH/bin/netmon"
		ssh -q $host $CMD
	done
fi
