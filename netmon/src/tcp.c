/*
 * nm_tcp.c: Library of common used functions used in client-server 
 *           applications.
 *
 * (C) 2008-2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <netdb.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <syslog.h>
#include <fcntl.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>

#include "netmon.h"
#include "tcp.h"
#include "error.h"

/* That is in order to make declaration of function 'inet_aton' explicit. */
int inet_aton(const char *cp, struct in_addr *inp);

/* connect_nonb: non-blocking connect(). */
static int connect_nonb(int s, const struct sockaddr *peer, socklen_t salen, 
                        int nsec);

/* set_address: Fill sap with hname and sname */
int set_address(char *hname, char *sname, struct sockaddr_in *sap, 
                char *protocol)
{
	struct servent *sp;
	struct hostent *hp;
	char *endptr;
	short port;

	bzero(sap, sizeof(*sap));
	sap->sin_family = AF_INET;

	if (hname != NULL)
	{
		if (!inet_aton(hname, &sap->sin_addr))
		{
			hp = gethostbyname(hname);
			if (hp == NULL)
			{
				error(0, errno, "Unknown host: %s", hname);
				return -1;
			}
			sap->sin_addr = *(struct in_addr * )hp->h_addr_list[0];
		}
	}
	else
		sap->sin_addr.s_addr = htonl(INADDR_ANY);

	port = strtol(sname, &endptr, 0);
	if (*endptr == '\0')
		sap->sin_port = htons(port);
	else
	{
		sp = getservbyname(sname, protocol);
		if (sp == NULL)
		{
			error(0, errno, "Unknown service: %s", sname);
			return -1;
		}
		sap->sin_port = sp->s_port;
	}

	return 0;
}

/* tcp_server: Common used tcp server code */
SOCKET tcp_server(char *hname, char *sname)
{
	struct sockaddr_in local;
	SOCKET s;
	const int on = 1;

	if (set_address(hname, sname, &local, "tcp") < 0)
		error(1, 0, "set_address() failed");

	if ((s = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		error(1, errno, "socket() failed");

	if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof(on)))
		error(1, errno, "setsockopt() failed");

	if (bind(s, (struct sockaddr *) &local, sizeof(local)))
		error(1, errno, "bind() failed");

	/* applog("Listen to %s:%s...", hname, sname); */
	if (listen(s, NLISTEN))
		error(1, errno, "listen() failed");

	return s;
}

/* tcp_client: Try to connect. Don't exit, if failed. */
SOCKET tcp_client(char *hname, char *sname)
{
	struct sockaddr_in peer;
	SOCKET s;

	/*
	applog("Trying %s...", hname);
	*/

	if (set_address(hname, sname, &peer, "tcp") < 0)
		return -1;

	s = socket(AF_INET, SOCK_STREAM, 0);
	if (s < 0)
		error(0, errno, "socket() failed");

	if (connect_nonb(s, (const struct sockaddr *) &peer, sizeof(peer), 
		             CONNECT_TIMEOUT) < 0)
		return -1;
	else
		return s;

	/*
	if (connect(s, (struct sockaddr *) &peer, sizeof(peer)))
		return -1;
	else
		return s;
	*/
}

/* connect_nonb: non-blocking connect(). */
static int connect_nonb(int s, const struct sockaddr *peer, socklen_t salen, 
                        int nsec)
{
	int flags, n, err;
	socklen_t len;
	fd_set rset, wset;
	struct timeval tval;

	flags = fcntl(s, F_GETFL, 0);
	fcntl(s, F_SETFL, flags | O_NONBLOCK);

	err = 0;
	if ((n = connect(s, peer, salen)) < 0)
	{
		if (errno != EINPROGRESS)
		{
			error(0, errno, "connect() failed");
			return -1;
		}
	}

	if (n == 0)
		goto done;		/* connect completed immediately */

	FD_ZERO(&rset);
	FD_SET(s, &rset);
	wset = rset;
	tval.tv_sec = nsec;
	tval.tv_usec = 0;

	if ((n = select(s + 1, &rset, &wset, NULL, nsec ? &tval : NULL)) == 0)
	{
		close(s);
		errno = ETIMEDOUT;
		/* error(0, errno, "select() failed"); */
		return -1;
	}

	if (FD_ISSET(s, &rset) || FD_ISSET(s, &wset))
	{
		len = sizeof(err);
		if (getsockopt(s, SOL_SOCKET, SO_ERROR, &err, &len) < 0)
		{
			error(0, errno, "getsockopt() failed");
			return -1;
		}
	}
	else
	{
		error(1, errno, "FD_ISSET failed(): socket not set");
		return -1;
	}

	done:
		fcntl(s, F_SETFL, flags);

		if (err)
		{
			close(s);
			errno = err;
			return -1;
		}
		return 0;
}

/* recvn: Read len bytes to buffer bp */
int recvn(SOCKET s, char *bp, size_t len)
{
	int cnt;
	int rc;

	cnt = len;
	while (cnt > 0)
	{
		rc = recv(s, bp, cnt, 0);
		if (rc < 0)			/* Read error? */
		{
			if (errno == EINTR)	/* Call interrupt? */
				continue;	/* Return error code */
			return -1;
		}
		if (rc == 0)			/* End of file? */
			return len - cnt;	/* Return incomplete counter */
        /* applog("RECVN received '%s'\n", bp); */
		bp += rc;
		cnt -= rc;
	}
	return len;
}

/* recvvrec: Read the record which size is len bytes to bp */
int recvvrec(SOCKET s, char *bp, size_t len)
{
	u_int32_t reclen;
	int rc;

	/* Read length of record */
	rc = recvn(s, (char *)&reclen, sizeof(u_int32_t));
	if (rc != sizeof(u_int32_t))
		return rc < 0 ? -1 : 0;
	reclen = ntohl(reclen);
	if (reclen > len)
	{
		/* 
		 * There's not enough memory in buffer to put data -
		 * discard it and return error code
		 */

		while (reclen > 0)
		{
			rc = recvn(s, bp, len);
			if (rc != len)
				return rc < 0 ? -1 : 0;
			reclen -= len;
			if (reclen < len)
				len = reclen;
		}

		errno = EMSGSIZE;
		return -1;
	}

	/* Read the record */
	rc = recvn(s, bp, reclen);
	if (rc != reclen)
		return rc < 0 ? -1 : 0;
	return rc;
}

/* recvline: Read a line, ended in '\n' and append binary 0 */
int recvline(SOCKET s, char *bufptr, size_t len)
{
	char *bufx = bufptr;
	static char *bp;
	static int cnt = 0;
	static char b[LINEBUFSZ];
	char c;

	while (--len > 0)
	{
		if (--cnt <= 0)
		{
			cnt = recv(s, b, sizeof(b), 0);
			if (cnt < 0)
			{
				if (errno == EINTR)
				{
					len++; /* decrease by 1 in 'while' */
					continue;
				}
				return -1;
			}
			if (cnt == 0)
				return 0;
			bp = b;
		}
		c = *bp++;
		*bufptr++ = c;
		if (c == '\n')
		{
			*bufptr = '\0';
			/* *bufptr = "\0" ? */
			return bufptr - bufx;
		}
	}
	errno = EMSGSIZE;
	return -1;
}

/* sendn: Send bytes of buffer bp */
int sendn(SOCKET s, char *bp, size_t len)
{
	int count;
	int rc;

	count = len;
	while (count > 0)
	{
		rc = send(s, bp, count, 0);
		if (rc < 0)
		{
			if (errno == EINTR)
				continue;
			return -1;
		}
		else if (rc == 0)
			return len - count;
		bp += rc;
		count -= rc;
	}

	return len;
}

/* sync_server: Syncronize by send. */
void sendsync(SOCKET s)
{
	if (sendn(s, SYNC_MSG, SYNC_MSG_LEN) != SYNC_MSG_LEN)
		error(1, errno, "sendn() failed");
}

/* sync_client: Syncronize client with server. */
void recvsync(SOCKET s)
{
	char msg[SYNC_MSG_LEN];

	if (recvn(s, msg, SYNC_MSG_LEN) != SYNC_MSG_LEN)
		error(1, errno, "recvn() failed");
}

/* sync_server: Syncronize by send. */
int sendtest(SOCKET s)
{
	if (sendn(s, SYNC_MSG, SYNC_MSG_LEN) != SYNC_MSG_LEN)
	{
		/*
		error(0, errno, "test sendn() failed");
		*/
		return -1;
	}
	/*
	applog("test send() ok");
	*/
	return 1;
}

/* sync_client: Syncronize client with server. */
int recvtest(SOCKET s)
{
	char msg[SYNC_MSG_LEN];

	if (recvn(s, msg, SYNC_MSG_LEN) != SYNC_MSG_LEN)
	{
		/*
		error(0, errno, "test recvn() failed");
		*/
		return -1;
	}
	/*
	applog("test recv() ok");
	*/
	return 1;
}

/* print_address: Print IP address and port number. */
void print_address(struct sockaddr_in peer)
{
	applog("%s:%d\n", inet_ntoa(peer.sin_addr), 
	       (int) ntohs(peer.sin_port));
}

/* getshortname: Get short hostname by full hostname (foo.bar.com -> foo). */ 
void getshorthname(const char *full_hname, char *short_hname)
{
	int dotloc;

	/* Find first occurence of '.' in hname. */
	for (dotloc = 0; dotloc < strlen(full_hname); dotloc++)
		if (full_hname[dotloc] == '.')
			break;
	dotloc++;

	snprintf(short_hname, dotloc, "%s", full_hname);
}
