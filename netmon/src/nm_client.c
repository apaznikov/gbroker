/*
 * nm_client.h: Netmon client functions. 
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include "netmon.h"
#include "nm_client.h"
#include "error.h"
#include "tcp.h"

/* nm_gettime: Return time to send size. */
double nm_gettime(char *host1, char *host2, double size)
{
	SOCKET sock;
	double des_time;
	char serv_msg[NM_SERV_MSG_LEN];
	struct timeval tv;

	if ((sock = tcp_client(host1, NETMON_PORT)) < 0)
	{
		error(0, errno, "Failed to connect to %s", host1);
		return -1;
	}

	/* Set recv timeout interval, because of strage. */
	tv.tv_sec = NM_SOCK_RCVTIMEOUT_SEC;
	tv.tv_usec = NM_SOCK_RCVTIMEOUT_USEC;
	setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));

	/* Send request to get info. */
	if (sendn(sock, GETINFO_RQST, NM_SERV_MSG_LEN) != NM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	if (sendn(sock, host2, NM_HNAME_LEN) != NM_HNAME_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	/* Recv reply. */
	if (recvn(sock, serv_msg, NM_SERV_MSG_LEN) != NM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "recvn() failed");
		return -1;
	}

	if (!strncmp(serv_msg, GETINFO_NOHOST, NM_SERV_MSG_LEN))
	{
		close(sock);
		error(0, 0, "No info about bandwidth between %s and %s available",
		      host1, host2);
		return -1;
	}

	if (sendn(sock, GETBAND_RQST, NM_SERV_MSG_LEN) != NM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	if (sendn(sock, (char *) &size, sizeof(size)) != sizeof(size))
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	/* Receive time. */
	if (recvn(sock, (char *) &des_time, sizeof(des_time)) != sizeof(des_time))
	{
		close(sock);
		error(0, errno, "recvn() failed");
		return -1;
	}

	close(sock);

	return des_time;
}

/* nm_gettimeforfile: Return time to send file. */
double nm_gettimeforfile(char *host1, char *host2, char *file)
{
	SOCKET sock;
	double des_time;
	char serv_msg[NM_SERV_MSG_LEN];
	int rc;
	struct timeval tv;

	if ((sock = tcp_client(host1, NETMON_PORT)) < 0)
	{
		error(0, errno, "Failed to connect to %s", host1);
		return -1;
	}

	/* Set recv timeout interval, because of strage. */
	tv.tv_sec = NM_SOCK_RCVTIMEOUT_SEC;
	tv.tv_usec = NM_SOCK_RCVTIMEOUT_USEC;
	setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));

	/* Send request to get info. */
	if (sendn(sock, GETINFO_RQST, NM_SERV_MSG_LEN) != NM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	if (sendn(sock, host2, NM_HNAME_LEN) != NM_HNAME_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	/* Recv reply. */
	if ((rc = recvn(sock, serv_msg, NM_SERV_MSG_LEN)) != NM_SERV_MSG_LEN)
	{
		error(0, errno, "recvn() failed");
		close(sock);
		return -1;
	}

	if (strncmp(serv_msg, GETINFO_OK, NM_SERV_MSG_LEN))
	{
		close(sock);
		error(0, 0, "No info about bandwidth between %s and %s available",
		      host1, host2);
		return -1;
	}

	if (sendn(sock, GETBANDFORFILE_RQST, NM_SERV_MSG_LEN) != NM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	if (sendn(sock, file, NM_PATH_LEN) != NM_PATH_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return -1;
	}

	/* Receive time. */
	if (recvn(sock, (char *) &des_time, sizeof(des_time)) != sizeof(des_time))
	{
		close(sock);
		error(0, errno, "recvn() failed");
		return -1;
	}

	close(sock);

	return des_time;
}

/* nm_gettable: Return pointer to bandwidth table. */
char *nm_gettable(char *host1, char *host2)
{
	SOCKET sock;
	char *table;
	int table_size;
	char serv_msg[NM_SERV_MSG_LEN];
	struct timeval tv;

	if ((sock = tcp_client(host1, NETMON_PORT)) < 0)
	{
		error(0, errno, "Failed to connect to %s", host1);
		return NULL;
	}

	/* Set recv timeout interval, because of strage. */
	tv.tv_sec = NM_SOCK_RCVTIMEOUT_SEC;
	tv.tv_usec = NM_SOCK_RCVTIMEOUT_USEC;
	setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));

	/* Send request to get info. */
	if (sendn(sock, GETINFO_RQST, NM_SERV_MSG_LEN) != NM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return NULL;
	}

	if (sendn(sock, host2, NM_HNAME_LEN) != NM_HNAME_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return NULL;
	}

	/* Recv reply. */
	if (recvn(sock, serv_msg, NM_SERV_MSG_LEN) != NM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "recvn() failed");
		return NULL;
	}

	if (!strncmp(serv_msg, GETINFO_NOHOST, NM_SERV_MSG_LEN))
	{
		close(sock);
		error(0, 0, "No info about bandwidth between %s and %s available",
		      host1, host2);
		return NULL;
	}

	if (sendn(sock, GETTABLE_RQST, NM_SERV_MSG_LEN) != NM_SERV_MSG_LEN)
	{
		close(sock);
		error(0, errno, "sendn() failed");
		return NULL;
	}

	/* Receive table. */
	if (recvn(sock, (char *) &table_size, sizeof(table_size)) 
		!= sizeof(table_size))
	{
		close(sock);
		error(0, errno, "recvn() failed");
		return NULL;
	}

	if ((table = (char *) malloc(table_size)) == NULL)
	{
		close(sock);
		error(0, errno, "malloc() failed\n");
		return NULL;
	}

	table[0] = '\0'; 

	/* Receive table. */
	if (recvn(sock, table, table_size) != table_size)
	{
		close(sock);
		error(0, errno, "recvn() failed");
		return NULL;
	}

	close(sock);

	return table;
}
