/*
 * nm_config_parser.c
 * (C) 2008-2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <libxml/parser.h>
#include <libxml/xmlmemory.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>

#include "nm_config_parser.h"
#include "netmon.h"
#include "tcp.h"

static measurement_t *parse_options(xmlDocPtr doc, xmlNodePtr cur, 
                                   int *interval, int *mtable_len);
static int cmp_mtable(const void *a, const void *b);

/* netmon_cfg_get_nhosts: Get number of remote hosts. */
int netmon_cfg_get_nhosts(char *Cfg_file)
{
	int nhosts;
	xmlDocPtr doc;
	xmlNodePtr cur;

	doc = xmlParseFile(Cfg_file);

	if (doc == NULL)
		error(1, 0, "xmlParseFile() failed");

	cur = xmlDocGetRootElement(doc);

	if (cur == NULL)
	{
		error(1, 0, "xmlDocGetRootElement() failed");
		xmlFreeDoc(doc);
	}

	if (xmlStrcmp(cur->name, (const xmlChar *) "netmon-config"))
	{
		error(1, 0, "wrong config file");
		xmlFreeDoc(doc);
	}

	nhosts = 0;

	for (cur = cur->xmlChildrenNode; cur->next != NULL; cur = cur->next)
	{

		if (!xmlStrcmp(cur->name, (const xmlChar *) "host-list"))
		{
			for (cur = cur->xmlChildrenNode; cur->next != NULL;
			     cur = cur->next)
			{
				if (!xmlStrcmp(cur->name, (const xmlChar *) "host"))
					nhosts++;
			}
		
			cur = cur->parent;
		}
	}

	xmlFreeDoc(doc);
	xmlCleanupParser();

	return nhosts;
}

/* netmon_readcfg: Read configuration file. */
void netmon_readcfg(char *Cfg_file, nm_host_t *hosts)
{
	xmlDocPtr doc;
	xmlNodePtr cur;
	xmlChar *key;
	int i, ihost, interval_default, mtable_len_default;

	measurement_t *mtable_default = NULL;

	doc = xmlParseFile(Cfg_file);

	if (doc == NULL)
		error(1, 0, "xmlParseFile() failed");

	cur = xmlDocGetRootElement(doc);

	if (cur == NULL)
	{
		error(1, 0, "xmlDocGetRootElement() failed");
		xmlFreeDoc(doc);
	}

	if (xmlStrcmp(cur->name, (const xmlChar *) "netmon-config"))
	{
		error(1, 0, "wrong config file");
		xmlFreeDoc(doc);
	}

	for (cur = cur->xmlChildrenNode; cur->next != NULL; cur = cur->next)
	{
		if (!xmlStrcmp(cur->name, (const xmlChar *) "options"))
		{
			mtable_default = parse_options(doc, cur, &interval_default, 
			                               &mtable_len_default);
			break;
		}
	}

	cur = xmlDocGetRootElement(doc);

	for (cur = cur->xmlChildrenNode; cur->next != NULL; cur = cur->next)
	{
		if (!xmlStrcmp(cur->name, (const xmlChar *) "host-list"))
		{
			for (cur = cur->xmlChildrenNode, ihost = 0; 
			     cur->next != NULL; 
				 cur = cur->next)
			{
				if (!xmlStrcmp(cur->name, (const xmlChar *) "host"))
				{
					key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
					sprintf(hosts[ihost].hname, "%s", (char *) key);

					getshorthname(hosts[ihost].hname, hosts[ihost].short_hname);

					xmlFree(key);

					/* Allocate memory and initialize host structure. */
					if ((hosts[ihost].mtable = (measurement_t *) 
					                           malloc(sizeof(*mtable_default) *
					                           mtable_len_default)) == NULL)
						error(1, errno, "mallo() failed");

					memcpy(hosts[ihost].mtable, mtable_default, 
						   mtable_len_default * sizeof(measurement_t));

					hosts[ihost].mtable_len = mtable_len_default;
					hosts[ihost].interval = interval_default;
					hosts[ihost].min_size = hosts[ihost].mtable[0].size;
					hosts[ihost].max_size = hosts[ihost].
					                        mtable[mtable_len_default - 1].size;
					hosts[ihost].isconnected = 0;

					for (i = 0; i < mtable_len_default; i++)
						hosts[ihost].mtable[i].time = 0.0;

					ihost++;
				}
			}
		
			cur = cur->parent;
		}
	}

	xmlFreeDoc(doc);
	xmlCleanupParser();

	if (mtable_default != NULL)
		free(mtable_default);

	/*
	for (ihost--; ihost >= 0; ihost--)
	{
		int i;

		printf("\n%s:\nsizes:\t", hosts[ihost].hname);
		for (i = 0; i < hosts[ihost].mtable_len; i++)
			printf("%d\t", hosts[ihost].mtable[i].size);
		printf("\ntimes:\t");
		for (i = 0; i < hosts[ihost].mtable_len; i++)
			printf("%0.2f\t", hosts[ihost].mtable[i].time);
		printf("\n");
		printf("sleep:\t%d\n", hosts[ihost].interval);
		printf("min-size: %d\t", hosts[ihost].min_size);
		printf("max-size: %d\n", hosts[ihost].max_size);
	}
	*/
}


/* cmp_mtable: Comparison function for mtable structure. */
static int cmp_mtable(const void *a, const void *b)
{
	measurement_t *m1 = (measurement_t *) a;
	measurement_t *m2 = (measurement_t *) b;

	return (m1->size / 1000 - m2->size / 1000);
}

/* parse_options: Parse options contained in <options> </options>. */
static measurement_t *parse_options(xmlDocPtr doc, xmlNodePtr cur, 
                                    int *interval, int *mtable_len)
{
	xmlChar *key;
	measurement_t *mtable;
	measurement_t *bsearch_res;
	int isize;
	double size;

	*mtable_len = 0;
	*interval = 0;
	
	for (cur = cur->xmlChildrenNode; cur->next != NULL; cur = cur->next)
	{
		if (!xmlStrcmp(cur->name, (const xmlChar *) "size-list"))
		{
			*mtable_len = 0;

			/* Count size count. */
			for (cur = cur->xmlChildrenNode; cur->next != NULL;
			     cur = cur->next)
				if (!xmlStrcmp(cur->name, (const xmlChar *) "size"))
					(*mtable_len)++;
			cur = cur->parent;

			if (*mtable_len == 0)
				error(1, 0, "Sizes must be specified in configuration file!");

			/* Allocate memory for default mtable. */
			if ((mtable = (measurement_t *) malloc((*mtable_len) *
							sizeof(*mtable))) == NULL)
				error(1, errno, "malloc() failed");

			/* Read sizes. */
			isize = 0;
			for (cur = cur->xmlChildrenNode; cur->next != NULL;
			     cur = cur->next)
			{
				if (!xmlStrcmp(cur->name, (const xmlChar *) "size"))
				{
					key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
					size = atof((char *) key) * 1000;
					xmlFree(key);

					bsearch_res = NULL;
					if (isize == 0)
						bsearch_res = NULL;
					else
						bsearch_res = bsearch(&size, mtable, isize,
											  sizeof(measurement_t), 
											  cmp_mtable);

					if (bsearch_res == NULL)
					{
						mtable[isize].size = (size > 0 ? size : 1000);
						mtable[isize].time = 0.0;
						isize++;
						qsort(mtable, isize, sizeof(measurement_t), 
						      cmp_mtable);
					}
				}
			}
			*mtable_len = isize;
		
			cur = cur->parent;
		}
		else if (!xmlStrcmp(cur->name, (const xmlChar *) "sleep"))
		{
			key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
			*interval = atoi((char *)key);
			xmlFree(key);
		}
	}

	if (*mtable_len == 0)
		error(1, 0, "Sizes must be specified in configuration file!");
	if (*interval == 0)
		error(1, 0, "Sleep interval must be specified in configuration file!");

	return mtable;
}
