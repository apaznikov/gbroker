/*
 * nm_band.h: Client module. 
 *            Get bandwidth value for specified message size and get table.
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "netmon.h"
#include "nm_client.h"

static int check_usage(int argc, char **argv, 
                       char *host1, char *host2, double *size);

int main(int argc, char **argv)
{
	char host1[NM_HNAME_LEN], host2[NM_HNAME_LEN];
	double size;

	if (check_usage(argc, argv, host1, host2, &size) < 0)
		return 1;

	if (size > 0)
	{
		double time;

		if ((time = nm_gettime(host1, host2, size)) >= 0)
			printf("%f\n", time);
		else
			return 1;
	}
	else
	{
		char *table;

		if ((table = nm_gettable(host1, host2)) != NULL)
		{
			printf("%s\n", DIVIDE_LINE3);
			printf("Msg, kbytes\tTime, sec\n");
			printf("%s\n", DIVIDE_LINE4);

			printf("%s", table);

			printf("%s\n", DIVIDE_LINE3);

			free(table);
		}
		else
			return 1;
	}

	return 0;
}

/* check_usage: Check program usage. */
static int check_usage(int argc, char **argv, 
                       char *host1, char *host2, double *size)
{
	enum { NARGS = 4 };

	if (argc < NARGS)
	{
		fprintf(stderr, "Usage:\t%s <host1> <host2> <size>\n", NETMON_BW_EXEC);
		fprintf(stderr, "or\t%s <host1> <host2> table\n", NETMON_BW_EXEC);
		return -1;
	}

	if (strcmp(argv[3], "table")) 
	{
		int i;

		for (i = 0; i < strlen(argv[3]); i++)
			if (!isdigit(argv[3][i])) 
			{
				fprintf(stderr, "msg-size must be positive integer\n");
				return -1;
			}

		*size = atof(argv[3]);

		if (*size < 1) 
		{
			fprintf(stderr, "msg-size must be positive integer\n");
			return -1;
		}
	}
	else
		*size = 0;

	sprintf(host1, "%s", argv[1]);
	sprintf(host2, "%s", argv[2]);

	return 0;
}

