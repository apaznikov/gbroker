/*
 * daemon.c
 *
 * (C) 2008-2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <syslog.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "daemon.h"
#include "error.h"

extern int ftruncate(int, off_t);
int inet_aton(const char *, struct in_addr *);

/* daemonize: Initialize damon process. */
void daemonize(void)
{
	int fd0, fd1, fd2;
	pid_t pid;
	struct rlimit r1;
	struct sigaction sa;
	int i;

	/* Take off mask of file creation mode. */
	umask(0);

	/* Get highest possible file descriptor number. */
	if (getrlimit(RLIMIT_NOFILE, &r1) < 0)
		error(1, errno, 
		     "can't get highest possible file descriptor number");
	
	/* To become the session leader to lose control terminal. */
	if ((pid = fork()) < 0)
		error(1, errno, "fork() failed");
	else if (pid != 0) /* parent process */
		exit(0);
	setsid();

	/* 
	 * Ensure impossibility of acquisition control terminal for the 
	 * future 
	 */
	sa.sa_handler = SIG_IGN;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	if (sigaction(SIGHUP, &sa, NULL) < 0)
		error(1, errno, "can't ignore signal SIGHUP");

	if ((pid = fork()) < 0)
		error(1, errno, "fork() failed");
	else if (pid != 0) /* parent process */	
		exit(0);
	

	/* 
	 * Make root directory to current working directory
	 * for it'll be possible to mount file system subsequently. 
	 */
	if (chdir("/") < 0)
		error(1, errno, "can't make '/' by current working directory");

	/* Close all opened file descriptor */
	if (r1.rlim_max == RLIM_INFINITY)
		r1.rlim_max = 1024;
	for (i = 0; i < r1.rlim_max; i++)
		close(i);

	/* Attach file descriptors 0, 1 and 2 to /dev/null.  */
	fd0 = open("/dev/null", O_RDWR);
	fd1 = dup(0);
	fd2 = dup(0);

	if (fd0 != 0 || fd1 != 1 || fd2 != 2)
		error(1, errno, "invalid file descriptors %d %d %d", 
		      fd0, fd1, fd2);
}

/* gobg: Go to the background mode. */
void gobg(void)
{
	pid_t pid;

	/* Take off mask of file creation mode. */
	umask(0);

	/* To become the session leader to lose control terminal. */
	if ((pid = fork()) < 0)
		error(1, errno, "fork() failed");
	else if (pid != 0) /* parent process */
		exit(0);
	setsid();
}

/* isrunning: Is the daemon already run? */
int isrunning(const char *home_path)
{
	int fd;
	char buf[BUFLEN];
	struct flock lck;

	sprintf(Lockfile, "%s/var", home_path);

	/* Check if directory exists. */
	if ((fd = open(Lockfile, O_EXCL)) < 0) 
	{
		/* Create directory. */
		if (mkdir(Lockfile, LOCKDIRMODE) < 0)
			error(1, errno, "can't create %s", Lockfile);	
	}

	sprintf(Lockfile, "%s/var/netmon.pid", home_path);
	
	/*
	if ((fd = open(Lockfile, O_RDWR | O_CREAT, LOCKMODE)) < 0)
		error(0, errno, "can't open %s", Lockfile);
	*/
	if ((fd = open(Lockfile, O_RDWR | O_CREAT | O_EXCL, LOCKMODE)) < 0)
	{
		error(0, errno, "can't open %s", Lockfile);
		return 1;
	}
	
	/* File structure need to lock file */
	lck.l_type = F_WRLCK;
	lck.l_whence = SEEK_SET;
	lck.l_start = 0;
	lck.l_len = 0;
	
	/* Try to lock file */
	if (fcntl(fd, F_SETLK, &lck) < 0) 
	{
		if (errno == EACCES || errno == EAGAIN) 
		{
			close(fd);
			return 1;
		}
		error(1, errno, "can't set lock on %s", Lockfile);
	}

	ftruncate(fd, 0);
	sprintf(buf, "%d", (int) getpid());
	write(fd, buf, strlen(buf));

	return 0;
}
