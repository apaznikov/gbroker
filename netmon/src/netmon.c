/*
 * netmon.c: Main netmon module
 *
 * (C) 2008-2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <getopt.h>
#include <string.h>
#include <syslog.h>
#include <errno.h>
#include <pthread.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <math.h>
#include <time.h>
#include <fcntl.h>
#include <semaphore.h>
#include <netdb.h>

#include "netmon.h"
#include "error.h"
#include "daemon.h"
#include "nm_config_parser.h"
#include "gb_subst.h"

nm_host_t       *Hosts = NULL;

char 		    Cfg_file[NM_PATH_LEN];
char		    Netmon_home[NM_PATH_LEN], Globus_home[NM_PATH_LEN];
char            Thishost[NM_HNAME_LEN];

char 		    *Buf_client = NULL;		/* Test msg on client side. */
char 		    *Buf_server = NULL;		/* Test msg on server side. */

int 		    Nhosts;
int             Nconnected;
int			    Is_runned_connect_thread = 0;

sem_t		    Sem_connect;			/* Semaphore for connect thread. */
pthread_mutex_t	Cond_mutex;
pthread_cond_t  Measure_cond;
pthread_t       Aside_tid, Bside_tid, Manage_tid, Connect_tid; 

char            Lockfile[BUFLEN];

int			    Gucflag = 0;					/* Usage globus-url-copy. */

static void *Aside_thread(void *arg);
static void *Bside_thread(void *arg);
static void *getinfo_thread(void *arg);
static void *gettime_for_table_thread(void *arg);

static void init_sync_flags(void);

static void *connect_thread(void *arg);
static void client_init(void);
static void *Bside_measurements(void *arg);

static void send_mmax(int ihost);
static void test_msg(int ihost, int isize);
static void test_msg_guc(int ihost, int isize);
static double _test_msg(int ihost, double size);
static double _test_msg_guc(int ihost, double size);
static int globus_url_copy(char *src, char *dest, int timeout);

static measurement_t *get_nearest_size(char *hname, double size);
static int findinhosts(char *hname);
static double gettime(int ihost, double size);

static void memfree(void);
static void sigint_handler(int signo);
static void sigterm_handler(int signo);
static void sighup_handler(int signo);

static void gettable(int ihost, char *table);
static int  check_usage(int argc, char **argv);
static void set_sighandlers(void);
static void set_thishost(void);
static void common_exit(void);

static void nm_jsdlsubst(char *ssrc, char *arch, char *sdest);
static void strreplace(char *s1, char *s2, char *s3);

#ifdef _DEBUG
static void print_tables(void);
#endif /* _DEBUG */

int main(int argc, char **argv)
{
	int size;
	char *cur_dir;

	if (check_usage(argc, argv) < 0)
		return 1;

	/* 
	 * Start daemon mode. 
	 * daemonize() changes current directory to root dir. 
	 * One should to save current * directory and restore it after 
	 * daemonize() will be runned. 
	 */
	size = pathconf(".", _PC_PATH_MAX);
	if ((cur_dir = (char *) malloc(size)) == NULL)
		fprintf(stderr, "malloc() failed: %s", strerror(errno));

	getcwd(cur_dir, size);

	if (init_log(Netmon_home) < 0)
		error(1, 0, "init_log() failed");

	/* Make sure, that no daemons copy has been already started. */
	if (isrunning(Netmon_home))
		error(1, 0, "daemon has been already started");

	#ifndef _DEBUG
	daemonize();
	#endif

	/*
	if (chdir(cur_dir) < 0)
		fprintf(stderr, "chdir() failed for %s: %s", cur_dir, strerror(errno));
		*/

	if (atexit(common_exit) != 0)
		error(1, errno, "atexit() failed");

	set_sighandlers();
	set_thishost();

	/* Get number of hosts needed to allocate memory. */
	Nhosts = netmon_cfg_get_nhosts(Cfg_file);

	if ((Hosts = (nm_host_t *) malloc(Nhosts * sizeof(nm_host_t))) == NULL)
		error(1, errno, "malloc() failed");

	/* Parse config file and fill Hosts structure. */
	netmon_readcfg(Cfg_file, Hosts);

	/* Init semaphores and mutexes. */
	init_sync_flags();

	if (pthread_create(&Bside_tid, NULL, Bside_thread, 0) != 0) 
	{
		memfree();
		error(1, errno, "pthread_create() failed for B-side thread");
	}

	if (!Gucflag)
		/* Start connect thread only if measurements by means of sockets. */
		if ((pthread_create(&Connect_tid, NULL, connect_thread, NULL) != 0))
		{
			memfree();
			error(1, errno, "pthread_create() failed for connect thread");
		}
	
	/* If no hosts to measure, return. */
	if (Nhosts > 0)
	{
		if ((pthread_create(&Aside_tid, NULL, Aside_thread, NULL) != 0))
		{
			memfree();
			error(1, errno, "pthread_create() failed for A-side thread");
		}
	}

	if (pthread_join(Bside_tid, NULL)) 
	{
		memfree();
		error(1, errno, "pthread_join() failed");
	}

	return 0;
}

/* Aside_thread: Process measurements (client) session. */
static void *Aside_thread(void *arg)
{
	int i, cell;

	/* Initialize buffer, start connect thread. */
	client_init();

	/* Begin measurements... */
	/* If globus-url-copy use, don't wait connection. */
	if (Gucflag)
		/* Measurements using globus-url-copy. */
	{
		for (i = 0; i < Nhosts; i++) 
		{
			test_msg_guc(i, 0);
			test_msg_guc(i, Hosts[i].mtable_len - 1);
		}

		srand(time(NULL));
		for (;;) 
		{
			for (i = 0; i < Nhosts; i++) 
			{
				cell = Hosts[i].mtable_len * (rand() / (RAND_MAX + 1.0));
				test_msg_guc(i, cell);
			}

			/*
			 * TODO
			 * Make possible different intervals between requests.
			 */
			#ifdef _DEBUG
			print_tables();
			#endif /* _DEBUG */
			sleep(Hosts[0].interval);
		}
	}
	else
		/* Measurements using sockets. */
	{
		srand(time(NULL));
		for (;;) 
		{
			/* Wait until some at least one host will be connected. */
			pthread_mutex_lock(&Cond_mutex);
			while (Nconnected == 0)
				pthread_cond_wait(&Measure_cond, &Cond_mutex);
			pthread_mutex_unlock(&Cond_mutex);

			for (i = 0; i < Nhosts; i++) 
			{
				if (Hosts[i].isconnected) 
				{
					cell = Hosts[i].mtable_len * (rand() / (RAND_MAX + 1.0));
					test_msg(i, cell);
				}
			}

			/*
			 * TODO
			 * Make possible different intervals between requests.
			 */
			#ifdef _DEBUG
			print_tables();
			#endif /* _DEBUG */
			sleep(Hosts[0].interval);
		}
	}

  return NULL;
}

/* send_mmax: Send maximal of max message to the remote host. */
static void send_mmax(int ihost)
{

	if (sendn(Hosts[ihost].sock, (char *) &Hosts[ihost].max_size, 
		      sizeof(Hosts[ihost].max_size)) != sizeof(Hosts[ihost].max_size)) 
	{
		close(Hosts[ihost].sock);
		Hosts[ihost].isconnected = 0;

		sem_post(&Sem_connect);

		pthread_mutex_lock(&Cond_mutex);
		Nconnected--;
		pthread_mutex_unlock(&Cond_mutex);

		error(0, errno, "sendn() failed");
	}
}

/* test_msg: Test message 'size' with 'ihost'. Write results to 'cell'. */
static void test_msg(int ihost, int isize)
{
	double time;

	time = _test_msg(ihost, Hosts[ihost].mtable[isize].size);

	if (time < 0)
	{
		close(Hosts[ihost].sock);
		Hosts[ihost].isconnected = 0;

		sem_post(&Sem_connect);

		pthread_mutex_lock(&Cond_mutex);
		Nconnected--;
		pthread_mutex_unlock(&Cond_mutex);
	}
	else
		Hosts[ihost].mtable[isize].time = time;
}

/* test_msg: Test message 'size' with 'ihost'. Write results to 'cell'. */
static void test_msg_guc(int ihost, int isize)
{
	double time;

	time = _test_msg_guc(ihost, Hosts[ihost].mtable[isize].size);

	if (time < 0)
	{
		if (Hosts[ihost].isconnected == 1)
			error(0, 0, "globus-url-copy() failed for %s", Hosts[ihost].hname);
		Hosts[ihost].isconnected = 0;
	}
	else
	{
		Hosts[ihost].mtable[isize].time = time;
		Hosts[ihost].isconnected = 1;
	}
}

/* _test_msg: Send and receive test message. Returns passed time. */
static double _test_msg(int ihost, double size)
{
	struct timeval time1, time2;
	double time;

	/* Send size of message will be sended. */
	if (sendn(Hosts[ihost].sock, (char *) &size, sizeof(size)) != sizeof(size))
	{
		error(0, errno, "sendn() failed for size");
		return -1;
	}

	/*
	 * Send and receive test message.
	 */

	/* Send test message to the server. */
	gettimeofday(&time1, NULL);

	if (sendn(Hosts[ihost].sock, Buf_client, size) != size)
	{
		error(0, errno, "sendn() failed");
		return -1;
	}

	gettimeofday(&time2, NULL);

    time = (time2.tv_sec * 1E6 + time2.tv_usec -
	        time1.tv_sec * 1E6 - time1.tv_usec) / 1E6;
		
	/* Received sended test message from the server. */
	gettimeofday(&time1, NULL);

	if (recvn(Hosts[ihost].sock, Buf_client, size) != size)
	{
		error(0, errno, "recvn() failed for Buf_client");
		return -1;
	}

	gettimeofday(&time2, NULL);

    time += (time2.tv_sec * 1E6 + time2.tv_usec -
	         time1.tv_sec * 1E6 - time1.tv_usec) / 1E6;

	time /= 2;

	/* Get time past from sending to receiving message.  */
	#ifdef _DEBUG
	applog("A-side: %s: size = %f kb, time = %0.3f sec", 
	       Hosts[ihost].short_hname, size / 1000, time);
	#endif /* _DEBUG */

	return time;
}

/* _test_msg_guc: Send test file. Return passed time. */
static double _test_msg_guc(int ihost, double size)
{
	struct timeval time1, time2;
	double time;
	char src[NM_PATH_LEN], dest[NM_PATH_LEN];

	/* Test connectivity. */
	sprintf(src, "file:///dev/null");
	sprintf(dest, "gsiftp://%s/tmp/nm_testfile_%s_%s", 
	        Hosts[ihost].hname, Thishost, Hosts[ihost].hname);
	if (globus_url_copy(src, dest, NM_GUC_TEST_TIMEOUT) < 0)
		return -1;

	/* Send test file. */
	sprintf(src, "gsiftp://%s%s/var/test-files/testfile%0.0f", 
	        Thishost, Netmon_home, size);
	sprintf(dest, "gsiftp://%s/tmp/nm_testfile_%s_%s", 
	        Hosts[ihost].hname, Thishost, Hosts[ihost].hname);

	gettimeofday(&time1, NULL);
	if (globus_url_copy(src, dest, 0) < 0)
		return -1;
	gettimeofday(&time2, NULL);

    time = (time2.tv_sec * 1E6 + time2.tv_usec -
	        time1.tv_sec * 1E6 - time1.tv_usec) / 1E6;

	/* Get time past from sending to receiving message.  */
	#ifdef _DEBUG
	applog("A-side: %s: size = %f kb, time = %0.3f sec", 
	       Hosts[ihost].short_hname, size / 1000, time);
	#endif /* _DEBUG */

	return time;
}

/* globus_url_copy: Globus Toolkit globus_url_copy launcher. */
int globus_url_copy(char *src, char *dest, int timeout)
{
	char rc[NM_GUC_RC_LEN] = "\0", cmd[NM_CMD_LEN] = "\0";
	FILE *pipe;

	#ifdef _DEBUG
	fprintf(stderr, "globus-url-copy -t %d %s %s\n", timeout, src, dest);
	#endif

	if (timeout > 0)
		sprintf(cmd, "globus-url-copy -t %d %s %s 2>&1", timeout, src, dest);
	else
		sprintf(cmd, "globus-url-copy %s %s 2>&1", src, dest);

	if ((pipe = popen(cmd, "r")) == NULL)
	{
		error(0, 0, "popen() failed for %s", cmd);
		return -1;
	}

	if (fscanf(pipe, "%s", rc) != EOF)
	{
		/*
		while (fscanf(pipe, "%s", rc) != EOF)
			applog("%s\n", rc);
		*/
		pclose(pipe);
		return -1;
	}

	pclose(pipe);

	return 0;
}

/* connect_thread: Try to connect to remote host in time lag. */
static void *connect_thread(void *arg)
{
	int i;

	/* Start only if there are any hosts. */
	sem_wait(&Sem_connect);
	sem_post(&Sem_connect);

	for (;;)
	{
		for (i = 0; i < Nhosts; i++)
		{
			if (!Hosts[i].isconnected)
			{
        applog("Connect to %s:%s...", Hosts[i].hname, NETMON_PORT);
				Hosts[i].sock = tcp_client(Hosts[i].hname, NETMON_PORT);

				if (Hosts[i].sock >= 0)
				{
					/* Send request to begin measurements. */
					if (sendn(Hosts[i].sock, MEASUREMENTS_RQST, 
						      NM_SERV_MSG_LEN) != NM_SERV_MSG_LEN)
						error(0, errno, "sendn() failed");

					/* Send max size of buffer. */
					send_mmax(i);

					/* Test min and max msg sizes. */
					test_msg(i, 0);
					test_msg(i, Hosts[i].mtable_len - 1);

					Hosts[i].isconnected = 1;

					/* Wait until some hosts will be disconnected. */
					sem_wait(&Sem_connect);

					/* Signal about host is connected for begin measurements. */
					pthread_mutex_lock(&Cond_mutex);
					if (Nconnected == 0)
						pthread_cond_signal(&Measure_cond);
					Nconnected++;
					pthread_mutex_unlock(&Cond_mutex);
				}
			}
		}
		
		sleep(NM_CONNECT_SLEEP);
	} 

	return NULL;
}

/* Initializing structures and creating sockets. */
static void client_init(void)
{
	enum { BYTE = 255 };
	int i, max_max_size;
	double byte;

	max_max_size = Hosts[0].max_size;

	/* Find max size of all hosts to fill send/recv buffer. */
	for (i = 0; i < Nhosts; i++)
		if (Hosts[i].max_size > max_max_size)
			max_max_size = Hosts[i].max_size;

	/* Allocate memory in accordance width found maximal 'mmax'. */
	if ((Buf_client = (char *) malloc(max_max_size)) == NULL)
	{
		memfree();
		error(1, errno, "malloc() failed for \'buf\'");
	}

	/* If globus-url-copy-measurements, create files. */
	if (Gucflag)
	{
		FILE *testfile;
		char fname[NM_PATH_LEN];
		int fd, ihost;

		sprintf(fname, "%s/var/test-files/", Netmon_home);

		/* Check if directory exists. */
		if ((fd = open(fname, O_EXCL)) < 0)
		{
			/* Create directory. */
			if (mkdir(fname, 0700) < 0)
				error(1, errno, "mkdir() failed for %s: %s\n", fname);
		}
		close(fd);

		/* Create files for globus-url-copy test. */
		for (ihost = 0; ihost < Nhosts; ihost++)
		{
			for (i = 0; i < Hosts[ihost].mtable_len; i++)
			{
				sprintf(fname, "%s/var/test-files/testfile%0.0f", 
						Netmon_home, Hosts[ihost].mtable[i].size);
				//fprintf(stderr, "create %s\n", fname);

				if (access(fname, F_OK) < 0)
				{
					//fprintf(stderr, "rc = %d, %s\n", rc, strerror(errno));
					/* Create file with random content. */
					testfile = fopen(fname, "w");
					srand(time(NULL));
					for (byte = 0; byte < Hosts[ihost].mtable[i].size; byte +=1)
						if (fputc(1 + (int) (BYTE * 
							(rand() / (RAND_MAX + 1.0))), testfile) == EOF)
							error(0, errno, "fputc() failed");
					fclose(testfile);
				}
			}
		}
	}
}

/* getinfo_thread: Get information from netmon daemon. */
static void *getinfo_thread(void *arg)
{
	char host[NM_HNAME_LEN], serv_msg[NM_SERV_MSG_LEN];
	int ihost;
	double size, des_time;

	getinfo_args_t *getinfo_args = (getinfo_args_t *) arg;
	SOCKET sclient = getinfo_args->sock;

	/* Recv hostname. */
	if (recvn(sclient, host, NM_HNAME_LEN) != NM_HNAME_LEN)
	{
		error(0, errno, "recvn() failed for host");
		close(sclient);
		return NULL;
	}

	if ((ihost = findinhosts(host)) < 0)
		/* If remote host not found, send error. */
	{
		if (sendn(sclient, GETINFO_NOHOST, NM_SERV_MSG_LEN) != NM_SERV_MSG_LEN)
			error(0, errno, "sendn() failed");
		close(sclient);
		return NULL;
	}
	else if ((Hosts[ihost].mtable[0].time < TIME_EPS) ||
		     (Hosts[ihost].mtable[Hosts[ihost].mtable_len - 1].time < TIME_EPS))
	{
		if (sendn(sclient, GETINFO_NOHOST, NM_SERV_MSG_LEN) != NM_SERV_MSG_LEN)
			error(0, errno, "sendn() failed for GETINFO_NOHOST");
		close(sclient);
		return NULL;
	}
	else
	{
		if (sendn(sclient, GETINFO_OK, NM_SERV_MSG_LEN) != NM_SERV_MSG_LEN)
		{
			error(0, errno, "sendn() failed for GETINFO_OK");
			close(sclient);
			return NULL;
		}
	}

	if (recvn(sclient, serv_msg, NM_SERV_MSG_LEN) != NM_SERV_MSG_LEN)
	{
		close(sclient);
		error(0, errno, "recvn() failed for serv_msg");
		return NULL;
	}

	if (!strncmp(serv_msg, GETBANDFORFILE_RQST, NM_SERV_MSG_LEN))
	{
		char file[NM_PATH_LEN];
		struct stat buf;

		/* Receive filename. */
		if (recvn(sclient, file, NM_PATH_LEN) != NM_PATH_LEN)
		{
			close(sclient);
			error(0, errno, "recvn() failed for file");
			return NULL;
		}

		size = (stat(file, &buf) == -1) ? (-1) : (buf.st_size);
		des_time = gettime(ihost, size);

		if (sendn(sclient, (char *) &des_time, sizeof(des_time)) 
			!= sizeof(des_time))
		{
			error(0, errno, "sendn() failed");
			close(sclient);
			return NULL;
		}
	}

	else if (!strncmp(serv_msg, GETBAND_RQST, NM_SERV_MSG_LEN))
		/* Get bandwidth value. */
	{
		/* Receive message size. */
		if (recvn(sclient, (char *) &size, sizeof(size)) != sizeof(size))
		{
			close(sclient);
			error(0, errno, "recvn() failed for size");
			return NULL;
		}

		des_time = gettime(ihost, size);

		if (sendn(sclient, (char *) &des_time, sizeof(des_time)) 
			!= sizeof(des_time))
		{
			error(0, errno, "sendn() failed");
			close(sclient);
			return NULL;
		}
	}
	else if (!strncmp(serv_msg, GETTABLE_RQST, NM_SERV_MSG_LEN))
	{
		char table[NM_TABLE_STR_LEN] = "\0";
		int table_len;

		gettable(ihost, table);
		table_len = strlen(table);

		if (sendn(sclient, (char *) &table_len, sizeof(table_len)) 
			!= sizeof(table_len))
		{
			error(0, errno, "sendn() failed");
			close(sclient);
			return NULL;
		}

		if (sendn(sclient, table, table_len) != table_len)
		{
			error(0, errno, "sendn() failed");
			close(sclient);
			return NULL;
		}
	}

	close(sclient);

	return NULL;
}

/* 
 * gettime_for_table_thread: Get time for list of files and list of hosts 
 * (gbroker use). 
 */
static void *gettime_for_table_thread(void *arg)
{
	getinfo_args_t *getinfo_args = (getinfo_args_t *) arg;
	SOCKET sclient = getinfo_args->sock;

	char **filelist         = NULL;
	nm_hostlist_t *hostlist = NULL;
	double *stagetimes      = NULL;

	double size, time;
	int i, j, ihost, nfiles = 0, nhosts = 0;
	char filename[NM_PATH_LEN];
	struct stat buf;

	int rc;

	/*
	 * Files
	 */

	/* Recv number of files. */
	if ((rc = recvn(sclient, (char *) &nfiles, sizeof(nfiles))) 
		!= sizeof(nfiles))
	{
		error(0, errno, "recvn() failed for nfiles (%d bytes recived of %d)",
		      rc, sizeof(nfiles));
		goto lbl_return;
	}

	/* Allocate memory for filelist. */
	if ((filelist = malloc(nfiles * sizeof(char *))) == NULL)
	{
		error(0, errno, "malloc() failed for filelist");
		goto lbl_return;
	}

	for (i = 0; i < nfiles; i++)
		if ((filelist[i] = malloc(NM_PATH_LEN)) == NULL)
		{
			error(0, errno, "malloc() failed for filelist[%d]", i);
			goto lbl_return;
		}

	/* Recvn filenames. */
	for (i = 0; i < nfiles; i++)
		if (recvn(sclient, filelist[i] , NM_PATH_LEN) != NM_PATH_LEN)
		{
			error(0, errno, "recvn() failed for filelist[%d]", i);
			goto lbl_return;
		}

	/*
	 * Hosts
	 */

	/* Recv number of hosts. */
	if (recvn(sclient, (char *) &nhosts, sizeof(nhosts)) != sizeof(nhosts))
	{
		error(0, errno, "recvn() failed for nhosts");
		goto lbl_return;
	}

	/* Allocate memory for hostlist and stagetimes. */
	if ((hostlist = malloc(nhosts * sizeof(nm_hostlist_t))) == NULL)
	{
		error(0, errno, "malloc() failed for hostlist (nhosts = %d)");
		applog("nfiles = %d\n", nfiles);
		for (i = 0; i < nfiles; i++)
			applog("filelist[%d] = '%s'\n", i, filelist[i]);
		goto lbl_return;
	}

	if ((stagetimes = malloc(nhosts * sizeof(*stagetimes))) == NULL)
	{
		error(0, errno, "malloc() failed for stagetimes");
		goto lbl_return;
	}

	/* Recvn hostlist (hostnames and architectures). */
	if (recvn(sclient, (char *) hostlist, nhosts * sizeof(nm_hostlist_t))
				!= nhosts * sizeof(nm_hostlist_t))
	{
		error(0, errno, "recvn() failed for hostlist[%d]", i);
		goto lbl_return;
	}

	/*
	 * Get time
	 */

	for (i = 0; i < nhosts; i++)
	{
		stagetimes[i] = 0;

		if (strcmp(hostlist[i].hname, Thishost))
		{
			/* Look for hostname. If host is not found, send error. */
			if ((ihost = findinhosts(hostlist[i].hname)) >= 0)
				/* If host exists. */
			{
				/* Time to stage files to this host. */
				for (j = 0; j < nfiles; j++)
				{
					nm_jsdlsubst(filelist[j], hostlist[i].arch, filename);

					if (access(filename, F_OK) >= 0)
						/* If file exists. */
					{
						size = (stat(filename, &buf) == -1) ? (-1) : 
						                                      (buf.st_size);
						
						if ((time = gettime(ihost, size)) < 0)
							time = 0;
						stagetimes[i] += time;

					}
				}
			}
		}
	}

	#ifdef _DEBUG
	applog("STAGETIMES:");
	for (i = 0; i < nhosts; i++)
		applog("stagetime(%s): %f",hostlist[i].hname, stagetimes[i]);
	#endif
	
	/* Send stagetimes. */

	if (sendn(sclient, (char *) stagetimes, nhosts * sizeof(*stagetimes)) 
		!= nhosts * sizeof(*stagetimes))
	{
		error(0, errno, "sendn() failed for stagetimes for stagetimes");
		goto lbl_return;
	}

lbl_return:
	if (filelist != NULL)
	{
		for (i = 0; i < nfiles; i++)
			if (filelist[i] != NULL)
				free(filelist[i]);
		free(filelist);
	}
	if (hostlist != NULL)
		free(hostlist);
	if (stagetimes != NULL)
		free(stagetimes);

	close(sclient);
	return NULL;
}

/* gettime: Compute time to send size bytes. */
static double gettime(int ihost, double size)
{
	double time1, time2, size1, size2; 
	int i, j;

	if ((Hosts[ihost].mtable[0].time < TIME_EPS) ||
	    (Hosts[ihost].mtable[Hosts[ihost].mtable_len - 1].time < TIME_EPS))
		return -1;

	/* Find nearest measurements in table and interpolate desired time. */
	if (size <= Hosts[ihost].min_size)
	{
		return size / (Hosts[ihost].mtable[0].size / 
		               Hosts[ihost].mtable[0].time);
	}
	else if (size >= Hosts[ihost].max_size)
	{
		return size / (Hosts[ihost].max_size / 
		               Hosts[ihost].mtable[Hosts[ihost].mtable_len - 1].time);
	}
	else
	{
		for (i = 1; i < Hosts[ihost].mtable_len; i++) 
		{
			if (size < Hosts[ihost].mtable[i].size) 
			{
				/* Find nearest non-zero time (low bound). */
				j = i - 1;
				while (((Hosts[ihost].mtable[j].time) < TIME_EPS) &&
					   (j > 0))
					j--;

				size1 = Hosts[ihost].mtable[j].size;
				time1 = Hosts[ihost].mtable[j].time;

				/* Find nearest non-zero time (upper bound). */
				j = i;
				while (((Hosts[ihost].mtable[j].time) < TIME_EPS) &&
						(j < Hosts[ihost].mtable_len))
					j++;

				size2 = Hosts[ihost].mtable[j].size;
				time2 = Hosts[ihost].mtable[j].time;

				return  (size - size1) * (time2 - time1) / 
				        (size2 - size1) + time1;
			}
		}
	}

	return -1;
}

/* server_process: Process server session. */
static void *Bside_thread(void *arg)
{
	SOCKET sserver;
	SOCKET sclient;

	struct sockaddr_in peer;
	socklen_t peerlen = sizeof(peer);

	pthread_t Bside_tid, getinfo_tid;
	Bside_args_t Bside_args;

	getinfo_args_t getinfo_args;

	char serv_msg[NM_SERV_MSG_LEN];

	sserver = tcp_server(Thishost, NETMON_PORT);


	/*
	 * 1. Create server
	 * 2. Accept client
	 * 3. If client is netmon, create measurement thread
	 * 4. If client is performance query, create getinfo thread
	 */

	for (;;)
	{
		if ((sclient = accept(sserver, (struct sockaddr *) &peer, 
		                      (socklen_t *) &peerlen)) < 0)
			error(0, errno, "accept() failed");
		else
		{
			if (recvn(sclient, serv_msg, NM_SERV_MSG_LEN) != NM_SERV_MSG_LEN)
				error(0, errno, "recvn() failed for serv_msg");
			
			if (!strncmp(serv_msg, MEASUREMENTS_RQST, NM_SERV_MSG_LEN))
				/* Request from netmon service. Begin measurements... */
			{
				Bside_args.sock = sclient;
				getnameinfo((struct sockaddr *) &peer, (socklen_t) peerlen,
							Bside_args.hname, NM_HNAME_LEN, NULL, 0, NI_NOFQDN);
				getshorthname(Bside_args.hname, Bside_args.short_hname);

				if (pthread_create(&Bside_tid, NULL, 
								   Bside_measurements, &Bside_args) != 0) 
				{
					error(0, errno, "pthread_create() failed for B-side");
					close(sclient);
				}
			}
			else if (!strncmp(serv_msg, GETINFO_RQST, NM_SERV_MSG_LEN))
				/* Request from netmon client. Get info... */
			{
				pthread_attr_t attr;
				pthread_attr_init(&attr);
				pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

				getinfo_args.sock = sclient;

				if (pthread_create(&getinfo_tid, &attr, 
					               getinfo_thread, &getinfo_args) != 0) 
				{
					error(0,errno,"pthread_create() failed for getinfo thread");
					close(sclient);
				}
			}
			else if (!strncmp(serv_msg, GETTABLE_RQST, NM_SERV_MSG_LEN))
				/* 
				 * Request from netmon client (gbroker e.g.). 
				 * Get time for hostlist and filelist (table). 
				 */
			{
				pthread_attr_t attr;
				pthread_attr_init(&attr);
				pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

				getinfo_args.sock = sclient;

				if (pthread_create(&getinfo_tid, &attr, 
					               gettime_for_table_thread, &getinfo_args)!= 0) 
				{
					error(0, errno, 
				        "pthread_create() failed for gettime_for_table_thread");
					close(sclient);
				}
			}
		}
	}

  return NULL;
}

/* Bside_measurements: Network monitoring on the B (server) side. */
static void *Bside_measurements(void *arg)
{
	double size = 0;//, max_size = 0;
  int max_size;
	Bside_args_t *Bside_args = (Bside_args_t *) arg;
	SOCKET sclient = Bside_args->sock;
	struct timeval time1, time2;
	double time = 0;
	measurement_t *match;		/* Size matched with own A-side table. */ 

	/* Receive 'mmax' from the client. */
	if ((recvn(sclient, (char *) &max_size, sizeof(max_size)))
	    != sizeof(max_size))
	{
		close(sclient);
		error(0, errno, "recvn() failed for mmax");
		return NULL;
	}

	if ((Buf_server = (char *) malloc(max_size)) == NULL)
	{
		close(sclient);
		error(1, errno, "malloc() failed for \'buf\'");
		return NULL;
	}

	for (;;)
	{
		/* Receive message size. */
		if ((recvn(sclient, (char *) &size, sizeof(size))) != sizeof(size))
		{
			close(sclient);
			error(0, errno, "recvn() failed for size");
			return NULL;
		}

		/* Check if size is in own A-side table. */
		if ((match = get_nearest_size(Bside_args->hname, size)) != NULL)
		{
			/*
			 * Receive and send test message.
			 */

			/* Receive test message. */

			gettimeofday(&time1, NULL);

			if (recvn(sclient, Buf_server, size) != size)
			{
				close(sclient);
				error(0, errno, "recvn() failed Buf_server");
				return NULL;
			}

			gettimeofday(&time2, NULL);

			time = (time2.tv_sec * 1E6 + time2.tv_usec -
					time1.tv_sec * 1E6 - time1.tv_usec) / 1E6;

			/* Send received test message. */

			gettimeofday(&time1, NULL);

			if (sendn(sclient, Buf_server, size) != size)
			{
				close(sclient);
				error(0, errno, "sendn() failed");
				return NULL;
			}

			gettimeofday(&time2, NULL);

			time += (time2.tv_sec * 1E6 + time2.tv_usec -
					 time1.tv_sec * 1E6 - time1.tv_usec) / 1E6;

			time /= 2;

			match->time = time;

			#ifdef _DEBUG
			applog("B-side: %s: size = %d kb, time = %0.4f sec", 
				   Bside_args->short_hname, size / 1000, time, match->size);
			#endif /* _DEBUG */
		}
		else
		{
			/*
			 * Receive and send test message.
			 */

			/* Receive test message. */
			if (recvn(sclient, Buf_server, size) != size)
			{
				close(sclient);
        applog("buf_server = '%s' size = %d\n", Buf_server, size);
				error(0, errno, "recvn() failed for Buf_server");
				return NULL;
			}

			/* Send received test message. */
			if (sendn(sclient, Buf_server, size) != size)
			{
				close(sclient);
				error(0, errno, "sendn() failed");
				return NULL;
			}

			#ifdef _DEBUG
			applog("B-side: %s: size = %d kb", 
				   Bside_args->short_hname, size / 1000);
			#endif /* _DEBUG */
		}
	}
}


/* get_nearest_size: Search B-side size in own A-side table. */
static measurement_t *get_nearest_size(char *hname, double size)
{
	int ihost_found, i;

	if (Gucflag)
		return NULL;

	ihost_found = findinhosts(hname);

	if (ihost_found >= 0)
		for (i = 0; i < Hosts[ihost_found].mtable_len; i++)
			if (fabs(log(Hosts[ihost_found].mtable[i].size) - 
				     log(size)) < SIZE_EPS)
				return &Hosts[ihost_found].mtable[i];
	return NULL;
}

/* findinhosts: Find hname in Hosts. */
static int findinhosts(char *hname)
{
	int i;

	for (i = 0; i < Nhosts; i++)
		if (!strcmp(hname, Hosts[i].hname))
			return i;
	return -1;
}

/* init_sync_flags: Init all semaphores and mutexes. */ 
static void init_sync_flags(void)
{
	sem_init(&Sem_connect, 0, Nhosts);
	pthread_mutex_init(&Cond_mutex, NULL);
	pthread_cond_init(&Measure_cond, NULL);
	Nconnected = 0;
}

/* sigterm: Handler for the signal SIGTERM. */
static void sigterm_handler(int signo)
{
	int i;


	for (i = 0; i < Nhosts; i++)
		close(Hosts[i].sock);

	memfree();
	unlink(Lockfile);

	error(1, 0, "catch signal SIGTERM - server shutdown");
}

/* sigint_handler: Handler for the signal SIGINT. */
static void sigint_handler(int signo)
{
	int i;

	for (i = 0; i < Nhosts; i++)
		close(Hosts[i].sock);

	error(1, 0, "catch signal SIGINT - server shutdown");
}

/* sighup_handler: Handler for the signal SIGHUP. */
static void sighup_handler(int signo)
{
	error(0, 0, "catch signal SIGHUP - read configuration file");
	netmon_readcfg(Cfg_file, Hosts);

	/*
	 * TODO:
	 * Reset netmon at SIGHUP
	 */
}

/* common_exit: At exit function. */
static void common_exit(void)
{
	memfree();
	unlink(Lockfile);
}

/* memfree: Free global allocated memory. */
static void memfree(void)
{
	int i;

	for (i = 0; i < Nhosts; i++)
	{
		if (Hosts[i].mtable != NULL)
			free(Hosts[i].mtable);
	}

	free(Hosts);

	if (Buf_client != NULL)
		free(Buf_client);

	if (Buf_server != NULL)
		free(Buf_server);

	return;
}

/* gettable: Return bandwidth table with host. */
static void gettable(int ihost, char *table)
{
	int i;

	if (!Hosts[ihost].isconnected)
		sprintf(table, "not connected!\n");

	for (i = 0; i < Hosts[ihost].mtable_len; i++)
		sprintf(table, "%s%0.0f\t\t%0.4f\n", 
		        table, Hosts[ihost].mtable[i].size / 1000,
		        Hosts[ihost].mtable[i].time);
}

/* print_pairs: Print statistics for all pairs. */
#ifdef _DEBUG
static void print_tables(void)
{
	char logstr[NM_TABLE_STR_LEN] = "\n";
	int i, cell, print_size;

	if (Gucflag)
		/* globus-url-copy */
	{
		applog(DIVIDE_LINE1);
		applog("Msg, kbytes\tTime, sec");

		for (i = 0; i < Nhosts; i++)
		{
			applog(DIVIDE_LINE2);

			if (Hosts[i].isconnected)
				applog("%s", Hosts[i].hname);
			else
				applog("%s (not connected)", Hosts[i].hname);

			for (cell = 0; cell < Hosts[i].mtable_len; cell++)
			{
				print_size = Hosts[i].mtable[cell].size / 1000;
				applog("%d\t\t%0.4f", print_size, 
									  Hosts[i].mtable[cell].time);
			}
		}
		sprintf(logstr, "%s%s", logstr, DIVIDE_LINE1);
		applog(DIVIDE_LINE1);

		return;
	}

	/* Sockets. */

	if (Nconnected == 0)
		return;

	applog(DIVIDE_LINE1);
	applog("Msg, kbytes\tTime, sec");

	for (i = 0; i < Nhosts; i++)
	{
		if (Hosts[i].isconnected)
		{
			applog(DIVIDE_LINE2);
			applog("Host: %s", Hosts[i].hname);

			for (cell = 0; cell < Hosts[i].mtable_len; cell++)
			{
				print_size = Hosts[i].mtable[cell].size / 1000;
				applog("%d\t\t%0.4f", print_size, Hosts[i].mtable[cell].time);
			}
		}
	}
	sprintf(logstr, "%s%s", logstr, DIVIDE_LINE1);
	applog(DIVIDE_LINE1);
}
#endif /* _DEBUG */

/* set_sighandlers: Set signal handlers. */
static void set_sighandlers(void)
{
	struct sigaction sa;

	sigset_t mask;

	sigfillset(&mask);
	sigdelset(&mask, SIGINT);
	sigdelset(&mask, SIGTERM);
	sigdelset(&mask, SIGHUP);
	sigprocmask(SIG_SETMASK, &mask, NULL);

	sa.sa_handler = sigint_handler;
	sigaction(SIGINT, &sa, NULL);

	sa.sa_handler = sigterm_handler;
	sigaction(SIGTERM, &sa, NULL);

	sa.sa_handler = sighup_handler;
	sigaction(SIGHUP, &sa, NULL);
}

/* set_thithost: Set Thishost variable (hostname). */
static void set_thishost(void)
{
	FILE *fp;
	char hostnamefile[NM_PATH_LEN];

	sprintf(hostnamefile, "%s/etc/hostname", Netmon_home);

	if ((fp = fopen(hostnamefile, "r")) != NULL) 
	{
		fscanf(fp, "%s", Thishost);
		fclose(fp);

		if (strlen(Thishost) > 0)
			return;
	}

	if (gethostname(Thishost, NM_HNAME_LEN) < 0)
		error(1, errno, "gethostnamej() failed");
}

/* check_usage: Check command line usage. */
static int check_usage(int argc, char **argv)
{
	int this_option_optind = optind ? optind : 1;
	int option_index = 0, cflag = 0;
	int errflag = 0, fd;
	char opt, *netmon_home_tmp, *globus_home_tmp1, *globus_home_tmp2;
	static struct option long_options[] = { {"config", 1, 0, 0}, };

	if ((netmon_home_tmp = getenv("NETMON_HOME")) == NULL) 
	{
		fprintf(stderr, "Environment variable NETMON_HOME must be set up\n");
		return -1;
	}
	sprintf(Netmon_home, "%s", netmon_home_tmp);

	for (;;) 
	{
		this_option_optind = optind ? optind : 1;
		opt = getopt_long(argc, argv, "cg", long_options, &option_index);
		if (opt == -1)
			break;
		switch (opt) 
		{
			case 'c':
				cflag++;
				break;
			case 'g':
			{
				char guc_path[NM_PATH_LEN]; /* globus-url-copy command */
				Gucflag = 1;

				/* Check envs for GLOBUS. */
				if (((globus_home_tmp1 = getenv("GLOBUS_PATH")) == NULL) &&
				    ((globus_home_tmp2 = getenv("GLOBUS_LOCATION")) == NULL))
				{
					fprintf(stderr, 
		"Environment variable GLOBUS_PATH or GLOBUS_LOCATION must be set up\n");
					return -1;
				}
				if (globus_home_tmp1 != NULL)
					sprintf(Globus_home, "%s", globus_home_tmp1);
				else
					sprintf(Globus_home, "%s", globus_home_tmp2);

				sprintf(guc_path, "%s/bin/globus-url-copy", Globus_home);
				
				/* Test if globus-url-copy exists. */
				if (access(guc_path, X_OK) < 0)
				{
					fprintf(stderr, "Can not execute %s\n", guc_path);
					return -1;
				}

				break;
			}
			case '?':
				error(0, 0, "Unrecognized option: -%c\n", optopt);
				errflag++;
		}
	}

	if (errflag) 
	{
		fprintf(stderr, "Usage: %s [OPTION]\n\n", argv[0]);
		fprintf(stderr, "-c\t\t<config-file>\n");
		return -1;
	}


	if (cflag)
		sprintf(Cfg_file, "%s", argv[optind]);
	else 
	{
		sprintf(Cfg_file, "%s/etc", Netmon_home);

		/* Create etc directory if it not exists. */
		if ((fd = open(Cfg_file, O_EXCL)) < 0) 
		{
			if (mkdir(Cfg_file, LOCKDIRMODE) < 0)
				error(1, errno, "can't create %s", Cfg_file);	
		}
		close(fd);
		sprintf(Cfg_file, "%s/etc/netmon.conf", Netmon_home);
	}

	return 0;
}

/* nm_jsdlsubst: Make substitutions with line. */
static void nm_jsdlsubst(char *ssrc, char *arch, char *sdest)
{
	sprintf(sdest, "%s", ssrc);

	strreplace(sdest, GB_JSDLSUBST_ARCH, arch);
}

/* strreplace: Find s2 in s1 and replace it by s3. */
static void strreplace(char *s1, char *s2, char *s3)
{
	char s[GB_JOBDESCR_LEN], *p = NULL;

	while ((p = strstr(s1, s2)) != NULL)
	{
		snprintf(s, strlen(s1) - strlen(p) + 1, "%s", s1);
		strcat(s, s3);
		p += strlen(s2);
		strcat(s, p);
		sprintf(s1, "%s", s);
	}
}
