/*
 * netmon_cfgparse.h
 * (c) 2008 Alexey Paznikov
 */

#ifndef _NETMON_CFGPARSE_H_
#define _NETMON_CFGPARSE_H_

#include "netmon.h"

enum
{
	SIZES_LEN      = 1024,
	SIZE_TOKEN_LEN = 255
};

/* netmon_readcfg: Read configuration file. */
void netmon_readcfg(char *cfg_file, nm_host_t *hosts);

/* netmon_cfg_get_nhosts: Get number of remote hosts. */
int netmon_cfg_get_nhosts(char *cfg_file);

#endif /* _NETMON_CFGPARSE_H_ */
