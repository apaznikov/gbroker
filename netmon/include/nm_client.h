/*
 * nm_client.h: Netmon client functions. 
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _NM_CLIENT_H
#define _NM_CLIENT_H

#include "netmon.h"

enum
{
	NM_SOCK_RCVTIMEOUT_SEC  = 2,
	NM_SOCK_RCVTIMEOUT_USEC = 0
};

/* nm_get_nettime_for_table: Get time for list of files and list of hosts. */
int nm_get_nettime_for_table(char *host, 
                             char **filelist, int nfiles,
                             nm_hostlist_t *hostlist, int nhosts, 
							 double *stagetimes);

/* nm_gettime: Return time to send size. */
double nm_gettime(char *host1, char *host2, double size);

/* nm_gettimeforfile: Return time to send file. */
double nm_gettimeforfile(char *host1, char *host2, char *file);

/* gettable: Return pointer to bandwidth table. */
char *nm_gettable(char *host1, char *host2);

#endif /* _NM_CLIENT_H */
