/*
 * netmon.h
 *
 * (C) 2008-2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _NETMON_H
#define _NETMON_H

#include "tcp.h"

//#define	_DEBUG

enum
{
	NM_PATH_LEN			      = 255,
	NM_RETURN_OK 			  = 0,
	NM_ERR_CMD 			      = 11,
	NM_ERR_FUNC 			  = -1,
	NM_STR_LEN 			      = 255,
	NM_NMAX_HOSTS             = 4,
	NM_PREFIX_SIZE            = 1,
	NM_TIME_FIELD_WIDTH       = 8,
	NM_TEMP_BUF_LEN           = 2,

	NM_HNAME_LEN              = 80,
	NM_SNAME_LEN              = 6,
	NM_ARCH_LEN               = 10,

	NM_N_CONNECT_ATTEMPTS     = 100000,
	NM_CONNCET_ATTEMPTS_SLEEP = 5,
	NM_CONNECT_SLEEP          = 5,

	NM_SERV_MSG_SZ            = 1,
	NM_TABLE_STR_LEN          = 4096,

	NM_SERV_MSG_LEN           = 10, 

	NM_CMD_LEN                = 1024,

	/* End-point connectivity test time out. */
	NM_GUC_TEST_TIMEOUT       = 5,

	NM_GUC_RC_LEN             = 1024
};

#define NETMON_BW_EXEC      "netmon_bw"

/* Client requests. */
#define MEASUREMENTS_RQST   "nm_measure"
#define GETINFO_RQST        "nm_getinfo"
#define GETINFO_NOHOST      "no_hostfou"
#define GETINFO_OK          "nm_getinok"
#define GETTABLE_RQST       "nm_gettabl"

#define GETBAND_RQST		"nm_getband"
#define GETBANDFORFILE_RQST	"nm_gbwfile"
#define GETTABLE_RQST		"nm_gettabl"

/* Netmon intercommunication protocol. */
#define HALT				"h"
#define TEST_MSG			"m"
#define BUF 				"b"

#define NETMON_PORT			"13000"

/* Management commands. */
#define SERV_MSG_YES		"y"
#define SERV_MSG_NO			"n"
#define NETMON_GET_TIME		"b"

#define SIZE_EPS			0.05	    /* eps-value for get_nearest_size() */
#define TIME_EPS            0.00000001	/* time eps-value for getinfo_thread()*/

typedef struct
{
	SOCKET sclient;
} sm_args_t;

typedef struct
{
	double size;
	double time;
} measurement_t;

typedef struct
{
	char hname[NM_HNAME_LEN];
	char arch[NM_ARCH_LEN];
} nm_hostlist_t;

typedef struct
{
	SOCKET sock;

	char   hname[NM_HNAME_LEN];
	char   short_hname[NM_HNAME_LEN];

	int    isconnected;

	int    mtable_len;
	int    interval;
	int    min_size;
	int    max_size;
	measurement_t *mtable;
} nm_host_t;

typedef struct
{
	SOCKET sock;
	char hname[NM_HNAME_LEN];
	char short_hname[NM_HNAME_LEN];
} Bside_args_t;

typedef struct
{
	SOCKET sock;
} getinfo_args_t;

#define DIVIDE_LINE1		"==============================="
#define DIVIDE_LINE2		"-------------------------------"
#define DIVIDE_LINE3		"==========================="
#define DIVIDE_LINE4		"---------------------------"

#endif /* _NETMON_H */
