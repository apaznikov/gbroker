/*
 * gb_subst.h: Gbroker JSDL substitutions.
 *
 * (c) 2008-2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _GB_SUBST_H
#define _GB_SUBST_H

enum
{
	GB_JOBDESCR_LEN    = 255
};

/* JSDL substitutions */
#define GB_JSDLSUBST_ARCH     "${ARCH}"
#define GB_JSDLSUBST_GBHOST   "${GBHOST}"
#define GB_JSDLSUBST_HOME     "${HOME}"

/* RSL substitutions */
#define GB_RSLSUBST_HOME      "$(HOME)"

#endif /* _GB_SUBST_H */
