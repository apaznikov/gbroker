/*
 * nm_tcp.h: Library of common used functions used in client-server 
 *           applications.
 *
 * (c) 2008-2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _NM_TCP_H
#define _NM_TCP_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>

typedef int SOCKET;

#define EXIT(s)		exit(s)
#define CLOSE(s)	if (close(s)) error(1, errno, "close() failed")

#define SYNC_MSG	"1"

enum
{
	CONNECT_TIMEOUT    = 1,     /* Timeout in non-blocking connect function */
	HOSTNAME_LEN       = 255,
	TRUE 	           = 1,
	FALSE	           = 0,
	NLISTEN	           = 5,   	/* Maximal number of waiting connections */
	NSMB	           = 5,   	/* Number of buffers in shared memory. */
	SMBUFSZ            = 256, 	/* Size of buffer in shared mamory. */
	RECBUFSZ           = 128,	/* Size of record buffer in packet. */
	LINEBUFSZ          = 1500, 	/* Size of record buffer in line. */
	ERRSTRSZ           = 255,	/* Size of string for record in log. */
	REQUEST_REPLY_SIZE = 1,
	SYNC_MSG_LEN       = 1
};

extern char *Program_name; /* for error log */

#ifdef _SVR4
#define bzero(b,n) memset((b), 0, (n))
#endif

/* packet: Record with header (reclen) and body (buf) */
typedef struct packet_t
{
	u_int32_t reclen;
	char buf[RECBUFSZ];
} packet_t;

typedef void (*tofunc_t)(void *);

void error(int a, int b, char *c, ...);
int recvn(SOCKET s, char *buf, size_t len);
int recvvrec(SOCKET, char *buf, size_t len);
int recvline(SOCKET, char *buf, size_t len);
int sendn(SOCKET, char *buf, size_t len);

SOCKET tcp_server(char *hname, char *sname);
SOCKET tcp_client(char *hname, char *sname);
int set_address(char *hname, char *sname,
                struct sockaddr_in *sap, char *protocol);

/* synchronize_server: Syncronize server with client. */
void recvsync(SOCKET s);

/* synchronize_client: Syncronize client with server. */
void sendsync(SOCKET s);

/* synchronize_server: Receive test message (for connection test). */
int recvtest(SOCKET s);

/* synchronize_client: Send test message (for connection test). */
int sendtest(SOCKET s);

/* print_address: Print IP address and port number. */
void print_address(struct sockaddr_in peer);

/* getshortname: Get short hostname by full hostname (foo.bar.com -> foo). */ 
void getshorthname(const char *full_hname, char *short_hname);

#endif /* _NM_TCP_H */
