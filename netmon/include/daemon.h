/*
 * daemon.h:
 * (c) 2008 Alexey Paznikov
 */

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#ifndef _DAEMON_H
#define _DAEMON_H

#define	DAEMON_ERR	1
#define	BUFLEN		128

#define LOCKDIRMODE	(S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH)
#define LOCKMODE	(S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)

enum
{
	NHOSTS	 = 10,		/* Number of host systems. */
	STR_SIZE = 255
};

/*
	int hcntr;
	struct sockaddr_in hosts[NHOSTS];
*/

extern char Lockfile[BUFLEN];

/* daemonize: Initialize damon process. */
void daemonize(void);

/* isrunning: Is the daemon already run? */
int isrunning(const char *home_path);

/* gobg: Go to the background mode. */
void gobg(void);

#endif /* _DAEMON_H */
