/*
 * nm_tcp.h: Library of common used functions used in client-server 
 *           applications.
 *
 * (c) 2008-2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _NM_TCP_H
#define _NM_TCP_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>

typedef int SOCKET;

#define EXIT(s)		exit(s)
#define CLOSE(s)	if (close(s)) error(1, errno, "close() failed")

#define TCP_ACK     "a"

enum
{
	CONNECT_TIMEOUT = 1,       /* Timeout in non-blocking connect function */

	HOSTNAME_LEN    = 255,
	NLISTEN	        = 5,
	BUF_LEN         = 1024,
	TCP_ACK_SIZE    = 1
};

#ifdef _SVR4
#define bzero(b,n) memset((b), 0, (n))
#endif

/* recvn: Safe socket receive function. */
int recvn(SOCKET sock, char *buf, size_t len);

/* recvn: Safe socket send function. */
int sendn(SOCKET sock, char *buf, size_t len);

/* sendfile: Send file filename to socket. */
int sendfile(int sock, char *filename);

/* recvfile: Receive file. */
int recvfile(int sock, char *filename);

SOCKET tcp_server(char *hname, char *sname);
SOCKET tcp_client(char *hname, char *sname);
int set_address(char *hname, char *sname,
                struct sockaddr_in *sap, char *protocol);

/* getshortname: Get short hostname by full hostname (foo.bar.com -> foo). */ 
void getshorthname(const char *full_hname, char *short_hname);

#endif /* _NM_TCP_H */
