/*
 * gbps.h: Gbroker ps module.
 *
 * (c) 2008-2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _GBPS_H
#define _GBPS_H

enum
{
	GB_PS_ID_FIELD       = 4,
	GB_PS_NAME_FIELD     = 13,
	GB_PS_TOTTIME_FIELD  = 11,
	GB_PS_EXECTIME_FIELD = 11,
	GB_PS_STATE_FIELD    = 9,
	GB_PS_HOST_FIELD     = 6
};

#endif /* _GBPS_H */
