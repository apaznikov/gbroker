/*
 * dcsmon.h
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _DCSMON_H
#define _DCSMON_H

#include "tcp.h"

enum
{
	DM_PATH_LEN      = 255,
	DM_HNAME_LEN     = 80,
	DM_SNAME_LEN     = 6,
	DM_NPROC         = 1000,
	DM_ARCH_LEN      = 10,
	DM_SERV_MSG_LEN  = 10,
	DM_GW_TIMEOUT    = 5,
	DM_GWCFGLINE_LEN = 1024,
	DM_GWHOST_STRLEN = 255,
	DM_TOK_LEN       = 255
};

#define DCSMON_PORT     "14000"

#define NNODES_RQST     "dm_nnodes_"
#define PPN_RQST        "dm_ppn____"
#define FREENODES_RQST  "dm_freenod"
#define NPROCS_RQST     "dm_nprocs_"
#define FREEPROCS_RQST  "dm_freeroc"
#define WALLTIME_RQST   "dm_walltim"
#define NJOBS_RQST      "dm_njobs__"
#define PROCMAP_RQST    "dm_procmap"
#define CPUFREQ_RQST    "dm_cpufreq"
#define ARCH_RQST       "dm_arch__"

typedef struct
{
	SOCKET sock;

	char   hname[DM_HNAME_LEN];
	char   short_hname[DM_HNAME_LEN];
} dm_host_t;


typedef unsigned short int procmap_t; /* Map of free processes. */

extern char GWhome[DM_PATH_LEN];

#endif /* _DCSMON_H */
