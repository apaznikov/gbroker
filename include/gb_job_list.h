/*
 * gb_sched.c: Job list functions.
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _GB_JOBLIST_H
#define _GB_JOBLIST_H

#include "gbroker.h"

enum
{
	GB_PSFIELD_LEN = 15,
	GB_PSFIELDHOST_LEN = 1024
};

typedef struct 
{
	int id;
	char name[GB_PSFIELD_LEN];
	char tottime[GB_PSFIELD_LEN];
	char exectime[GB_PSFIELD_LEN];
	char state[GB_PSFIELD_LEN];
	char host[GB_PSFIELDHOST_LEN];
} gb_ps_t;

typedef struct gb_joblist_s gb_joblist_t;
struct gb_joblist_s
{
	gb_job_t *job;
	gb_joblist_t *next;
};

typedef struct 
{
	gb_joblist_t *prev;
	gb_joblist_t *cur;
} gb_joblist_iter_t;

extern gb_joblist_t *Rjobs_head, *Rjobs_tail; /* Runned jobs. */
extern gb_joblist_t *Cjobs_head, *Cjobs_tail; /* Completed jobs. */

extern pthread_mutex_t Rjobs_mutex;
extern pthread_mutex_t Cjobs_mutex;

/* Runned jobs. */
extern gb_joblist_t *Rjobs_head, *Rjobs_tail;  

/* Completed jobs. */
extern gb_joblist_t *Cjobs_head, *Cjobs_tail;  

/* joblist_getps: Write job list to buffer. */
int joblist_getps(char *psbuf);

/* joblist_iter_isend: Test if end of list is reached. */
int joblist_iter_isend(gb_joblist_iter_t *iter);

/* joblist_iter_getnext: Get next job in joblist. */
void joblist_iter_getnext(gb_joblist_iter_t *iter);

/* joblist_iter_getjob: Get job by iterator. */
gb_job_t *joblist_iter_getjob(gb_joblist_iter_t *iter);

/* runjoblist_iter_init: Init running job list iterator. */
void runjoblist_iter_init(gb_joblist_iter_t *iter);

/* getjobstate: Return job state of the job. */
void getjobstate(int id, gb_job_t **job, gb_jobstate_t *jobstate);

/* addcompljob: Add job to job list of completed jobs. */
int addcompljob(gb_job_t *job);

/* clearcompljobs: Remove all completed jobs. */
void clearcompljobs(void);

/* printcompljobs: Print all completed jobs. */
void printcompljobs(void);

/* addrunjob: Add job to job list. */
int addrunjob(gb_job_t *job);

/* delrunjob: Del job from list of running jobs. */
/*
void delrunjob(gb_joblist_t **prev, gb_joblist_t **cur);
*/
void delrunjob(gb_joblist_iter_t *iter);

/* printrunjobs: Print all jobs being in job list. */
void printrunjobs(void);

/* clearrunjobs: Remove all running jobs. */
void clearrunjobs(void);

/* joblist_init: Init job list. */
void joblist_init(void);

#endif /* _GB_JOBLIST_H */
