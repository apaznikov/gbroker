/*
 * gb_sched.c: Schedule functions.
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _GB_SCHED_H
#define _GB_SCHED_H

#include "gbroker.h"

#define EPS 0.001

typedef struct
{
	char **file;
	char nfiles;
	char hname[GB_HNAME_LEN];
	int fcurr;
} hoststage_t;

/* 
 * dosched: Make schedule decision. 
 *          migrhost is HID, from which job is migrating now. 
 */
int dosched(gb_job_t *job, gb_schedhosts_t **schedhosts);

/* getnnodesppn: Get count of nodes and ppn from job rank for host. */
int getnodesppn(char *host, int rank, int *nodes, int *ppn);

#endif /* _GB_SCHED_H */
