/*
 * gclient.h:
 *
 * (c) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _GCLIENT_H
#define _GCLIENT_H

/*
#define _GCLIENT_DEBUG
*/

#define GCLIENT_PORT    "15100"
#define GCLIENT_LOGNAME "gclient.log"

#endif /* _GCLIENT_H */
