/*
 * daemon.h:
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#ifndef _DAEMON_H
#define _DAEMON_H

#define	DAEMON_ERR	1
#define	BUFLEN		128

#define LOCKDIRMODE	(S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH)
#define LOCKMODE	(S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)

/* daemonize: Initialize damon process. */
int daemonize(const char *prog_home);

extern char Lockfile[BUFLEN];

#endif /* _DAEMON_H */
