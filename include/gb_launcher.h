/*
 * gb_launcher.h: Gbroker launcher. 
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _GB_LAUNCHER_H
#define _GB_LAUNCHER_H

#include <pthread.h>
#include "dcsmon.h"
#include "gb_sched.h"

enum
{
	NETMON_NATTEMPTS      = 5,
	DCSMON_NATTEMPTS      = 5, 
	GB_TIMEOUT_INTERVAL   = 100000, /* in usec */
	GB_TIMEOUT_NINTERVALS = 20
};

/*
 * Globus Toolkit functions
 */

/* globusrun: Globus Toolkit globusrun launcher. */
int globusrun(char *resource, char *job, char *contact);

/* globuskill: Kill job by Globus Toolkit globusrun. */
int globuskill(char *contact);

/* globusrun_status: Get status of the job by contact. */
int globusrun_status(char *contact, char *status);

/* globus-url-copy; Globus Toolkit globus-url-copy launcher. */
int globus_url_copy(char *src, char *dest);

/* remote_mkdir: Remote make dir on host. */
int remote_mkdir(char *host, char *dir);

/* getprocmap: Get map of free processors on host. */
procmap_t *getprocmap(char *host, int *nnodes);

/*
 * Netmon functions.
 */

/* getnettime: Return time to send msg with 'size' from hsot1 to host2. */
double getnettime(char *host1, char *host2, double size);

/* getnettimeforfile: Return time to send file from hsot1 to host2. */
double getnettimeforfile(char *host1, char *host2, char *file);

/* get_nettime_for_table: Get time for list of files. */
int get_nettime_for_table(char *host, hoststage_t *hoststages,
                          gb_host_t *hosts, int nhosts, double *stagetimes);

/*
 * Dcsmon functions.
 */

/* getnjobs: Get number of jobs (running and queued). */
int getnjobs(char *host, int *nrunning, int *nqueued);

/* getnnodes: Get total number of nodes. */
int getnnodes(char *host);

/* getnnodes: Get total number of nodes. */
int getppn(char *host);

/* getfreenodes: Get number of free nodes. */
int getfreenodes(char *host);

/* getnprocs: Get total number of processors. */
int getnprocs(char *host);

/* getnprocs_quick: Get total number of processors, ONE attempt. */
int getnprocs_quick(char *host);

/* getfreeprocs: Get number of free processors. */
int getfreeprocs(char *host);

/* getwalltime: Get queue walltime. */
int getwalltime(char *host);

/* getarch: Get system architecture. */
int getarch(char *host, char *arch);

/* getarch_quick: Get system architecture. One attempt only. */
int getarch_quick(char *host, char *arch);

#endif /* _GB_LAUNcHER_H */
