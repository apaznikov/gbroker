/*
 * gb_host.h: Functions for work with hosts.
 *
 * (c) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _GB_HOST_H
#define _GB_HOST_H

/* hosts_init: Init hosts' array. */
int hosts_init();

#endif /* _GB_HOST_H */
