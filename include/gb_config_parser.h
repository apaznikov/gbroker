/*
 * gb_config_parser.h
 *
 * (c) 2008 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _GB_CONFIG_PARSER_H
#define _GB_CONFIG_PARSER_H

#include "gbroker.h"

/* readcfg: Read configuration file. */
gb_host_t *readcfg(char *gbroker_home, int *nhosts);

#endif /* _GB_CONFIG_PARSER_H */
