/*
 * dm_client.h: Dcsmon client functions.
 *
 * (c) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _DM_CLIENT_H
#define _DM_CLIENT_H

#include "dcsmon.h"

/* dm_getnnodes: Get number of nodes. */
int dm_getnnodes(char *host);

/* dm_getfreenodes: Get number of free nodes. */
int dm_getfreenodes(char *host);

/* dm_getnprocs: get number of processors. */
int dm_getnprocs(char *host);

/* dm_getppn: Get number of processors per node. */
int dm_getppn(char *host);

/* dm_getfreeprocs: Get number of free processors. */
int dm_getfreeprocs(char *host);

/* dm_getwalltime: Get total walltime over the jobs. */
int dm_getwalltime(char *host);

/* dm_getnjobs: Get number of jobs. */
int dm_getnjobs(char *host, int *nrunning, int *nqueued);

/* dm_getcpufreq: Get value of CPU frequency. */
int dm_getcpufreq(char *host);

/* dm_getarch: Get system architecture (i686, x86_64, etc). */
int dm_getarch(char *host, char *arch);

/* dm_getprocmap: Get map of free processors. */
procmap_t *dm_getprocmap(char *host, int *nnodes);

#endif /* _DM_CLIENT_H */
