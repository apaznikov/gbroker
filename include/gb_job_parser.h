/*
 * gb_job_parser.h: Parse JSDL-file
 *
 * (c) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _GB_JOB_PARSER_H
#define _GB_JOB_PARSER_H

#include "gbroker.h"

/* parsejsdl: Read jsdl file, fill structure. */
int parsejsdl(char *jobfile, gb_job_t *job);

/* freejob: Free memory for job structure. */
void freejob(gb_job_t *job);

/* printjob: */
void printjob(gb_job_t *job);

#endif /* _GB_JOB_PARSER_H */
