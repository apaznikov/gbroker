/*
 * gb_jobmon.h: Job monitor module.
 *
 * (c) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _GB_JOB_MONITOR_H
#define _GB_JOB_MONITOR_H

extern int Nrunned;
extern int Ncompl;

/* jobmon: Start job monitor. */
void *jobmon(void *arg);

/* jobmon_addjob: Process job add. */
void jobmon_addjob(void);

/* is_pending: True if job is PENDING. */
int is_pending(char *status);

/* is_active: True if job is ACTIVE (running). */
int is_active(char *status);

/* is_done: True if job is DONE. */
int is_done(char *status);

/* is_indef: True if job state if INDEFINITE. */
int is_indef(char *status);

/* is_stagein: True if job state if STAGE_IN. */
int is_stagein(char *status);

#endif /* _GB_JOB_MONITOR_H */
