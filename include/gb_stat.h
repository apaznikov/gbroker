/*
 * gb_stat.h: Statistic collector (for modelling).
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _GB_STAT_H
#define _GB_STAT_H

/* gbstat: Gbroker system stat collector. */
void *gbstat(void *arg);

/* gbstat_schedtime: Write scheduling time into log. */
int gbstat_schedtime(gb_job_t *job);

/* gbstat_queuetime: Write queue waiting time into log. */
int gbstat_queuetime(gb_job_t *job);

/* gbstat_servtime: Write job service time into log. */
int gbstat_servtime(gb_job_t *job);

#endif /* _GB_STAT_H */
