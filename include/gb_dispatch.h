/*
 * gb_dispatch.c: Gbroker job dispatching functions.
 *
 * (C) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _GB_DISPATCH_H 
#define _GB_DISPATCH_H 

/* submittohost: Submit job to host, return contact. */
int submittohost(gb_job_t *job, gb_host_t *host, gb_schedhosts_t *schedhost);

/* migrate: Try to migrate job from cureent host to better host. */
int migrate(gb_job_t *job, int currhid);

/* killrest: Kill all jobs, not submitted to hit. */
int killrest(gb_job_t *job, int hid);

/* sendjob: Send job to gbroker. */
int sendjob(char *host, gb_job_t *job, SOCKET *sock);

/* recvjob: Recv job from client. */
int recvjob(SOCKET sock, gb_job_t *job);

/* sendoutput: Send job stdout to client. */
int sendoutput(SOCKET sock, gb_job_t *job);

/* recvoutputbyjid: Recv output from gbroker by global JID. */
int recvoutputbyjid(char *jid, char *stout, gb_jobstate_t *state);

/* clearjob: Remove temprorary job files and dir. */
int clearjob(gb_job_t *job);

/* pathtoname: Get filename from path. */
char *pathtoname(char *s);

/* dispatch_init: Init job dispatcher. */
void dispatch_init(void);

#endif /* _GB_DISPATCH_H */
