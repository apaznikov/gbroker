/*
 * gb_utils.h
 *
 * (c) 2008 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _GB_UTILS_H
#define _GB_UTILS_H

/* gettimepast: Get time past from 'past' to this moment. */
double gettimepast(struct timeval past);

/* pathtoname: Get filename from path. */
char *pathtoname(char *s);

/* jsdlsubst: Make substitutions with line. */
void jsdlsubst(char *ssrc, gb_host_t *host, char *sdest);

#endif /* _GB_UTILS_H */
