/*
 * gb_job.h: Functions connected with job.
 *
 * (c) 2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _GB_JOB_H
#define _GB_JOB_H

#include "gbroker.h"

/* job_resource_add_adj: Make resource state adjustment. */
void job_resource_add_adj(gb_job_t *job, gb_host_t *host);

/* job_resource_remove_adj: Make resource state adjustment. */
void job_resource_remove_adj(gb_job_t *job, gb_host_t *host);

/* jobmalloc: Allocate memory for job (hostcount-dependent attributes). */
int jobmalloc(gb_job_t *job);

/* jobfree: Free memory for job. */
void jobfree(gb_job_t *job);

#endif /* _GB_JOB_H */
