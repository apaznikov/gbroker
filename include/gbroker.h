/*
 * grbroker.h: Main gbroker module.
 *
 * (c) 2008-2010 Alexey Paznikov <apaznikov@gmail.com>
 */

#ifndef _GBROKER_H
#define _GBROKER_H

#include <semaphore.h>
#include <pthread.h>
#include "tcp.h"
#include "gb_subst.h"

#define GB_DEBUG
#define GB_SCHEDULER_DEBUG
/*
#define GB_JOBMON_DEBUG
*/

/******************************************************************************
 *                               Scheduling                                   *
 ******************************************************************************/ 
/**********************
 * Objective function *
 **********************/

#define GB_FUNCOBJ_FREEPROCS
#define GB_FUNCOBJ_NJOBS_Q
/*
#define GB_FUNCOBJ_NJOBS_QR
*/

/**********************
 *    Migration       *
 **********************/

#define GB_MIGRATE_ENABLE

/* 
 * Job migrating interval. (Must be more than minimal time after submitting to 
 * host when job is in pending state.)
 * Default value is 60.
 */
#define GB_MIGR_INTERVAL 60
/*
#define GB_MIGR_INTERVAL 120
*/

/* Number of hosts, on which job is simultaneously submitted. */
#define GB_NSUBMITS      1

/* 
 * Criterion for job migration. 
 * Min gain by object function between old and new object values. 
 * Default value is 0.2.
 */ 
/*
#define GB_MIGRATE_OBJ_CRIT 0.2
*/
#define GB_MIGRATE_OBJ_CRIT 0.5

/* Min value of stagetime. */
#define GB_STAGETIME_EPS    5.0

enum
{
	/* Job state query interval. */
	GB_JOBMON_TIMEOUT  = 10,

	/* Resource utilize statistics gathering timeout. */
	GB_STAT_TIMEOUT    = 30,

	/* String lengts. */
	GB_PATH_LEN        = 255,
	GB_HNAME_LEN       = 255,
	GB_CMD_LEN         = 255,
	GB_CONTACT_LEN     = 255,
	GB_STATUS_LEN      = 255,
	GB_TOKEN_LEN       = 255,
	GB_GID_LEN         = 255,
	GB_LOGLINE_LEN     = 1024,
	GB_SRVMSG_LEN      = 20,
	GB_ARCH_LEN        = 10,
	GB_PSBUF_LEN       = 50000,

	/* Max number of unique hosts in malloc_hoststages(). */
	GB_MAX_HOSTS       = 1024
};

typedef enum
{
	GB_JOBRUNNING_ST,
	GB_JOBCOMPLETE_ST,
	GB_UNKNOWN_ST
} gb_jobstate_t;

#define GBROKER_PORT          "15000"
#define GB_JOBSUBMIT_RQST     "gb_jobsubmit_request_"
#define GB_GETOUTPUT_RQST     "gb_getoutput_request_"
#define GB_JOBSUBMIT_ERROR    "gb_jobsubmit_error___"
#define GB_JOB_RUNNING_RPL    "gb_job_running_______"
#define GB_JOB_COMPLETE_RPL   "gb_job_complete______"
#define GB_PS_RQST            "gb_ps_request________"
#define GB_SUCCESS            "gb_success___________"
#define GBROKER_LOGNAME       "gbroker.log"

/* JSDL substitutions */
#define GB_JSDLSUBST_ARCH     "${ARCH}"
#define GB_JSDLSUBST_GBHOST   "${GBHOST}"
#define GB_JSDLSUBST_HOME     "${HOME}"

/* RSL substitutions */
#define GB_RSLSUBST_HOME      "$(HOME)"

#define GB_DEFAULT_ARCH       "i686"

/* Job statuses. */
#define GB_JOB_STATUS_PENDING    "PENDING"
#define GB_JOB_STATUS_STAGE_IN   "STAGE_IN"
#define GB_JOB_STATUS_STAGE_OUT  "STAGE_OUT"
#define GB_JOB_STATUS_ACTIVE     "ACTIVE"
#define GB_JOB_STATUS_UNKNOWN    "UNKNOWN"
#define GB_JOB_STATUS_DONE       "DONE"
#define GB_JOB_STATUS_INDEF      "--"

typedef struct
{
	int    id;
	SOCKET sock;
	char   hname[GB_HNAME_LEN];
	char   shname[GB_HNAME_LEN];
	char   arch[GB_ARCH_LEN];
	double obj;
	int    nprocs;

	/* 
	 * Adjustments for jobs was previsously submitted to resource,
	 * but have not executed yet
	 */
	int adj_jobnum;			/* Number of enqueued jobs. */
	int adj_procnum;		/* Number of free processors. */
} gb_host_t;

typedef struct
{
	char name[GB_JOBDESCR_LEN];
	char src[GB_JOBDESCR_LEN];
	char dest[GB_JOBDESCR_LEN];
} gb_stage_t;

typedef struct 
{
	int hid;                    /* Host ID */

	/* Host current state: */
	double stagetime;		    /* Time to stage files. */
	double frprocs;			    /* Number of free processors. */
	double totprocs;		    /* Total number of processors. */
	double nrjobs;			    /* Number of running jobs. */
	double nqjobs;			    /* Number of queued jobs. */
	double obj;			        /* Value of objective function. */
} gb_schedhosts_t;

typedef struct
{
	char exec[GB_JOBDESCR_LEN]; /* Execution file name. */
	char arch[GB_ARCH_LEN];     /* Architecture. */
} gb_execlist_t;

typedef struct
{
	/* Job identifiers. */
	char gid[GB_GID_LEN];				/* Global job id. */
	int  id;							/* Local job id. */
	char locdir[GB_PATH_LEN];           /* Local directory for all job files. */
	char remdir[GB_PATH_LEN];           /* Remote job directory. */

	/* Job description. */
	char name[GB_JOBDESCR_LEN];		    /* Job name. */
	char exec[GB_JOBDESCR_LEN];		    /* Default execution file. */
	char execname[GB_JOBDESCR_LEN];     /* Execution filename. */
	int  is_defexec_exists;             /* Flag is default exec exists. */
	gb_execlist_t *execlist;            /* List of exec files. */
	int  narchs;						/* Length of architecture list. */
	char args[GB_JOBDESCR_LEN];		    /* Program arguments. */
	char stout[GB_JOBDESCR_LEN];		/* Name of standart output file. */
	char sterr[GB_JOBDESCR_LEN];		/* Name of standart error file. */
	gb_stage_t *stages;					/* Stage-files. */
	int  nstages;						/* Number of stage-files. */
	int  rank;							/* Parallel program rank. */
	int  wallt;							/* Walltime. */
	char username[GB_JOBDESCR_LEN];     /* Name of user submitted job. */

	/* Globus execution. */
	char **contact;						/* Globus Toolkit contact string. */
	char **status;   					/* Globus Toolkit status. */
	char **rsl;							/* Globus Toolkit RSL job file. */
	gb_host_t **submithosts;            /* Hosts where job was submitted. */
	gb_schedhosts_t *schedhosts;        /* Non-sorted hosts (state & obj), 
	                                       where job is submitted. */
	int migrhost;                       /* Host, from which job is migrating */
	gb_host_t *exechost;				/* Host where job was executed. */
	int  *nodes;						/* Number of nodes (subsystem). */
	int  *ppn;							/* Processors per node (subsystems) */

	/* Flags. */
	int  outputflag;					/* 1 if job s interactive. */
	sem_t complete_sem;					/* 1 if job is completed. */
	int  execflag;                      /* 1 if job has been executed. */
	int  *res_adj_flags;				/* Resource state adjustment flags. */

	/* Time moments. */
	struct timeval exectime;			/* When job was started executing. */
	struct timeval hostsubmittime;		/* When job was submitted to host. */
	struct timeval submittime;			/* When job was submitted to gbroker. */
	struct timeval migrtime;			/* Migrate attempt time. */
} gb_job_t;

typedef struct
{
	SOCKET sock;
} server_args_t;

extern char Gbroker_home[GB_PATH_LEN];
extern char Thishost[GB_HNAME_LEN];
extern gb_host_t *Hosts;
extern int Nhosts;

extern pthread_mutex_t Jid_mutex;
extern pthread_mutex_t Jobsubmit_mutex;

#endif /* _GBROKER_H */
